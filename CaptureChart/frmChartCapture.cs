﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using QlikLog;
using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Media;

namespace CaptureChart
{
    public partial class Capture : Form
    {
        public string newFilePath = string.Empty;
        public string _chartType = string.Empty;
        public string currencyFormat = string.Empty;
        public string numberFormat = "_crunchbot_";
        public bool _isIndependent = false;
        public bool isCurrency = false;
        public bool isNumberFormat = false;
        public static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        public static string module = "CaptureChart", method = string.Empty;

        public Capture()
        {
            _isIndependent = true;
            newFilePath = @"E:\Received Files\7.png";
            double v = 1.25;
            string check = v.ToString("#,##0K");
            InitializeComponent();
            DataSet ds = new DataSet();
            ds.ReadXml(@"E:\Received Files\QlikSenseTableLive!Vertical.xml");
            DataTable dataTable = ds.Tables[0];
            _chartType = "bar_vertical";
            CreateChart(dataTable);
            #region
            //cartesianChart1.Series = new LiveCharts.SeriesCollection
            //{
            //    new ColumnSeries
            //    {
            //        Title = "Sales",
            //        Values = chartValues,
            //        DataLabels = true
            //    }
            //};

            ////adding series will update and animate the chart automatically
            //cartesianChart1.Series.Add(new ColumnSeries
            //{
            //    Title = "2016",
            //    Values = new ChartValues<double> { 11, 56, 42 }
            //});

            ////also adding values updates and animates the chart automatically
            //cartesianChart1.Series[1].Values.Add(48d);

            //cartesianChart1.AxisX.Add(new LiveCharts.Wpf.Axis
            //{
            //    Title = "Customer",
            //    Labels = xAxisLables
            //});

            //cartesianChart1.AxisY.Add(new LiveCharts.Wpf.Axis
            //{
            //    Title = "Sales",
            //    LabelFormatter = value => "$" + (value / 1000).ToString("N") + "K",
            //    Separator = new Separator { UseLayoutRounding = true }
            //});
            #endregion
        }

        public void AddSeriesCollection(DataTable table, ChartValues<double> chartValues, string yAxisTitle, string chartType)
        {
            method = "AddSeriesCollection";

            try
            {
                var converter = new System.Windows.Media.BrushConverter();

                if (chartType.ToLower().Contains("bar") || chartType.ToLower().Contains("combo"))
                {
                    if (table.Columns.Count <= 9999)//change 9999 to 4, if you want stack bar
                    {
                        if (cartesianChart.Series.Count == 0)
                        {
                            if (chartType.ToLower().Contains("vertical"))
                            {
                                cartesianChart.Series = new LiveCharts.SeriesCollection
                            {
                                new ColumnSeries
                                {
                                    Title =  yAxisTitle,
                                    Values = chartValues,
                                    DataLabels = true,
                                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),
                                    LabelsPosition = BarLabelPosition.Parallel
                                }
                            };
                            }
                            else
                            {

                                cartesianChart.Series = new LiveCharts.SeriesCollection
                            {
                                new RowSeries
                                {
                                    Title =  yAxisTitle,
                                    Values = chartValues,
                                    DataLabels = true,
                                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),
                                    LabelsPosition = BarLabelPosition.Parallel
                                }
                            };
                            }
                        }
                        else
                        {
                            if (chartType.ToLower().Contains("vertical"))
                            {
                                cartesianChart.LegendLocation = LegendLocation.Right;
                                cartesianChart.Series.Add(new ColumnSeries
                                {
                                    Title = yAxisTitle,
                                    Values = chartValues,
                                    PointGeometry = cartesianChart.Series.Count == 1 ? DefaultGeometries.Diamond : cartesianChart.Series.Count == 2 ? DefaultGeometries.Square : DefaultGeometries.Circle,
                                    DataLabels = true,
                                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                                });
                            }
                            else
                            {
                                cartesianChart.LegendLocation = LegendLocation.Right;
                                cartesianChart.Series.Add(new RowSeries
                                {
                                    Title = yAxisTitle,
                                    Values = chartValues,
                                    PointGeometry = cartesianChart.Series.Count == 1 ? DefaultGeometries.Diamond : cartesianChart.Series.Count == 2 ? DefaultGeometries.Square : DefaultGeometries.Circle,
                                    DataLabels = true,
                                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                                });
                            }
                        }
                    }
                    else
                    {
                        if (cartesianChart.Series.Count == 0)
                        {
                            cartesianChart.Series = new LiveCharts.SeriesCollection
                        {
                            new StackedColumnSeries
                            {
                                Title =  yAxisTitle,
                                Values = chartValues,
                                StackMode = StackMode.Values,
                                DataLabels = true,
                                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                            }
                        };
                        }
                        else
                        {
                            cartesianChart.Series.Add(new StackedColumnSeries
                            {
                                Title = yAxisTitle,
                                Values = chartValues,
                                StackMode = StackMode.Values,
                                DataLabels = true,
                                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                            });
                        }
                    }
                }
                else if (chartType.ToLower().Contains("line"))
                {
                    if (cartesianChart.Series.Count == 0)
                    {
                        cartesianChart.Series = new LiveCharts.SeriesCollection
                    {
                        new LineSeries
                        {
                            Title =  yAxisTitle,
                            Values = chartValues,
                            DataLabels = true,
                            Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                        }
                    };
                    }
                    else
                    {
                        cartesianChart.Series.Add(new LineSeries
                        {
                            Title = yAxisTitle,
                            Values = chartValues,
                            DataLabels = true,
                            Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                        });
                    }
                }
                else if (chartType.ToLower().Contains("scatter"))
                {
                    bool _1stRow = true;

                    foreach (DataRow row in table.Rows)
                    {
                        if (_1stRow)
                        {
                            cartesianChart.Series = new LiveCharts.SeriesCollection
                        {
                            new ScatterSeries
                            {
                                Title = row[0].ToString(),
                                Values = new ChartValues<ScatterPoint>
                                {
                                    //X  Y   W
                                    new ScatterPoint(Convert.ToDouble(row[1].ToString().Replace(currencyFormat, "").Replace(numberFormat, "")), Convert.ToDouble(row[2].ToString().Replace(currencyFormat, "").Replace(numberFormat, "")))
                                },
                                MinPointShapeDiameter = 15,
                                MaxPointShapeDiameter = 45,
                                LabelPoint = point => table.Columns[2].ColumnName + ": " + point.Y + "\n" + table.Columns[1].ColumnName  + ": " + point.X.ToString("#,##0,"+(numberFormat != "NA" ? numberFormat : "")),
                                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                            }
                        };
                        }
                        else
                        {
                            cartesianChart.Series.Add(new ScatterSeries
                            {
                                Title = row[0].ToString(),
                                Values = new ChartValues<ScatterPoint>
                            {
                                //X  Y   W
                                new ScatterPoint(Convert.ToDouble(row[1].ToString().Replace(currencyFormat, "").Replace(numberFormat, "")), Convert.ToDouble(row[2].ToString().Replace(currencyFormat, "").Replace(numberFormat, "")))
                            },
                                MinPointShapeDiameter = 15,
                                MaxPointShapeDiameter = 45,
                                LabelPoint = point => table.Columns[2].ColumnName + ": " + point.Y + "\n" + table.Columns[1].ColumnName + ": " + point.X.ToString("#,##0," + (numberFormat != "NA" ? numberFormat : "")),
                                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                            });
                        }
                        _1stRow = false;
                    }

                    cartesianChart.AxisX.Add(new LiveCharts.Wpf.Axis
                    {
                        Title = table.Columns[1].ColumnName,
                        LabelFormatter = value => isCurrency == true && isNumberFormat == true ? currencyFormat + value.ToString("N") + numberFormat : isCurrency == true && isNumberFormat == false ? currencyFormat + value.ToString("#,##0") : isCurrency == false && isNumberFormat == true ? value.ToString("N") + numberFormat : value.ToString("#,##0"),
                        //LabelFormatter = value => (currencyFormat != "NA" ? currencyFormat : "") + (value > 1000 ? ((value / (numberFormat != "NA" || numberFormat.ToLower() == "k" ? 1000 : numberFormat.ToLower() == "m" ? 1000000 : 1000000000)).ToString("N") + (numberFormat != "NA" ? numberFormat : "")) : value.ToString("#,##0")),// + (numberFormat != "NA" ? "," + numberFormat : ""))),
                        //LabelFormatter = value => (currencyFormat != "NA" ? currencyFormat : "") + (value.ToString("#,##0" + (numberFormat != "NA" ? "," + numberFormat : ""))),
                        Separator = new Separator { UseLayoutRounding = true },
                        Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                    });

                    cartesianChart.AxisY.Add(new LiveCharts.Wpf.Axis
                    {
                        Title = table.Columns[0].ColumnName,
                        //LabelFormatter = value => (currencyFormat != "NA" ? currencyFormat : "") + (value > 1000 ? ((value / (numberFormat != "NA" || numberFormat.ToLower() == "k" ? 1000 : numberFormat.ToLower() == "m" ? 1000000 : 1000000000)).ToString("N") + (numberFormat != "NA" ? numberFormat : "")) : value.ToString("#,##0")),// + (numberFormat != "NA" ? "," + numberFormat : ""))),
                        //LabelFormatter = value => (currencyFormat != "NA" ? currencyFormat : "") + (value.ToString("#,##0" + (numberFormat != "NA" ? "," + numberFormat : ""))),
                        Separator = new Separator { UseLayoutRounding = true },
                        Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                    });
                }
                else if (chartType.ToLower().Contains("pie"))
                {
                    Func<ChartPoint, string> labelPoint = chartPoint => string.Format("{0} ({1:P})", chartPoint.Y.ToString("#,##0,K"), chartPoint.Participation);

                    if (pieChart.Series.Count == 0)
                    {
                        pieChart.Series = new LiveCharts.SeriesCollection
                    {
                        new PieSeries
                        {
                            Title = table.Rows[0][0].ToString(),
                            Values = new ChartValues<double> {Convert.ToDouble(table.Rows[0][1].ToString())},
                            PushOut = 15,
                            DataLabels = true,
                            LabelPoint = labelPoint,
                            Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                        }
                    };

                        bool skip1stRow = true;
                        foreach (DataRow item in table.Rows)
                        {
                            if (!skip1stRow)
                            {
                                pieChart.Series.Add(new PieSeries
                                {
                                    Title = item[0].ToString(),
                                    Values = new ChartValues<double> { Convert.ToDouble(item[1].ToString()) },
                                    DataLabels = true,
                                    LabelPoint = labelPoint,
                                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),

                                });
                            }
                            else
                                skip1stRow = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Error while adding series to chart.", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
            }
        }

        public Capture(DataTable dataTable, string filePath, string chartType)
        {
            method = "Capture";

            try
            {
                InitializeComponent();
                newFilePath = filePath;

                log.Info(module, method, "File path: " + newFilePath, true);

                if (chartType.ToLower().Contains("map"))
                    _chartType = "barchart_vertical";
                else
                    _chartType = chartType;

                log.Info(module, method, "Chart type: " + _chartType, true);

                CreateChart(dataTable);
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Error component initialization.", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
            }
        }

        public void CreateChart(DataTable dataTable)//Live Chart Component
        {
            try
            {
                //Disable Animations
                cartesianChart.DisableAnimations = true;
                pieChart.DisableAnimations = true;

                //Hoverable
                cartesianChart.Hoverable = false;

                DataTable dt = dataTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull)).Take(24).CopyToDataTable();
                ChartValues<double> chartValues = new ChartValues<double>();

                string _1stMeasureValue = dt.Rows[0][1].ToString();
                if (new[] { "k", "m", "b" }.Any(c => _1stMeasureValue.ToLower().Contains(c)))
                {
                    isNumberFormat = true;
                    numberFormat = _1stMeasureValue.ToCharArray(_1stMeasureValue.Length - 1, 1)[0].ToString();
                }
                char currencySymbol = _1stMeasureValue.ToCharArray(0, 1)[0];
                if (!Char.IsNumber(currencySymbol))
                {
                    currencyFormat = currencySymbol.ToString();
                    isCurrency = true;
                }
                else
                    currencyFormat = "_NA";


                int colCount = 0;
                if (_chartType.ToLower().Contains("scatter"))
                {
                    AddSeriesCollection(dt, chartValues, "", _chartType);
                    pieChart.SendToBack();
                    cartesianChart.BringToFront();
                }
                else if (_chartType.ToLower().Contains("pie"))
                {
                    AddSeriesCollection(dt, chartValues, "", _chartType);
                    cartesianChart.SendToBack();
                    pieChart.BringToFront();
                    pieChart.LegendLocation = LegendLocation.Right;
                }
                else
                {
                    foreach (DataColumn Columns in dt.Columns)
                    {
                        var list = dt.Rows.Cast<DataRow>().Select(row => row[Columns.ColumnName].ToString().Length > 14 ? row[Columns.ColumnName].ToString().Substring(0, 13) + "..." : row[Columns.ColumnName].ToString()).ToList();
                        //if (_chartType.ToLower().Contains("horizontal"))
                        //    list.Reverse();

                        if (colCount == 0)//X-Axis 
                        {
                            LiveCharts.Wpf.Axis _1stAxis = new LiveCharts.Wpf.Axis
                            {
                                Title = Columns.ColumnName,
                                Labels = list,
                                Separator = new Separator { Step = 1 },// dt.Columns.Count <= 4 ? new Separator { Step = 1 } : DefaultAxes.CleanSeparator,
                                LabelsRotation = _chartType.ToLower().Contains("horizontal") ? 0 : -45,
                                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),
                                FontSize = 12
                            };

                            if (_chartType.ToLower().Contains("horizontal"))
                                cartesianChart.AxisY.Add(_1stAxis);
                            else
                                cartesianChart.AxisX.Add(_1stAxis);
                        }
                        else
                        {
                            try
                            {
                                chartValues = new ChartValues<double>();
                                var tempChartValues = new double[dt.Rows.Count];
                                int chartValuesCount = -1;
                                dt.Rows.Cast<DataRow>().ToList().ForEach(delegate (DataRow row)
                                {
                                    tempChartValues[++chartValuesCount] = (Convert.ToDouble(row[Columns.ColumnName].ToString().Replace(currencyFormat, "").Replace(numberFormat, "")));
                                });
                                chartValues.AddRange(tempChartValues);
                                AddSeriesCollection(dt, chartValues, Columns.ColumnName, _chartType);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex.Message);
                            }
                        }
                        colCount++;
                    }

                    LiveCharts.Wpf.Axis _2ndAxis = new LiveCharts.Wpf.Axis
                    {
                        Title = cartesianChart.Series[0].Title,
                        LabelFormatter = value => isCurrency == true && isNumberFormat == true ? currencyFormat + value.ToString("N") + numberFormat : isCurrency == true && isNumberFormat == false ? currencyFormat + value.ToString("#,##0") : isCurrency == false && isNumberFormat == true ? value.ToString("N") + numberFormat : value.ToString("#,##0"),
                        //LabelFormatter = value => (currencyFormat != "NA" ? currencyFormat : "") + (value > 1000 ? ((value / (numberFormat != "NA" || numberFormat.ToLower() == "k" ? 1000 : numberFormat.ToLower() == "m" ? 1000000 : 1000000000)).ToString("N") + (numberFormat != "NA" ? numberFormat : "")) : value.ToString("#,##0")),// + (numberFormat != "NA" ? "," + numberFormat : ""))),
                        Separator = new Separator { UseLayoutRounding = true },
                        Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0)),
                        FontSize = 12
                    };

                    if (_chartType.ToLower().Contains("horizontal"))
                        cartesianChart.AxisX.Add(_2ndAxis);
                    else
                        cartesianChart.AxisY.Add(_2ndAxis);

                    cartesianChart.BringToFront();
                }
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Error while creating chart.", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
            }
        }

        public void Capture_Load(object sender, EventArgs e)
        {
            try
            {
                //Bitmap bitmap = new Bitmap(pieChart1.Width, pieChart1.Height);
                //pieChart1.DrawToBitmap(bitmap, new Rectangle(0, 0, pieChart1.Width, pieChart1.Height));
                //bitmap.Save(newFilePath, ImageFormat.Png);

                if (!_isIndependent)
                {
                    timer.Stop();
                    timer.Interval = 1; //800
                    timer.Start();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private void Capture_FormClosing(object sender, FormClosingEventArgs e)
        {
            Bitmap bitmap = new Bitmap(6, 6);
            if (_chartType.ToLower().Contains("pie"))
            {
                bitmap = new Bitmap(pieChart.Width, pieChart.Height);
                pieChart.DrawToBitmap(bitmap, new Rectangle(0, 0, pieChart.Width, pieChart.Height));
            }
            else if (_chartType.ToLower().Contains("map"))
            {
                //Do Nothing
            }
            else
            {
                bitmap = new Bitmap(cartesianChart.Width, cartesianChart.Height);
                cartesianChart.DrawToBitmap(bitmap, new Rectangle(0, 0, cartesianChart.Width, cartesianChart.Height));
            }
            //bitmap.Save(newFilePath, ImageFormat.Png);

            using (MemoryStream memory = new MemoryStream())
            {
                method = "Capture_FormClosing";
                log.Info(module, method, "File path: " + newFilePath, true);

                try
                {
                    if (File.Exists(newFilePath))
                    {
                        log.Info(module, method, "Delete file from: " + newFilePath, true);
                        File.Delete(newFilePath);
                    }
                    else
                    {
                        string filePathDirectory = Path.GetDirectoryName(newFilePath);
                        log.Info(module, method, "File directory: " + filePathDirectory, true);

                        if (!Directory.Exists(filePathDirectory))
                        {
                            log.Info(module, method, "File directory not exist.", true);
                            Directory.CreateDirectory(filePathDirectory);
                            log.Info(module, method, "File directory created.", true);
                        }
                    }

                    using (FileStream fs = new FileStream(newFilePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        bitmap.Save(memory, ImageFormat.Png);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Dispose();
                        fs.Close();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(module, method, "Error while capturing chart image.", true);
                    log.Error(module, method, ex.Message, true);
                    log.Error(module, method, ex.StackTrace, true);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
