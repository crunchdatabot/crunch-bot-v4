﻿using System;
using System.Diagnostics;
using System.IO;

namespace QlikLog
{
    public class LogFile
    {
        private static string LogFileName;
        private static string LogFilePath;
        public enum LogType { logInfo = 1, logWarning = 2, logError = 3 };

        public LogFile(String path)
        {
            if (path == "" || path == null)
            {
                LogFilePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", "")) + "\\Log";
                System.IO.Directory.CreateDirectory(LogFilePath);
            }
            
            if (string.IsNullOrEmpty(LogFilePath))
                LogFilePath = path;

            LogFileName = LogFilePath + "\\SlackBOT-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
        }

        public string GetLogFileName()
        {
            return LogFileName;
        }

        public void Info(string module, string method, string LogLine, bool WriteToConsole = false, string ErrorType = "Info")
        {
            try
            {
                string line = string.Format("{0}:\t{1}:\t{2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ErrorType, LogLine);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    w.WriteLine(line);
                    w.Close();
                    w.Dispose();
                }

                if (WriteToConsole)
                {
                    Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("LogFile: {0} Exception caught.", e);
            }
        }

        public void Error(string module, string method, string LogLine, bool WriteToConsole = false, string ErrorType = "Error")
        {
            try
            {
                string line = string.Format("{0}:\t{1}:\t{2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ErrorType, LogLine);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    w.WriteLine(line);
                    w.Close();
                    w.Dispose();
                }
                if (WriteToConsole)
                {
                    Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("LogFile: {0} Exception caught.", e);
            }
        }

        public void Warn(string module, string method, string LogLine, bool WriteToConsole = false, string ErrorType = "Warn")
        {
            try
            {
                //string line = string.Format("{0}:\t{1}:\t{2}:\t{3}:\t{4}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ErrorType, module, method, LogLine);
                string line = string.Format("{0}:\t{1}:\t{2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ErrorType, LogLine);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    w.WriteLine(line);
                    w.Close();
                    w.Dispose();
                }
                if (WriteToConsole)
                {
                    Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("LogFile: {0} Exception caught.", e);
            }
        }

        public void AddBotLine(string LogText, LogType Type = LogType.logInfo)
        {
            string line;
            line = string.Format("{0}\t{1}", Type.ToString("G"), LogText);
            Info("", "", line);
        }

        public void AddBotLine(string LogText, string userID, string firstName, string lastName, string username, LogType Type = LogType.logInfo)
        {
            string line;
            line = string.Format("{0}\tID:{1}\tFirst:{2}\tLast:{3}\tUserName:{4}\t<{5}>", Type.ToString("G"), userID, firstName, lastName, username, LogText);
            Info("", "", line);
        }

        //public void AddBotLine(string LogText, Telegram.Bot.Types.User User, LogType Type = LogType.logInfo)
        //{
        //    string line;
        //    line = string.Format("{0}\tID:{1}\tFirst:{2}\tLast:{3}\tUserName:{4}\t<{5}>", Type.ToString("G"), User.Id.ToString(), User.FirstName, User.LastName, User.Username, LogText);
        //    Info("", "", line);
        //}
    }
}
