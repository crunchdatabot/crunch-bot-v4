﻿using Dialogflow;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using GemBox.Spreadsheet;
using Newtonsoft.Json.Linq;
using QlikLog;
using QlikSense;
using Util.Multilanguage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Util;
using System.Text.RegularExpressions;

namespace SlackBOT
{
    public enum CallBackAction
    {
        None = 0, OpenApp = 1, ShowKPI = 2, ShowDimension = 3, ShowMeasure = 4, ShowSheet = 5, ShowStory = 6, Application = 7, KPI = 8, Dimension = 9, Measure = 10,
        Sheet = 11, Story = 12, Report = 13, Visualization = 14, ShowVisualization = 15
    };

    public class ResponseOptions
    {
        public string Title;
        public string ID;
        public string Group;
        public CallBackAction Action;
    }

    public class Response
    {
        public NLP nlp;
        public LUIS luisUtil;
        public QlikSenseObject ChartFound;
        public string ChartText = "";
        public List<ResponseOptions> Options = new List<ResponseOptions>();
        public string TextMessage = "";
        public string NarrativeText = "";
        public string WarningText = "";
        public string ErrorText = "";
        public string VizID = "";
        public DataTable chartTable = new DataTable();

        public string NewsSearch = "";
        public string OtherAction = "";
        public string VoiceMessage = "";
    }

    public class Conversation
    {
        private static NLP nlp = new NLP();

        //Dialogflow Agent
        public static string dialogflowKey = string.Empty;
        public static string language = string.Empty;

        //LUIS Agent
        public static string conLuisAppId = string.Empty;
        public static string conLuisSubscriptionKey = string.Empty;
        public static string conLuisBaseApiUrl = string.Empty;

        public static int NumberOfResults = 10;

        //Capture Image
        private string conNPrintingReportPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
        string captureImageFileName = "";
        /// <summary>
        /// COnversation constructor which intialized Dialogflow configuration
        /// </summary>
        /// <param name="dialogflowKey"></param>
        /// <param name="language"></param>
        public Conversation(bool isDialogflow = true)
        {
            if (isDialogflow)
                nlp.StartDialogflow(dialogflowKey, language);
            else
                nlp.StartLUIS(conLuisAppId, conLuisSubscriptionKey, conLuisBaseApiUrl);
        }

        private static string cntSavvyNarrativeKey;
        public string SavvyNarrativeKey { set { cntSavvyNarrativeKey = value; } }

        private static string cntNarrativeScienceKey;
        public string NarrativeScienceKey { set { cntNarrativeScienceKey = value; } }

        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        private static RandomResponse randomResponse = new RandomResponse();
        string module = "Conversation", method = string.Empty;

        public Response ReplySync(string inText, QlikSenseUser user)
        {
            Task<Response> ReplyTask = Reply(inText, user);
            ReplyTask.Wait();
            Response Resp = ReplyTask.Result;
            return Resp;
        }

        /// <summary>
        /// Generate response based on intents and their identities
        /// </summary>
        /// <param name="inText"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<Response> Reply(string inText, QlikSenseUser user, bool isDialogflow = true)
        {
            method = "Reply";

            Response Resp = new Response();

            string conQlikAppDateFormat = Util.Parameters.getConfigParameters("conQlikAppDateFormat").ToString().Replace("YYYY", "yyyy").Replace("DD", "dd").Replace("mm", "MM");

            IntentType IntentType = new IntentType();

            string narrative = string.Empty;

            string Dimension = "";
            string Dimension2 = "";
            string Dimension3 = "";
            string Measure = "";
            string Measure2 = "";
            List<string> Elements = new List<string>();
            List<string> Dates = new List<string>();
            string ChartType = "";

            List<string> DatePeriod = new List<string>();
            List<string> DatePeriod1 = new List<string>();
            List<string> OriginalDatePeriod = new List<string>();
            List<string> Date = new List<string>();

            string Response = string.Empty;

            string Percentage = "";
            List<double?> Number = null;
            double DistanceKm = 0;
            string Language = "";

            string url = "";
            string UserChart = "";
            string UserSheet = "";

            if (user.TimeSincePreviousAccess().Minutes > 5)
            {
                user.TheBotIsAngry = false;
                user.QlikSenseApp.lastFilters.Clear();
            }

            if (await UnderstandSentence(user, inText, isDialogflow))
            {
                IntentType = nlp.IntentType;
                Dimension = nlp.Dimension;
                Dimension2 = nlp.Dimension2;
                Dimension3 = nlp.Dimension3;
                Measure = nlp.Measure;
                Measure2 = nlp.Measure2;
                Elements = nlp.Elements;
                Percentage = nlp.Percentage;
                Number = nlp.Number;
                ChartType = nlp.ChartType;
                DistanceKm = nlp.DistanceKm;
                Language = nlp.Language;
                DatePeriod = nlp.DatePeriod;
                DatePeriod1 = nlp.DatePeriod1;
                OriginalDatePeriod = nlp.OriginalDatePeriod;
                Date = nlp.Date;
                Response = nlp.Response;

                captureImageFileName = conNPrintingReportPath + "\\" + user.UserId + "\\" + (Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension)) + ".png";

                captureImageFileName = GenericMethods.RefineFileName(captureImageFileName);
                
                QlikSenseFilter[] qlikSenseFilters = new QlikSenseFilter[1];

                Application application = new Application();

                if (string.IsNullOrEmpty(Measure))
                    application.userHasAccess = true;
                else
                    application = CheckUserAccess(user, Measure, Measure2, Elements, IntentType);

                if (application.userHasAccess)
                {
                    try
                    {
                        if (DatePeriod != null)
                        {
                            if (DatePeriod.Count() == 2)
                            {
                                //2018-01-08/2018-01-14 date format
                                string tempStartDate = DateTime.ParseExact(DatePeriod[0], "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString(conQlikAppDateFormat);
                                string tempEndDate = DateTime.ParseExact(DatePeriod[1], "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString(conQlikAppDateFormat);

                                qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(tempStartDate);
                                if (qlikSenseFilters == null || qlikSenseFilters.Count() == 0)
                                    qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(tempEndDate);

                                foreach (QlikSenseFilter qlikSenseFilter in qlikSenseFilters)
                                    if (qlikSenseFilter != null && qlikSenseFilter.Dimension.ToLower() == Util.Parameters.getConfigParameters("conQlikAppDateField").ToLower())
                                    {
                                        Dates = GetDateRangeBetweenTwoDates(DatePeriod);
                                        user.QlikSenseApp.AddFilter(Util.Parameters.getConfigParameters("conQlikAppDateField"), OriginalDatePeriod[0], Dates, IntentType == IntentType.Filter ? false : true);
                                        break;
                                    }
                            }
                        }

                        if (DatePeriod1 != null)
                        {
                            if (DatePeriod1.Count() == 2)
                            {
                                //2018-01-08/2018-01-14 date format
                                string tempStartDate = DateTime.ParseExact(DatePeriod1[0], "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString(conQlikAppDateFormat);
                                string tempEndDate = DateTime.ParseExact(DatePeriod1[1], "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString(conQlikAppDateFormat);

                                qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(tempStartDate);
                                if (qlikSenseFilters == null || qlikSenseFilters.Count() == 0)
                                    qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(tempEndDate);

                                foreach (QlikSenseFilter qlikSenseFilter in qlikSenseFilters)
                                    if (qlikSenseFilter != null && qlikSenseFilter.Dimension.ToLower() == Util.Parameters.getConfigParameters("conQlikAppDateField").ToLower())
                                    {
                                        Dates = GetDateRangeBetweenTwoDates(DatePeriod1);
                                        user.QlikSenseApp.AddFilter(Util.Parameters.getConfigParameters("conQlikAppDateField"), OriginalDatePeriod[1], Dates, IntentType == IntentType.Filter ? false : true);
                                        break;
                                    }
                            }
                        }

                        foreach (var date in Date)
                        {
                            if (date != null)
                            {
                                Dates = new List<string>();
                                Dates.Add(DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString(conQlikAppDateFormat));

                                qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(Dates[0]);

                                foreach (QlikSenseFilter qlikSenseFilter in qlikSenseFilters)
                                    if (qlikSenseFilter != null && qlikSenseFilter.Dimension.ToLower() == Util.Parameters.getConfigParameters("conQlikAppDateField").ToLower())
                                    {
                                        user.QlikSenseApp.AddFilter(Util.Parameters.getConfigParameters("conQlikAppDateField"), date, Dates, IntentType == IntentType.Filter ? false : true);
                                        break;
                                    }
                            }
                        }

                        foreach (var element in Elements)
                        {
                            qlikSenseFilters = user.QlikSenseApp.QlikSenseSearch(element);

                            if (qlikSenseFilters[0] != null)
                                user.QlikSenseApp.AddFilter(qlikSenseFilters[0], IntentType == IntentType.Filter ? false : true);
                        }

                        if (!string.IsNullOrEmpty(IntentType.ToString()))
                            log.Info(module, method, "Identified intent: " + IntentType, true);

                        if (!string.IsNullOrEmpty(Measure))
                            log.Info(module, method, "Identified Measures: " + Measure + (!string.IsNullOrEmpty(Measure2) ? ", " + Measure2 : ""), true);

                        if (!string.IsNullOrEmpty(Dimension))
                            log.Info(module, method, "Identified Dimensions: " + Dimension + (!string.IsNullOrEmpty(Dimension2) ? ", " + Dimension2 : (!string.IsNullOrEmpty(Dimension3) ? ", " + Dimension3 : "")), true);

                        if (Elements.Count() > 0)
                            log.Info(module, method, "Identified elements: " + string.Join(",", Elements), true);

                        if (Number.Count() > 0)
                            log.Info(module, method, "Identified number: " + string.Join(",", Number), true);

                        if (user.TheBotIsAngry && IntentType != IntentType.Apologize)
                        {
                            Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.cnvApologizeOrNoAnswer);
                            return Resp;
                        }

                        switch (IntentType)
                        {
                            case IntentType.DirectResponse:
                                Resp.TextMessage = Response;
                                break;
                            case IntentType.Welcome:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.Welcome);
                                break;
                            case IntentType.Greetings:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.Greetings);
                                break;
                            case IntentType.Gratitude:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.Gratitude);
                                break;
                            case IntentType.GetStarted:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.GetStarted);
                                break;
                            case IntentType.KPI:
                                if (Measure != null)
                                {
                                    if (user.QlikSenseApp.lastFilters.Count() > 0)
                                        Resp.TextMessage = GetFilteredMeasure(user.QlikSenseApp, ref Measure, user.QlikSenseApp.lastFilters);
                                    else
                                        Resp.TextMessage = GetKpiValue(user.QlikSenseApp, ref Measure, user.UserName);

                                    foreach (var visualization in user.QlikSenseApp.masterVisualizations)
                                    {
                                        string formatType = "number", VizID = string.Empty, measureLargeValue = "good";
                                        foreach (var tag in visualization.Tags)
                                        {
                                            if (tag == "_formatMoney")
                                                formatType = "money";
                                            else if (tag == "_formatPercent")
                                                formatType = "percent";
                                            else if (tag == "_formatNumber")
                                                formatType = "number";
                                            else if (tag == "_badMeasure")
                                                measureLargeValue = "bad";

                                            if (tag == "_" + Measure.ToLower() + "Summary")
                                                VizID = visualization.Id;
                                        }

                                        if (!string.IsNullOrEmpty(formatType) && !string.IsNullOrEmpty(VizID))
                                        {
                                            user.QlikSenseApp.ClearAllFilter();
                                            var vizResponse = GetVisualization(user, CallBackAction.Visualization, VizID);
                                            Resp.ChartText = vizResponse.ChartText;
                                            Resp.ChartFound = vizResponse.ChartFound;
                                            Resp.ChartFound.ObjectId = VizID;
                                            Resp.VizID = vizResponse.VizID;
                                            string filename = conNPrintingReportPath + "\\" + user.UserId + "\\" + Resp.ChartFound.Description + ".png";
                                            Resp.NarrativeText = user.QlikSenseApp.getVizNarrative(VizID, Util.Parameters.getConfigParameters("conApiKey"), Convert.ToInt32(Util.Parameters.getConfigParameters("conVerbosity")), formatType, measureLargeValue, filename);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.nlMeasureNotFound);
                                    Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.nlMeasureNotFound);
                                }

                                break;

                            case IntentType.Chart:
                                if (Dimension != null && Measure != null)
                                {
                                    Resp.TextMessage = GetChart(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, ref Resp.VoiceMessage, ref Resp.ChartFound);
                                }
                                else if (Measure != null)
                                {
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiChartWithoutDimension);
                                    Resp.TextMessage += GetKpiValue(user.QlikSenseApp, ref Measure, user.UserName);
                                }
                                else if (Dimension != null)
                                {
                                    Measure = user.QlikSenseApp.lastMeasure.Name;
                                    Resp.TextMessage = GetChart(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, ref Resp.VoiceMessage, ref Resp.ChartFound);
                                }
                                break;

                            case IntentType.Measure4Element:
                                if (Measure != null && Dimension != null && Elements.Count() != 0)
                                {
                                    Resp.TextMessage = GetFilteredMeasure(user.QlikSenseApp, ref Measure, user.QlikSenseApp.lastFilters, user.UserName);
                                    break;
                                }

                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                if (user.QlikSenseApp.lastFilters.Count() > 0)
                                {
                                    if (Measure != null)
                                        Resp.TextMessage = GetFilteredMeasure(user.QlikSenseApp, ref Measure, user.QlikSenseApp.lastFilters, user.UserName);
                                }
                                else
                                {
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiM4EWithoutElement);
                                    //Resp.TextMessage += GetKpiValue(user.QlikSenseApp, ref Measure, user.UserName);
                                }
                                break;

                            case IntentType.Filter:
                                Resp.TextMessage = ApplyFilter(user.QlikSenseApp, false);
                                break;

                            case IntentType.RankingTop:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                Resp.TextMessage = GetRanking(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, true, NumberOfResults: Number.Count() > 0 ? Convert.ToInt32(Number[0]) : NumberOfResults);

                                user.QlikSenseApp.ClearAllFilter();

                                ChartType = "barchart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension);

                                if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                }

                                break;

                            case IntentType.RankingBottom:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                Resp.TextMessage = GetRanking(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, false, NumberOfResults: Number.Count() > 0 ? Convert.ToInt32(Number[0]) : NumberOfResults);

                                user.QlikSenseApp.ClearAllFilter();

                                ChartType = "barchart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension);

                                if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                }

                                break;

                            case IntentType.Alert:
                                if (Percentage != null)
                                {
                                    if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                    var a = new QlikSense.QlikSenseAlert();
                                    a.UserName = user.UserName;
                                    a.UserID = user.UserId;
                                    a.SendToID = user.UserId;
                                    a.AlertRequest = Measure + " " + randomResponse.GetRandomResponse(StringResources.kpiHasChangedMoreThan + " " + Percentage);
                                    user.QlikSenseApp.alertList.Add(a);

                                    Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.alertAdknowledge), a.UserName, a.AlertRequest);
                                }
                                break;

                            case IntentType.GoodAnswer:
                                Resp.TextMessage = ":-)";
                                break;

                            case IntentType.BadWords:
                                user.TheBotIsAngry = true;
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.cnvBadwords);
                                break;

                            case IntentType.Apologize:
                                user.TheBotIsAngry = false;
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.cnvApologizeAnswer);
                                break;

                            case IntentType.Hello:
                                Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.Welcome), user.UserName);
                                break;

                            case IntentType.Bye:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.Bye + " " + user.UserName);
                                Resp.TextMessage += "\n?";
                                break;

                            case IntentType.Reports:
                                Resp.OtherAction = "Reports";
                                break;

                            case IntentType.CreateChart:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Dimension2 == null) Dimension2 = "";
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;
                                if (ChartType == null) ChartType = "BarChart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension, Dimension2);
                                if (UserChart == "forbidden")
                                    Resp.TextMessage = Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.cnvCreateChartNoRights);
                                else if (UserChart == "")
                                    Resp.TextMessage = Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.cnvCreateChartError);
                                else
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.cnvCreateChartResult), user.UserName, url).Replace("LinkStart", "<").Replace("LinkEnd", ">");
                                    string strDim2 = Dimension2 == "" ? "" : (" " + randomResponse.GetRandomResponse(StringResources.nlAnd + " " + Dimension2));
                                    Resp.ChartFound = new QlikSenseObject
                                    {
                                        ObjectId = UserChart,
                                        ChartType = ChartType,
                                        ObjectURL = url,
                                        Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension + strDim2)
                                    };
                                }
                                break;

                            case IntentType.ShowAnalysis:
                                user.SummarizeHistory();
                                UserSheet = CreateSummarySheet(user);
                                if (UserSheet == "forbidden")
                                    Resp.TextMessage = Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.cnvAnalysisNoRights);
                                else if (UserSheet == "")
                                    Resp.TextMessage = Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.cnvAnalysisError);
                                else
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer;
                                    url += "/sense/app/" + user.QlikSenseApp.qlikSenseAppId + "/sheet/" + UserSheet + "/state/analysis";

                                    Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.cnvAnalysisResult), user.UserName, url).Replace("LinkStart", "<").Replace("LinkEnd", ">");
                                }

                                break;

                            case IntentType.ContactQlik:
                                url = "http://www.qlik.com/us/try-or-buy/buy-now";
                                Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.cnvContactQlik), user.UserName, url);
                                break;

                            case IntentType.Help:
                                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.UsageInstructions);
                                Resp.TextMessage += "\r\n\r\nQlik Sense App: " + user.QlikSenseApp.qlikSenseAppName;
                                Resp.OtherAction = "Help";
                                break;

                            case IntentType.ClearAllFilters:
                                Resp = ClearFilters(user);
                                break;

                            case IntentType.ClearDimensionFilter:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                Resp = ClearFilters(user, Dimension);
                                break;

                            case IntentType.CurrentSelections:
                                if (user.QlikSenseApp.lastFilters != null && user.QlikSenseApp.lastFilters.Count() > 0)
                                {
                                    string strFilter = "";

                                    foreach (QlikSenseFilter qsFF in user.QlikSenseApp.lastFilters)
                                    {
                                        if (qsFF != user.QlikSenseApp.lastFilters.First()) strFilter += ", " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");
                                        strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), qsFF.Dimension, qsFF.Element);
                                    }
                                    Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.cnvCurrentSelections), strFilter);
                                }
                                else
                                {
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.cnvCurrentSelectionsEmpty);
                                }
                                break;

                            case IntentType.ShowAllApps:
                                foreach (QlikSenseAppProperties qa in user.QlikSenseApp.qlikSenseAlternativeApps)
                                {
                                    Resp.Options.Add(
                                        new ResponseOptions()
                                        { Title = qa.AppName, ID = qa.AppID, Action = CallBackAction.OpenApp }
                                        );
                                }
                                if (Resp.Options.Count() > 0) Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.appSelectApp);
                                break;

                            case IntentType.ShowAllVisualizations:
                                foreach (QlikSenseMasterItem v in user.QlikSenseApp.masterVisualizations)
                                {
                                    if (v.Tags != null && !v.Tags.Contains("_exclude"))
                                    {
                                        string groupTag = v.Tags.Where(x => x.ToString().Contains("Group")).FirstOrDefault<string>();

                                        Resp.Options.Add(
                                        new ResponseOptions()
                                        { Title = v.Name, ID = v.Name, Group = string.IsNullOrEmpty(groupTag) ? string.Empty : groupTag, Action = CallBackAction.ShowVisualization }
                                        );
                                    }
                                }
                                if (Resp.Options.Count() > 0)
                                {
                                    Resp.Options = Resp.Options.OrderBy(o => o.Group).ToList();
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.selectVisualization);
                                }
                                else Resp.ErrorText = "No visualization found!!";
                                break;

                            case IntentType.ShowAllDimensions:
                                foreach (QlikSenseMasterItem d in user.QlikSenseApp.masterDimensions)
                                {
                                    Resp.Options.Add(
                                    new ResponseOptions()
                                    { Title = d.Name, ID = d.Name, Action = CallBackAction.ShowDimension }
                                    );
                                }
                                if (Resp.Options.Count() > 0) Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiSelectDimension);
                                break;

                            case IntentType.ShowAllMeasures:
                                foreach (QlikSenseMasterItem m in user.QlikSenseApp.masterMeasures)
                                {
                                    if (m.Tags != null && !m.Tags.Contains("_exclude") && !m.Tags.Contains("_kpi"))
                                    {
                                        string groupTag = m.Tags.Where(x => x.ToString().Contains("Group")).FirstOrDefault<string>();

                                        Resp.Options.Add(
                                          new ResponseOptions()
                                          { Title = m.Name, ID = m.Name, Group = string.IsNullOrEmpty(groupTag) ? string.Empty : groupTag, Action = CallBackAction.ShowMeasure }
                                          );
                                    }
                                }
                                if (Resp.Options.Count() > 0)
                                {
                                    Resp.Options = Resp.Options.OrderBy(o => o.Group).ToList();
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiSelectMeasure);
                                }
                                else Resp.ErrorText = "No measures found!!";
                                break;

                            case IntentType.ShowAllSheets:
                                foreach (QlikSenseSheet s in user.QlikSenseApp.sheets)
                                {
                                    Resp.Options.Add(
                                        new ResponseOptions()
                                        { Title = s.Name, ID = s.Id, Action = CallBackAction.ShowSheet }
                                        );
                                }
                                if (Resp.Options.Count() > 0) Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiSelectSheet);
                                break;

                            case IntentType.ShowAllStories:
                                foreach (QlikSenseStory s in user.QlikSenseApp.stories)
                                {
                                    Resp.Options.Add(
                                        new ResponseOptions()
                                        { Title = s.Name, ID = s.Id, Action = CallBackAction.ShowStory }
                                        );
                                }
                                if (Resp.Options.Count() > 0) Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiSelectStory);
                                break;

                            case IntentType.ShowKPIs:
                                foreach (QlikSenseMasterItem m in user.QlikSenseApp.masterMeasures)
                                {
                                    if (m.Tags != null && !m.Tags.Contains("_exclude") && m.Tags.Contains("_kpi"))
                                    {
                                        string groupTag = m.Tags.Where(x => x.ToString().ToLower().Contains("Group")).FirstOrDefault<string>();

                                        Resp.Options.Add(
                                          new ResponseOptions()
                                          { Title = m.Name, ID = m.Name, Group = string.IsNullOrEmpty(groupTag) ? string.Empty : groupTag, Action = CallBackAction.ShowMeasure }
                                          );
                                    }
                                }
                                if (Resp.Options.Count() > 0)
                                {
                                    Resp.Options = Resp.Options.OrderBy(o => o.Group).ToList();
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.kpiSelectMeasure).Replace("measures", "KPIs").Replace("measure", "KPI");
                                }
                                else Resp.ErrorText = "No KPIs found!!";
                                break;

                            case IntentType.ShowMeasureByMeasure:
                                if (Measure != null && Measure2 != null)
                                {
                                    if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                    if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;
                                    if (Measure2 == null) Measure2 = user.QlikSenseApp.lastMeasure.Name;

                                    if (user.QlikSenseApp.lastFilters.Count() > 0)
                                        Resp.TextMessage = GetFilteredMeasure(user.QlikSenseApp, ref Measure, ref Measure2, user.QlikSenseApp.lastFilters);
                                    else
                                        Resp.TextMessage = GetKpiValue(user.QlikSenseApp, ref Measure, ref Measure2, user.UserName);

                                    user.QlikSenseApp.ClearAllFilter();

                                    ChartType = "ScatterChart";

                                    UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension, measure2: Measure2);

                                    if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                    {
                                        url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                        foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                            url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                        Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " vs " + Measure2 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                        Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                    }
                                    break;
                                }
                                else
                                {
                                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.nlMeasureNotFound);
                                    Resp.ErrorText = randomResponse.GetRandomResponse(StringResources.nlMeasureNotFound);
                                }

                                break;

                            case IntentType.ShowListOfElements:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                Resp.TextMessage = GetFilteredList(user.QlikSenseApp, ref Dimension, user.QlikSenseApp.lastFilters, user.UserName);
                                break;

                            case IntentType.ShowElementsAboveValue:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                Resp.TextMessage = GetRanking(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, true, Number);

                                user.QlikSenseApp.ClearAllFilter();

                                ChartType = "barchart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension);

                                if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                }

                                break;

                            case IntentType.ShowElementsBelowValue:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                Resp.TextMessage = GetRanking(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, false, Number);

                                user.QlikSenseApp.ClearAllFilter();

                                ChartType = "barchart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension);

                                if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                }

                                break;

                            case IntentType.ShowElementsBetweenValues:
                                if (Dimension == null) Dimension = user.QlikSenseApp.lastDimension.Name;
                                if (Measure == null) Measure = user.QlikSenseApp.lastMeasure.Name;

                                Resp.TextMessage = GetRanking(user.QlikSenseApp, ref Measure, ref Dimension, user.UserName, false, Number);

                                user.QlikSenseApp.ClearAllFilter();

                                ChartType = "barchart";

                                UserChart = CreateChart(ref narrative, ref captureImageFileName, user, ChartType, Measure, Dimension);

                                if (!string.IsNullOrEmpty(UserChart) && UserChart != "forbidden")
                                {
                                    url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + UserChart + "&opt=currsel";

                                    foreach (QlikSenseFilter f in user.QlikSenseApp.lastFilters)
                                        url += "&select=" + Uri.EscapeDataString(f.DimensionExpression) + "," + Uri.EscapeDataString(f.Element);

                                    Resp.ChartFound = new QlikSenseObject { ObjectId = UserChart, ChartType = ChartType, ObjectURL = url, Description = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension) };
                                    Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), Resp.ChartFound.Description, "<" + url + "|" + Resp.ChartFound.Description + ">");
                                }

                                break;
                        }

                        if (!string.IsNullOrEmpty(narrative))
                            Resp.NarrativeText = narrative;

                        if (!string.IsNullOrEmpty(Dimension))
                            user.QlikSenseApp.lastDimension = user.QlikSenseApp.GetMasterDimension(Dimension);

                        if (!string.IsNullOrEmpty(Measure))
                            user.QlikSenseApp.lastMeasure = user.QlikSenseApp.GetMasterMeasure(Measure);

                        user.QlikSenseApp.lastFilters.RemoveAll(delegate (QlikSenseFilter remove) { return remove.isInLine == true; });
                    }
                    catch (Exception e)
                    {
                        log.Error(module, method, string.Format("Error trying to predict '{0}': {1}", inText, e), true);
                        Resp.ErrorText = string.Format("Error trying to predict '{0}': {1}", inText, e);
                    }
                }
                else
                {
                    //I'm sorry, but I doubt whether I'm supposed to provide you this information.
                    //I'm afraid, I might have to ask my boss whether I can give you this information.
                    //Based on the instructions I have received, you do not seem to have access to this details.
                    Resp.TextMessage = "Sorry, you are not authorized to get this information.";
                    log.Warn(module, method, "Sorry, you are not authorized to get this information.", true);
                }

            }
            else
            {
                Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.nlMessageNotManaged);
                Resp.WarningText = string.Format("Message not managed: {0}", inText);
                log.Warn(module, method, string.Format("Message not managed: {0}", inText), true);
            }

            nlp.Dimension = Dimension;
            nlp.Measure = Measure;
            nlp.Measure2 = Measure2;
            nlp.Elements = Elements;
            nlp.Percentage = Percentage;
            nlp.Number = Number;
            nlp.ChartType = ChartType;
            nlp.DistanceKm = DistanceKm;

            Resp.nlp = nlp;

            Intent pi = nlp.GetCopyOfPredictedIntent();

            user.ConversationHistory.Add(pi);
            user.LastResponse = Resp;
            if (Resp.ChartFound != null)
                user.LastChart = Resp.ChartFound;

            return Resp;
        }

        #region User restriction
        public class Application
        {
            public string Id = string.Empty;
            public string Name = string.Empty;
            public bool userHasAccess = true;
        }

        public class UserAccess
        {
            public string UserID = string.Empty;
            public string Name = string.Empty;
            public string EmailID = string.Empty;
            public List<string> Measures = new List<string>();
        }

        public Application CheckUserAccess(QlikSenseUser user, string measure, string measure2 = null, List<string> elements = null, IntentType intent = 0)
        {
            Application application = new Application();

            if (intent != IntentType.Alert && intent != IntentType.Apologize && intent != IntentType.BadWords && intent != IntentType.Bye && intent != IntentType.ClearAllFilters &&
                intent != IntentType.ClearDimensionFilter && intent != IntentType.ContactQlik && intent != IntentType.CurrentSelections && intent != IntentType.DirectResponse &&
                intent != IntentType.GoodAnswer && intent != IntentType.Hello && intent != IntentType.Help && intent != IntentType.None && intent != IntentType.Reports &&
                intent != IntentType.ShowAllApps && intent != IntentType.ShowAllDimensions && intent != IntentType.ShowAllMeasures && intent != IntentType.ShowAllSheets &&
                intent != IntentType.ShowAllStories && intent != IntentType.ShowAllVisualizations)
            {
                string conUserAccessCSV = ConfigurationManager.AppSettings["conUserAccessCSV"].ToString();

                var users = File.ReadAllLines(conUserAccessCSV, System.Text.Encoding.Default).Skip(0).Select(v => FromCsv(v)).ToList().Where(x => x.EmailID == user.EmailID.ToLower()).ToList();

                if (users.Count() > 0)
                {
                    if (!string.IsNullOrEmpty(user.UserId) && !string.IsNullOrEmpty(measure))
                    {
                        DataSet ds = new DataSet();

                        string conAppMetadataXlsx = ConfigurationManager.AppSettings["conAppMetadataXlsx"].ToString();

                        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(conAppMetadataXlsx, false))
                        {
                            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();

                            foreach (var sheet in sheets)
                            {
                                System.Data.DataTable dt = new System.Data.DataTable();
                                dt.TableName = sheet.Name;
                                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(sheet.Id.Value);
                                Worksheet workSheet = worksheetPart.Worksheet;
                                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                                foreach (Cell cell in rows.ElementAt(0))
                                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));

                                foreach (Row row in rows)
                                {
                                    DataRow dataRow = dt.NewRow();
                                    for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                                        dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                                    dt.Rows.Add(dataRow);
                                }

                                dt.Rows.RemoveAt(0);
                                ds.Tables.Add(dt);
                            }
                        }

                        DataTable dataTable = new DataTable();
                        string AppID = string.Empty, AppName = string.Empty;

                        if (!string.IsNullOrEmpty(measure2))
                        {
                            if (users[0].Measures.Contains(measure2, StringComparer.CurrentCultureIgnoreCase))
                                application.userHasAccess = true;
                            else
                                application.userHasAccess = false;

                            if (application.userHasAccess)
                            {
                                dataTable = ds.Tables["Measures"];
                                DataRow[] rows = dataTable.Select("Name = '" + measure2 + "'");

                                if (rows.Length > 0)
                                    AppID = rows[0]["App_ID"].ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(measure))
                        {
                            if (users[0].Measures.Contains(measure, StringComparer.CurrentCultureIgnoreCase))
                                application.userHasAccess = true;
                            else
                                application.userHasAccess = false;

                            if (application.userHasAccess)
                            {
                                dataTable = ds.Tables["Measures"];
                                DataRow[] rows = dataTable.Select("Name = '" + measure + "'");

                                if (rows.Length > 0)
                                {
                                    if (string.IsNullOrEmpty(rows[0]["App_ID"].ToString()))
                                        application.userHasAccess = false;
                                    else
                                    {
                                        if (string.IsNullOrEmpty(AppID))
                                            AppID = rows[0]["App_ID"].ToString();
                                        else if (AppID != rows[0]["App_ID"].ToString())
                                            application.userHasAccess = false;
                                    }
                                }
                            }
                        }

                        //if (!string.IsNullOrEmpty(dimension))
                        //{
                        //    if (application.userHasAccess)
                        //    {
                        //        dataTable = ds.Tables["Dimensions"];
                        //        DataRow[] rows = dataTable.Select("Name = '" + dimension + "'");

                        //        if (rows.Length > 0)
                        //            if (AppID != rows[0]["App_ID"].ToString())
                        //                application.userHasAccess = false;
                        //    }
                        //}

                        //foreach (var element in elements)
                        //{
                        //    if (application.userHasAccess)
                        //    {
                        //        dataTable = ds.Tables["Dimensions' Values"];
                        //        DataRow[] rows = dataTable.Select("Value = '" + element + "'");

                        //        if (rows.Length > 0)
                        //        {
                        //            if (rows.Length > 0)
                        //                if (AppID != rows[0]["App_ID"].ToString())
                        //                    application.userHasAccess = false;
                        //        }
                        //    }
                        //}

                        if (application.userHasAccess && !string.IsNullOrEmpty(AppID))
                        {
                            dataTable = ds.Tables["Application"];

                            DataRow[] rows = dataTable.Select("App_ID = '" + AppID + "'");
                            if (rows.Length > 0)
                            {
                                AppName = rows[0]["Name"].ToString();
                                application.Id = AppID;
                                application.Name = AppName;
                                application.userHasAccess = true;

                                if (user.QlikSenseApp.qlikSenseAppId != AppID)
                                {
                                    user.QlikSenseApp.qlikSenseAppId = AppID;
                                    user.QlikSenseApp.qlikSenseAppName = AppName;
                                    user.QlikSenseApp.QlikSenseOpenApp(isAppChanged: true);
                                }
                            }
                        }

                        return application;
                    }
                    else
                    {
                        application.userHasAccess = false;
                        return application;
                    }
                }
                else
                {
                    application.userHasAccess = false;
                    return application;
                }
            }
            else
            {
                application.userHasAccess = true;
                return application;
            }
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerText.ToString();

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public static UserAccess FromCsv(string csvLine)
        {
            UserAccess userAccess = new UserAccess();

            string[] values = csvLine.Split(',');

            int index = 0;
            userAccess.UserID = values[index].ToLower().Trim();
            userAccess.Name = values[++index].Trim();
            userAccess.EmailID = values[++index].Trim();
            userAccess.Measures = values[++index].Trim().Split(';').ToList();

            return userAccess;
        }
        #endregion

        public Response ClearFilters(QlikSenseUser user, string dimensionName = "")
        {
            Response Resp = new Response();
            try
            {
                string DimensionExpression = null;
                if (dimensionName != "")
                    DimensionExpression = user.QlikSenseApp.GetDimensionExpression(dimensionName);

                if (DimensionExpression == null)
                {
                    user.QlikSenseApp.lastFilters.Clear();
                    Resp.TextMessage = randomResponse.GetRandomResponse(StringResources.nlClearFilters);
                }
                else
                {
                    foreach (QlikSenseFilter ff in user.QlikSenseApp.lastFilters)
                    {
                        if (ff.Dimension.Trim().ToLower() == dimensionName.Trim().ToLower())
                        {
                            user.QlikSenseApp.lastFilters.Remove(ff);
                            Resp.TextMessage = String.Format(randomResponse.GetRandomResponse(StringResources.nlClearOneFilter), ff.Dimension);
                            break;
                        }
                        Resp.TextMessage = String.Format(randomResponse.GetRandomResponse(StringResources.nlNoFilterToClear), dimensionName);
                    }
                }
            }
            catch (Exception e)
            {
                Resp.ErrorText = string.Format("Error in ClearFilters() for DimensionName '{0}': {1}", dimensionName, e);
            }

            return Resp;
        }

        public string CreateSummarySheet(QlikSenseUser user)
        {
            method = "CreateSummarySheet";

            string SheetID = "BotSumary-" + user.UserId;
            string narrative = string.Empty;

            log.Info(module, method, "Summary sheet ID: " + SheetID, true);

            if (user.QlikSenseApp.masterMeasures.Count < 4 || user.QlikSenseApp.masterDimensions.Count < 4) return "";

            try
            {
                string Meas0 = (user.ConversationSummary.Measure.Count > 0) ? user.ConversationSummary.Measure[0].Measure : user.QlikSenseApp.masterMeasures[0].Name;
                string Meas1 = (user.ConversationSummary.Measure.Count > 1) ? user.ConversationSummary.Measure[1].Measure : user.QlikSenseApp.masterMeasures[1].Name;
                string Meas2 = (user.ConversationSummary.Measure.Count > 2) ? user.ConversationSummary.Measure[2].Measure : user.QlikSenseApp.masterMeasures[2].Name;
                string Meas3 = (user.ConversationSummary.Measure.Count > 3) ? user.ConversationSummary.Measure[3].Measure : user.QlikSenseApp.masterMeasures[3].Name;

                string MxDDim0 = (user.ConversationSummary.MeasureByDimension.Count > 0) ? user.ConversationSummary.MeasureByDimension[0].Dimension : user.QlikSenseApp.masterDimensions[0].Name;
                string MxDDim1 = (user.ConversationSummary.MeasureByDimension.Count > 1) ? user.ConversationSummary.MeasureByDimension[1].Dimension : user.QlikSenseApp.masterDimensions[1].Name;
                string MxDDim2 = (user.ConversationSummary.MeasureByDimension.Count > 2) ? user.ConversationSummary.MeasureByDimension[2].Dimension : user.QlikSenseApp.masterDimensions[2].Name;
                string MxDDim3 = (user.ConversationSummary.MeasureByDimension.Count > 3) ? user.ConversationSummary.MeasureByDimension[3].Dimension : user.QlikSenseApp.masterDimensions[3].Name;

                string MxDMeas0 = (user.ConversationSummary.MeasureByDimension.Count > 0) ? user.ConversationSummary.MeasureByDimension[0].Measure : user.QlikSenseApp.masterMeasures[0].Name;
                string MxDMeas1 = (user.ConversationSummary.MeasureByDimension.Count > 1) ? user.ConversationSummary.MeasureByDimension[1].Measure : user.QlikSenseApp.masterMeasures[1].Name;
                string MxDMeas2 = (user.ConversationSummary.MeasureByDimension.Count > 2) ? user.ConversationSummary.MeasureByDimension[2].Measure : user.QlikSenseApp.masterMeasures[2].Name;
                string MxDMeas3 = (user.ConversationSummary.MeasureByDimension.Count > 3) ? user.ConversationSummary.MeasureByDimension[3].Measure : user.QlikSenseApp.masterMeasures[3].Name;

                string FilDim0 = (user.ConversationSummary.Filter.Count > 0) ? user.ConversationSummary.Filter[0].Dimension : user.QlikSenseApp.masterDimensions[0].Name;
                string FilDim1 = (user.ConversationSummary.Filter.Count > 1) ? user.ConversationSummary.Filter[1].Dimension : user.QlikSenseApp.masterDimensions[1].Name;
                string FilDim2 = (user.ConversationSummary.Filter.Count > 2) ? user.ConversationSummary.Filter[2].Dimension : user.QlikSenseApp.masterDimensions[2].Name;
                string FilDim3 = (user.ConversationSummary.Filter.Count > 3) ? user.ConversationSummary.Filter[3].Dimension : user.QlikSenseApp.masterDimensions[3].Name;

                string MxEDim0 = (user.ConversationSummary.MeasureForElement.Count > 0) ? user.ConversationSummary.MeasureForElement[0].Dimension : user.QlikSenseApp.masterDimensions[0].Name;
                string MxEDim1 = (user.ConversationSummary.MeasureForElement.Count > 1) ? user.ConversationSummary.MeasureForElement[1].Dimension : user.QlikSenseApp.masterDimensions[1].Name;
                string MxEDim2 = (user.ConversationSummary.MeasureForElement.Count > 2) ? user.ConversationSummary.MeasureForElement[2].Dimension : user.QlikSenseApp.masterDimensions[2].Name;
                string MxEDim3 = (user.ConversationSummary.MeasureForElement.Count > 3) ? user.ConversationSummary.MeasureForElement[3].Dimension : user.QlikSenseApp.masterDimensions[3].Name;

                string MxEMeas0 = (user.ConversationSummary.MeasureForElement.Count > 0) ? user.ConversationSummary.MeasureForElement[0].Measure : user.QlikSenseApp.masterMeasures[0].Name;
                string MxEMeas1 = (user.ConversationSummary.MeasureForElement.Count > 1) ? user.ConversationSummary.MeasureForElement[1].Measure : user.QlikSenseApp.masterMeasures[1].Name;
                string MxEMeas2 = (user.ConversationSummary.MeasureForElement.Count > 2) ? user.ConversationSummary.MeasureForElement[2].Measure : user.QlikSenseApp.masterMeasures[2].Name;
                string MxEMeas3 = (user.ConversationSummary.MeasureForElement.Count > 3) ? user.ConversationSummary.MeasureForElement[3].Measure : user.QlikSenseApp.masterMeasures[3].Name;

                string TopDim0 = (user.ConversationSummary.Top.Count > 0) ? user.ConversationSummary.Top[0].Dimension : user.QlikSenseApp.masterDimensions[0].Name;
                string TopDim1 = (user.ConversationSummary.Top.Count > 1) ? user.ConversationSummary.Top[1].Dimension : user.QlikSenseApp.masterDimensions[1].Name;
                string TopDim2 = (user.ConversationSummary.Top.Count > 2) ? user.ConversationSummary.Top[2].Dimension : user.QlikSenseApp.masterDimensions[2].Name;
                string TopDim3 = (user.ConversationSummary.Top.Count > 3) ? user.ConversationSummary.Top[3].Dimension : user.QlikSenseApp.masterDimensions[3].Name;

                string TopMeas0 = (user.ConversationSummary.Top.Count > 0) ? user.ConversationSummary.Top[0].Measure : user.QlikSenseApp.masterMeasures[0].Name;
                string TopMeas1 = (user.ConversationSummary.Top.Count > 1) ? user.ConversationSummary.Top[1].Measure : user.QlikSenseApp.masterMeasures[1].Name;
                string TopMeas2 = (user.ConversationSummary.Top.Count > 2) ? user.ConversationSummary.Top[2].Measure : user.QlikSenseApp.masterMeasures[2].Name;
                string TopMeas3 = (user.ConversationSummary.Top.Count > 3) ? user.ConversationSummary.Top[3].Measure : user.QlikSenseApp.masterMeasures[3].Name;

                string BottomDim0 = (user.ConversationSummary.Bottom.Count > 0) ? user.ConversationSummary.Bottom[0].Dimension : user.QlikSenseApp.masterDimensions[0].Name;
                string BottomDim1 = (user.ConversationSummary.Bottom.Count > 1) ? user.ConversationSummary.Bottom[1].Dimension : user.QlikSenseApp.masterDimensions[1].Name;
                string BottomDim2 = (user.ConversationSummary.Bottom.Count > 2) ? user.ConversationSummary.Bottom[2].Dimension : user.QlikSenseApp.masterDimensions[2].Name;
                string BottomDim3 = (user.ConversationSummary.Bottom.Count > 3) ? user.ConversationSummary.Bottom[3].Dimension : user.QlikSenseApp.masterDimensions[3].Name;

                string BottomMeas0 = (user.ConversationSummary.Bottom.Count > 0) ? user.ConversationSummary.Bottom[0].Measure : user.QlikSenseApp.masterMeasures[0].Name;
                string BottomMeas1 = (user.ConversationSummary.Bottom.Count > 1) ? user.ConversationSummary.Bottom[1].Measure : user.QlikSenseApp.masterMeasures[1].Name;
                string BottomMeas2 = (user.ConversationSummary.Bottom.Count > 2) ? user.ConversationSummary.Bottom[2].Measure : user.QlikSenseApp.masterMeasures[2].Name;
                string BottomMeas3 = (user.ConversationSummary.Bottom.Count > 3) ? user.ConversationSummary.Bottom[3].Measure : user.QlikSenseApp.masterMeasures[3].Name;


                user.QlikSenseApp.QlikSenseCreateSheet(SheetID, string.Format(randomResponse.GetRandomResponse(StringResources.cnvAnalysisSheetTitle), user.UserName),
                    string.Format("Sheet for Bot user {0}", user.UserName));

                user.QlikSenseApp.QlikSenseCreateKPI(SheetID, SheetID + "kpi1", Meas0, Meas2);

                user.QlikSenseApp.QlikSenseCreateKPI(SheetID, SheetID + "kpi2", Meas1, Meas3);

                user.QlikSenseApp.QlikSenseCreatePivotTableChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Pivotchart1", MxEDim0, MxEDim1, MxEDim2, MxEMeas0, MxEMeas1, MxEMeas2
                    , MxEMeas0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxEDim0));

                user.QlikSenseApp.QlikSenseCreateBarChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Barchart1", MxDDim0, MxDMeas0, MxDMeas0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxDDim0));
                user.QlikSenseApp.QlikSenseCreateBarChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Barchart2", MxDDim1, MxDMeas1, MxDMeas1 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxDDim1));

                user.QlikSenseApp.QlikSenseCreateLineChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Linechart1", MxDDim2, MxDMeas2, MxDMeas2 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxDDim2));
                if (user.QlikSenseApp.IsGeoLocationActive())
                    user.QlikSenseApp.QlikSenseCreateMapPoint(SheetID, SheetID + "mappoint1", MxDDim0, MxDMeas0, MxDMeas0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxDDim0));
                //Usr.QlikSense.QlikSenseCreateTreeChart(SheetID, SheetID + "Treechart1", TopDim0, TopDim1, TopMeas0, TopMeas0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + TopDim0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + TopDim1);
                user.QlikSenseApp.QlikSenseCreateTreeChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Treechart1", TopDim0, TopMeas0, TopMeas0 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + TopDim0));
                user.QlikSenseApp.QlikSenseCreateFilterPane(SheetID, SheetID + "filter1", FilDim0, FilDim1, FilDim2, FilDim0 + " - " + FilDim1 + " - " + FilDim2);

                user.QlikSenseApp.QlikSenseCreatePieChart(ref narrative, ref captureImageFileName, SheetID, SheetID + "Piechart1", MxDDim3, MxDMeas3, MxDMeas3 + " " + randomResponse.GetRandomResponse(StringResources.By + " " + MxDDim3));

                user.QlikSenseApp.QlikSenseCreateTextImage(SheetID, SheetID + "Text2", randomResponse.GetRandomResponse(StringResources.cnvAnalysisSheetTextboxTitle),
                    randomResponse.GetRandomResponse(StringResources.cnvAnalysisSheetTextboxText));

                user.QlikSenseApp.QlikSenseDoSave();
            }
            catch (Exception e)
            {
                method = "CreateSummarySheet";
                log.Error(module, method, "Error creating summary sheet.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                SheetID = "";

                if (e.Message.ToLower().StartsWith("forbidden")) SheetID = "forbidden";
            }

            return SheetID;
        }

        public string CreateChart(ref string narrative, ref string imageFileName, QlikSenseUser user, string chartType, string measure, string dimension
            , string dimension2 = "", string dimension3 = "", string measure2 = "", string measure3 = "", string element = "")
        {

            method = "CreateChart";

            string chartNarrative = string.Empty;

            string SheetID = "BotChart-" + user.UserId.ToLower();
            string ChartID = SheetID + "-UserChart-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");

            log.Info(module, method, "Sheet ID: " + SheetID, true);
            log.Info(module, method, "Chart ID: " + ChartID, true);

            try
            {
                user.QlikSenseApp.QlikSenseRemoveSheet(SheetID);
                user.QlikSenseApp.QlikSenseDoSave();

                user.QlikSenseApp.QlikSenseCreateSheet(SheetID, string.Format(randomResponse.GetRandomResponse(StringResources.cnvCreateChartSheetTitle), user.UserName),
                    string.Format(randomResponse.GetRandomResponse(StringResources.cnvCreateChartSheetDescription), user.UserName));

                chartType = chartType.Trim().ToLower();
                if (chartType.Contains("barchart"))
                    user.QlikSenseApp.QlikSenseCreateBarChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure);

                else if (chartType.Contains("linechart"))
                    user.QlikSenseApp.QlikSenseCreateLineChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure);

                else if (chartType.Contains("piechart"))
                    user.QlikSenseApp.QlikSenseCreatePieChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure);

                else if (chartType.Contains("treemap"))
                    user.QlikSenseApp.QlikSenseCreateTreeChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure);

                else if (chartType.Contains("pivotchart"))
                    user.QlikSenseApp.QlikSenseCreatePivotTableChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, dimension2, dimension3, measure);

                else if (chartType.Contains("mapchart"))
                    user.QlikSenseApp.QlikSenseCreateMapPoint(SheetID, ChartID, dimension, measure);

                else if (chartType.Contains("scatterchart"))
                    user.QlikSenseApp.QlikSenseCreateScatterChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure, measure2, measure3);

                else
                    user.QlikSenseApp.QlikSenseCreateBarChart(ref chartNarrative, ref imageFileName, SheetID, ChartID, dimension, measure);

                user.QlikSenseApp.QlikSenseDoSave();

                narrative = chartNarrative;
            }
            catch (Exception e)
            {
                method = "CreateChart";
                log.Error(module, method, "Error creating chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                ChartID = "";

                if (e.Message.ToLower().StartsWith("forbidden")) ChartID = "forbidden";
            }

            return ChartID;
        }

        private async Task<bool> UnderstandSentence(QlikSenseUser Usr, string Message, bool isDialogflow = true)
        {
            method = "UnderstandSentence";

            bool Predicted;

            Predicted = DetectDirectCommand(Message);

            log.Info(module, method, "Is message direct command? - " + (Predicted ? "Yes" : "No"), true);

            if (!Predicted)
                Predicted = await nlp.Predict(Message, isDialogflow);

            log.Info(module, method, isDialogflow ? "Is message identified by Dialogflow? - " + (Predicted ? "Yes" : "No") : "Is message identified by LUIS? - " + (Predicted ? "Yes" : "No"), true);

            if (Predicted && nlp.IntentType != IntentType.None)
                return Predicted;

            string[] Words = Message.Split(' ');
            QlikSenseMasterItem dim = null;
            QlikSenseMasterItem mea = null;

            foreach (string w in Words)
            {
                if (dim == null)
                    dim = Usr.QlikSenseApp.masterDimensions.Find(d => d.Name.ToLower().Trim() == w.ToLower().Trim());
                if (mea == null)
                    mea = Usr.QlikSenseApp.masterMeasures.Find(m => m.Name.ToLower().Trim() == w.ToLower().Trim());
            }

            if (dim != null) nlp.Dimension = dim.Name;
            if (mea != null) nlp.Measure = mea.Name;

            if (dim != null && mea != null)
            {
                nlp.IntentType = IntentType.Chart;
                Predicted = true;
            }
            else if (dim == null && mea != null)
            {
                nlp.IntentType = IntentType.KPI;
                Predicted = true;
            }
            else if (dim != null && mea == null)
            {
                nlp.IntentType = IntentType.Chart;
                nlp.Measure = Usr.QlikSenseApp.lastMeasure.Name;
                Predicted = true;
            }
            else
            {
                nlp.IntentType = IntentType.None;
                Predicted = false;
            }

            return Predicted;
        }

        private static bool DetectDirectCommand(string Message)
        {
            bool Detected = false;

            if (Message.StartsWith("#") && Message.EndsWith("#"))
            {
                Detected = true;
                Message = Message.Replace("#", "");
                if (Message == IntentType.ShowAnalysis.ToString())
                {
                    nlp.IntentType = IntentType.ShowAnalysis;
                }
                else
                    nlp.IntentType = IntentType.None;
            }
            else
                Detected = false;

            return Detected;
        }

        private static string GetMeasureValue(QlikSenseApp qa, string Measure, string UserName = "")
        {
            string msg;
            QlikSenseMasterItem m;

            m = qa.GetMasterMeasure(Measure);

            if (Measure == null || m == null)
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiUnknownMessage), Measure);
            }
            else
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiValueMessage), m.Name, m.FormattedExpression);

                qa.masterMeasures.Remove(m);
                qa.masterMeasures.Insert(0, m);
            }

            return msg;
        }

        private static string GetMeasureValue(QlikSenseApp qa, string Measure, string Measure2, string UserName = "")
        {
            string msg;
            QlikSenseMasterItem m, m2;

            m = qa.GetMasterMeasure(Measure);
            m2 = qa.GetMasterMeasure(Measure2);

            if ((Measure == null || m == null) && (Measure2 == null || m2 == null))
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpisUnknownMessage), Measure, Measure2);
            }
            else
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpisValueMessage), m.Name, m.FormattedExpression, m2.Name, m2.FormattedExpression);

                qa.masterMeasures.Remove(m);
                qa.masterMeasures.Insert(0, m);
                qa.masterMeasures.Remove(m2);
                qa.masterMeasures.Insert(0, m2);
            }

            return msg;
        }

        private static string GetKpiValue(QlikSenseApp qa, ref string Measure, string UserName = "")
        {
            string msg;

            if (Measure == null || Measure.Trim() == "")
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiUnknownMessage), Measure);
            }
            else
            {
                QlikSenseMasterItem m = qa.GetMasterMeasure(Measure);
                if (m != null)
                {
                    Measure = m.Name;
                    msg = GetMeasureValue(qa, Measure, UserName);

                    qa.masterMeasures.Remove(m);
                    qa.masterMeasures.Insert(0, m);
                }
                else
                {
                    msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiUnknownMessage), Measure);
                }
            }

            return msg;
        }

        private static string GetKpiValue(QlikSenseApp qa, ref string Measure, ref string Measure2, string UserName = "")
        {
            string msg;

            if ((Measure == null || Measure.Trim() == "") && (Measure2 == null || Measure2.Trim() == ""))
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpisUnknownMessage), Measure, Measure2);
            }
            else
            {
                QlikSenseMasterItem m = qa.GetMasterMeasure(Measure);
                QlikSenseMasterItem m2 = qa.GetMasterMeasure(Measure2);

                if (m != null && m2 != null)
                {
                    Measure = m.Name;
                    Measure2 = m2.Name;
                    msg = GetMeasureValue(qa, Measure, Measure2, UserName);

                    qa.masterMeasures.Remove(m);
                    qa.masterMeasures.Insert(0, m);
                    qa.masterMeasures.Remove(m2);
                    qa.masterMeasures.Insert(0, m2);
                }
                else
                {
                    msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiUnknownMessage), Measure);
                }
            }

            return msg;
        }

        private static string GetChart(QlikSenseApp qa, ref string Measure, ref string Dimension, string UserName, ref string MsgToSpeak, ref QlikSenseObject ChartFound)
        {
            string msg = "";
            QlikSenseObject[] qsFounds;
            string qsQuery;

            string AppDimension;
            QlikSenseMasterItem d = qa.GetMasterDimension(Dimension);
            if (d != null)
            {
                AppDimension = d.Expression;
            }
            else
            {
                AppDimension = qa.QlikSenseFindField(Dimension);
            }

            string AppExpression = "";
            QlikSenseMasterItem m = qa.GetMasterMeasure(Measure);
            if (m != null)
            {
                AppExpression = m.Expression;
            }

            qsQuery = Measure + " " + randomResponse.GetRandomResponse(StringResources.By + " " + Dimension);

            if (AppDimension.Length > 0 && AppExpression.Length > 0)
            {
                Measure = m.Name;
                Dimension = d.Name;

                try
                {
                    qsFounds = qa.QlikSenseSearchObjects(qsQuery, true);
                }
                catch (Exception e)
                {
                    //BotLog.AddBotLine(string.Format("{0} Exception caught.", e), LogFile.LogType.logError);
                    qsFounds = new QlikSenseObject[0];
                }

                QlikSenseDataList[] dl = qa.GetDataList(m, AppDimension, NoOfRows: 50);
                qa.lastDimension = d;
                qa.lastMeasure = m;

                if (dl.Count() > 0)
                {
                    msg += InterpretData(qa, dl, Measure, Dimension, AppExpression, AppDimension);

                    string msgNLG = "";

                    if (msgNLG != "")
                    {
                        msg += "\n\n" + msgNLG;
                        MsgToSpeak = msgNLG;
                    }

                }

                if (qsFounds.Length > 0)
                {
                    QlikSenseObject f = qsFounds.First();

                    msg += "\n\n";
                    msg += string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), f.Description, f.HRef);
                    ChartFound = f;
                }
            }
            else
            {
                msg = randomResponse.GetRandomResponse(StringResources.nlGetChartNotFound);
            }

            return msg;
        }

        private static string GetTextChart(QlikSenseDataList[] Data, int NumBuckets = 5)
        {
            string TextChart = "";
            string BarText = "";
            int BarUnits = 0;
            int i = 0;

            int NofElements = Data.Length;
            double MinVal = 0;
            double MaxVal = 0;

            i = 0;
            foreach (QlikSenseDataList d in Data)
            {
                i += 1;
                //if (i > NumBuckets) break;
                if (i == 1 || d.MeasValue < MinVal) MinVal = d.MeasValue;
                if (i == 1 || d.MeasValue > MaxVal) MaxVal = d.MeasValue;
            }

            i = 0;
            foreach (QlikSenseDataList d in Data)
            {
                i += 1;
                //if (i > NumBuckets) break;

                //BarText = "#";
                //BarUnits = 0;
                //if (MaxVal > MinVal && !double.IsNaN(d.MeasValue))
                //    BarUnits = Convert.ToInt32(Math.Round((d.MeasValue - MinVal) / (MaxVal - MinVal) * (NumBuckets)));
                //BarText += "#".PadRight(BarUnits, '#');

                if (i > 1) TextChart += "\r\n";
                TextChart += d.DimValue + "\t:\t" + d.MeasFormattedValue;

                //TextChart += BarText + "\t";
                //TextChart += BarText.PadRight(NumBuckets + 4, ' ');
            }

            return TextChart;
        }

        public static string GetRanking(QlikSenseApp qliksenseApp, ref string Measure, ref string Dimension, string UserName,
            bool Descending = true, List<double?> MeasureThreshold = null, int NumberOfResults = 0)
        {
            string msg = "";

            string dimensionExpression;
            QlikSenseMasterItem masterDimension = qliksenseApp.GetMasterDimension(Dimension);
            if (masterDimension == null)
                dimensionExpression = qliksenseApp.GetDimensionExpression(Dimension);
            else
                dimensionExpression = masterDimension.Expression;

            string measureExpression = "";
            QlikSenseMasterItem masterMeasure = qliksenseApp.GetMasterMeasure(Measure);
            if (masterMeasure != null)
                measureExpression = masterMeasure.Expression;

            if (dimensionExpression.Length > 0 && measureExpression.Length > 0)
            {
                Measure = masterMeasure.Name;
                Dimension = masterDimension.Name;

                QlikSenseDataList[] dataList = qliksenseApp.GetDataList(masterMeasure, dimensionExpression, qliksenseApp.lastFilters, NumberOfResults, Descending, MeasureThreshold);
                if (masterDimension != null)
                    qliksenseApp.lastDimension = masterDimension;
                if (masterMeasure != null)
                    qliksenseApp.lastMeasure = masterMeasure;

                if (dataList.Count() > 0)
                    msg += InterpretDataRanking(qliksenseApp, dataList, Measure, Dimension, measureExpression, dimensionExpression, Descending, MeasureThreshold);
                else
                    msg += randomResponse.GetRandomResponse(StringResources.nlGetListNotFound);
            }
            else
            {
                msg = randomResponse.GetRandomResponse(StringResources.nlGetChartNotFound);
            }

            return msg;
        }

        private static string InterpretData(QlikSenseApp qa, QlikSenseDataList[] gl, string Measure, string Dimension, string AppExpression, string AppDimension, bool Descending = true)
        {
            string msg = "";

            string Total = qa.GetExpressionFormattedValue(Measure: AppExpression, Label: Measure);

            if (Descending)
            {
                msg += String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataDescending), Measure, Total, Dimension, gl.First().DimValue, gl.First().MeasFormattedValue);
            }
            else
            {
                msg += String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataAscending), Measure, Total, Dimension, gl.First().DimValue, gl.First().MeasFormattedValue);
            }

            if (gl.Count() > 1)
            {
                if (!double.IsNaN(gl[1].MeasValue))
                {
                    if (Descending)
                    {
                        msg += " " + String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataNextDescending),
                            gl[1].DimValue, gl[1].MeasFormattedValue, (1 - gl[1].MeasValue / gl[0].MeasValue).ToString("P1"));
                    }
                    else
                    {
                        msg += " " + String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataNextAscending),
                            gl[1].DimValue, gl[1].MeasFormattedValue, (1 - gl[1].MeasValue / gl[0].MeasValue).ToString("P1"));
                    }
                }
            }

            return msg;
        }

        private static string InterpretDataRanking(QlikSenseApp qa, QlikSenseDataList[] gl, string Measure, string Dimension, string AppExpression,
            string AppDimension, bool Descending = true, List<double?> MeasureThreshold = null)
        {
            string msg = "";

            if (Descending)
            {
                msg += String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataRankingDescending), Dimension, Measure);
            }
            else
            {
                if (MeasureThreshold?.Count == 2)
                    msg += String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataRankingForBetweenAscending), Dimension, Measure);
                else
                    msg += String.Format(randomResponse.GetRandomResponse(StringResources.nlgInterpretDataRankingAscending), Dimension, Measure);
            }

            msg += "\r\n" + GetTextChart(gl, NumberOfResults);


            string strFilter = "";

            if (MeasureThreshold != null && MeasureThreshold.Count() > 0)
            {
                double t = MeasureThreshold[0].Value;

                if (Descending)
                    strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementAboveValue), Measure, Math.Round(t).ToString("N0"));
                else
                {
                    if (MeasureThreshold?.Count == 2)
                    {
                        double t2 = MeasureThreshold[1].Value;
                        strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementBetweenValue), Measure, Math.Round(t).ToString("N0"), Math.Round(t2).ToString("N0"));
                    }
                    else
                        strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementBelowValue), Measure, Math.Round(t).ToString("N0"));
                }
            }


            foreach (QlikSenseFilter ff in qa.lastFilters)
            {
                if (ff == qa.lastFilters.First())
                {
                    if (strFilter.Length > 0) strFilter += "\r\n";
                }
                else
                    strFilter += ", " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");

                strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element);
            }
            if (strFilter != "")
                msg += "\r\n\r\n(" + strFilter + ")";

            return msg;
        }

        private static string GetFilteredMeasure(QlikSenseApp qa, ref string Measure, List<QlikSenseFilter> Filters = null, string UserName = "")
        {
            string msg = "";

            string AppExpression = "";
            QlikSenseMasterItem m = qa.GetMasterMeasure(Measure);

            if (Measure == null || m == null)
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiUnknownMessage), Measure);
            }
            else if (Filters != null && Filters.Count() > 0)
            {
                AppExpression = m.Expression;
                Measure = m.Name;

                if (AppExpression.Length > 0)
                {
                    string val = qa.GetExpressionFormattedValue(AppExpression, Filters, Measure);
                    string strFilter = "";

                    foreach (QlikSenseFilter ff in Filters)
                    {
                        /*  if (ff != Filters.First())
                              strFilter += ", ";
                          else if(ff != Filters.First() && ff == Filters.Last())
                        */
                        //strFilter += randomResponse.GetRandomResponse(StringResources.nlAnd + " ";
                        if (Filters.Count > 1 && ff == Filters.Last())
                            strFilter += " " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");

                        strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element);
                        if (Filters.Count > 2 && ff != Filters.ElementAt(Filters.Count - 2) && ff != Filters.Last())
                            strFilter += ", ";
                    }
                    msg = string.Format(randomResponse.GetRandomResponse(randomResponse.GetRandomResponse(StringResources.nlGetElement)), Measure, strFilter, Measure, val);
                }
                else
                {
                    msg = randomResponse.GetRandomResponse(StringResources.nlGetElementNotFound);
                }
            }
            else
            {
                msg = string.Format(randomResponse.GetRandomResponse(randomResponse.GetRandomResponse(StringResources.kpiValueMessage)), m.Name, m.FormattedExpression);
            }

            return msg;
        }

        private static string GetFilteredMeasure(QlikSenseApp qa, ref string Measure, ref string Measure2, List<QlikSenseFilter> Filters = null, string UserName = "")
        {
            string msg = "";

            string AppExpression = "";
            QlikSenseMasterItem m = qa.GetMasterMeasure(Measure);
            QlikSenseMasterItem m2 = qa.GetMasterMeasure(Measure2);

            if ((Measure == null || m == null) && (Measure == null || m == null))
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpisUnknownMessage), Measure, Measure2);
            }
            else if (Filters != null && Filters.Count() > 0)
            {
                AppExpression = m.Expression;
                Measure = m.Name;
                Measure2 = m2.Name;

                if (AppExpression.Length > 0)
                {
                    string val = qa.GetExpressionFormattedValue(m.Expression, Filters, Measure);
                    string val2 = qa.GetExpressionFormattedValue(m2.Expression, Filters, Measure2);
                    string strFilter = "";

                    foreach (QlikSenseFilter ff in Filters)
                    {
                        if (ff != Filters.First()) strFilter += ", " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");
                        strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element);
                    }
                    msg = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetM4MElement), Measure, Measure2, strFilter, val, val2);
                }
                else
                {
                    msg = randomResponse.GetRandomResponse(StringResources.nlGetElementNotFound);
                }
            }
            else
            {
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.kpiValueMessage), m.Name, m.FormattedExpression);
            }

            return msg;
        }

        private static string GetFilteredList(QlikSenseApp qa, ref string Dimension, List<QlikSenseFilter> Filters = null, string UserName = "")
        {
            string msg = "";

            string AppDimension;
            QlikSenseMasterItem d = qa.GetMasterDimension(Dimension);
            if (d == null)
                AppDimension = qa.GetDimensionExpression(Dimension);
            else
            {
                AppDimension = d.Expression;
            }

            QlikSenseMasterItem m = new QlikSenseMasterItem() { Expression = "=1", Name = string.Empty };
            //string AppExpression = "=1";

            if (AppDimension.Length > 0)
            {
                Dimension = d.Name;

                QlikSenseDataList[] dl = qa.GetDataList(m, AppDimension, qa.lastFilters, NumberOfResults * 2);
                if (d != null)
                    qa.lastDimension = d;

                if (dl.Count() > 0)
                {
                    msg += CreateListOfElements(qa, dl, Dimension);
                }
            }
            else
            {
                msg = randomResponse.GetRandomResponse(StringResources.nlGetChartNotFound);
            }

            return msg;
        }

        private static string CreateListOfElements(QlikSenseApp qa, QlikSenseDataList[] gl, string Dimension)
        {
            string msg = "";
            msg += string.Format(randomResponse.GetRandomResponse(StringResources.nlgShowListOfElements), Dimension, NumberOfResults * 2);

            msg += "\r\n" + GetTextList(gl, NumberOfResults * 2);

            string strFilter = "";
            foreach (QlikSenseFilter ff in qa.lastFilters)
            {
                if (ff != qa.lastFilters.First()) strFilter += ", " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");
                strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element);
            }
            if (strFilter != "")
                msg += "\r\n\r\n(" + strFilter + ")";

            return msg;
        }

        private static string GetTextList(QlikSenseDataList[] Data, int NumBuckets = 5)
        {
            string TextList = "";
            int i = 0;

            int NofElements = Data.Length;

            foreach (QlikSenseDataList d in Data)
            {
                i += 1;
                if (i > NumBuckets) break;

                if (i > 1) TextList += "\r\n";
                TextList += d.DimValue;
            }

            return TextList;
        }

        private static string ApplyFilter(QlikSenseApp qa, string Filter, string FilterValue, bool isInLine = false)
        {
            string msg = "";

            if (Filter != "" && FilterValue != "")
            {
                qa.AddFilter(Filter, FilterValue, null, isInLine);
                msg = string.Format(randomResponse.GetRandomResponse(StringResources.nlFilterApplied), Filter, FilterValue);

                string strFilter = "";

                foreach (QlikSenseFilter ff in qa.lastFilters)
                {
                    if (ff != qa.lastFilters.First()) strFilter += ", " + randomResponse.GetRandomResponse(StringResources.nlAnd + " ");
                    strFilter += String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element);
                }


                msg += string.IsNullOrEmpty(strFilter) ? "\n" + string.Format(randomResponse.GetRandomResponse(StringResources.nlTotalFilters), strFilter) : string.Empty;
            }
            else
            {
                msg = randomResponse.GetRandomResponse(StringResources.nlNoFilterToApply);
            }

            return msg;
        }

        #region
        ////
        //private static string ApplyFilter(QlikSenseApp qa, bool isInLine = false)
        //{
        //    string msg = "";

        //    string strFilter = "";

        //    if (qa.lastFilters == null || qa.lastFilters.Count() == 0)
        //        msg = randomResponse.GetRandomResponse(StringResources.nlGetElementNotFound);
        //    else
        //    {
        //        int addedFilterCount = 0;
        //        msg = string.Format(randomResponse.GetRandomResponse(StringResources.nlFilterApplied), qa.lastFilters[qa.lastFilters.Count - 1].Dimension, qa.lastFilters[qa.lastFilters.Count - 1].Element);
        //        foreach (QlikSenseFilter ff in qa.lastFilters)
        //        {
        //            if (addedFilterCount != 0)
        //            {
        //                strFilter += randomResponse.GetRandomResponse(StringResources.nlAnd + " " + String.Format(randomResponse.GetRandomResponse(StringResources.nlGetElementMoreFilter), ff.Dimension, ff.Element));
        //                addedFilterCount++;
        //            }
        //        }
        //    }
        //    msg += !string.IsNullOrEmpty(strFilter) ? "\n" + string.Format(randomResponse.GetRandomResponse(StringResources.nlTotalFilters), strFilter) : string.Empty;

        //    return msg;
        //}
        ////
        #endregion

        private static string ApplyFilter(QlikSenseApp qa, bool isInLine = false)
        {
            string msg = "";

            string strFilter = "";

            if (qa.lastFilters == null || qa.lastFilters.Count() == 0)
                msg = StringResources.nlGetElementNotFound;
            else
                foreach (QlikSenseFilter ff in qa.lastFilters)
                {
                    if (qa.lastFilters.FindIndex(x => x.Dimension == ff.Dimension) == 0)
                        msg = string.Format(StringResources.nlFilterApplied, ff.Dimension, ff.Element);
                    else
                        strFilter += ", " + StringResources.nlAnd + " " + String.Format(StringResources.nlGetElementMoreFilter, ff.Dimension, ff.Element);
                }

            msg += !string.IsNullOrEmpty(strFilter) ? "\n" + string.Format(StringResources.nlTotalFilters, strFilter) : string.Empty;

            return msg;
        }

        private List<string> GetDateRangeBetweenTwoDates(List<string> datePeriod)
        {
            string[] startDate = datePeriod[0].Split('-');
            string[] endDate = datePeriod[1].Split('-');
            string conQlikAppDateFormat = Util.Parameters.getConfigParameters("conQlikAppDateFormat").ToString().Replace("YYYY", "yyyy").Replace("DD", "dd").Replace("mm", "MM");

            DateTime start = new DateTime(Convert.ToInt32(startDate[0]), Convert.ToInt32(startDate[1]), Convert.ToInt32(startDate[2]));
            DateTime end = new DateTime(Convert.ToInt32(endDate[0]), Convert.ToInt32(endDate[1]), Convert.ToInt32(endDate[2]));

            DateTime[] dates = Enumerable.Range(0, 1 + end.Subtract(start).Days).Select(offset => start.AddDays(offset)).ToArray();
            return dates.Select(date => date.ToString(conQlikAppDateFormat)).ToList();
            //return "\"" + string.Join("\", \"", dates.Select(date => date.ToString(conQlikAppDateFormat)).ToArray()) + "\"";
        }

        public Response ProcessAction(QlikSenseUser user, CallBackAction action, string value)
        {
            Response Resp = new Response();

            switch (action)
            {
                case CallBackAction.Application:
                    if (value.Trim().Length > 0)
                    {
                        string AppId = value.Trim();
                        user.QlikSenseApp.QlikSenseOpenApp(AppId);
                        if (user.QlikSenseApp.qlikSenseAppId == AppId)
                            Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.appOpenedApp), user.QlikSenseApp.qlikSenseAppName);
                        else
                            Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.appOpenAppError), user.QlikSenseApp.qlikSenseAppId);
                    }
                    break;
            }

            return Resp;
        }

        public Response GetVisualization(QlikSenseUser user, CallBackAction action, string value)
        {
            Response Resp = new Response();

            switch (action)
            {
                case CallBackAction.Visualization:
                    if (value.Trim().Length > 0)
                    {
                        foreach (var visualization in user.QlikSenseApp.masterVisualizations)
                        {
                            if (visualization.Name == value || visualization.Id == value)
                            {
                                string url = user.QlikSenseApp.qlikSenseSingleServer + "/single?appid=" + user.QlikSenseApp.qlikSenseAppId + "&obj=" + visualization.Id + "&opt=currsel";
                                Resp.ChartText = string.Format(randomResponse.GetRandomResponse(StringResources.nlGetChart), visualization.Name, "<" + url + "| " + visualization.Name + ">");
                                Resp.ChartFound = new QlikSenseObject() { ObjectId = visualization.Id, Description = visualization.Name, HRef = url, ObjectURL = url };
                                Resp.VizID = visualization.Id;
                                break;
                            }
                        }
                    }
                    break;
            }

            return Resp;
        }

        public Response ShowAMeasure(string MeasureName, QlikSenseUser Usr)
        {
            Response Resp = new Response();
            try
            {
                Resp.TextMessage = GetMeasureValue(Usr.QlikSenseApp, MeasureName, Usr.UserName);
            }
            catch (Exception e)
            {
                Resp.ErrorText = string.Format("Error in ShowAMeasure() for measure '{0}': {1}", MeasureName, e);
            }
            finally
            {
                Usr.ConversationHistory.Add(new Intent
                {
                    IntentType = IntentType.KPI,
                    Measure = MeasureName
                });
            }
            return Resp;
        }

        public Response ShowMeasureByDimension(ref string MeasureName, ref string DimensionName, QlikSenseUser Usr)
        {
            Response Resp = new Response();
            try
            {
                Resp.TextMessage = GetChart(Usr.QlikSenseApp, ref MeasureName, ref DimensionName, Usr.UserName, ref Resp.VoiceMessage, ref Resp.ChartFound);
            }
            catch (Exception e)
            {
                Resp.ErrorText = string.Format("Error in ShowMeasureByDimension() for measure '{0}' and dimension {1}: {2}", MeasureName, DimensionName, e);
            }
            finally
            {
                Usr.ConversationHistory.Add(new Intent
                {
                    IntentType = IntentType.Chart,
                    Measure = MeasureName,
                    Dimension = DimensionName
                });
            }
            return Resp;
        }

        public Response ShowSheet(string SheetID, QlikSenseUser Usr)
        {
            Response Resp = new Response();
            try
            {
                string url = Usr.QlikSenseApp.qlikSenseSingleServer + "/sense/app/" + Usr.QlikSenseApp.qlikSenseAppId
                    + "/sheet/" + SheetID + "/state/play";
                Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.kpiShowSheet), Usr.UserName, url).Replace("LinkStart", "<").Replace("LinkEnd", ">");
            }
            catch (Exception e)
            {
                Resp.ErrorText = string.Format("Error in ShowSheet() for SheetID '{0}': {1}", SheetID, e);
            }
            return Resp;
        }

        public Response ShowStory(string StoryID, QlikSenseUser Usr)
        {
            Response Resp = new Response();
            try
            {
                string url = Usr.QlikSenseApp.qlikSenseSingleServer + "/sense/app/" + Usr.QlikSenseApp.qlikSenseAppId
                    + "/story/" + StoryID + "/state/play"; // Open the Story
                Resp.TextMessage = string.Format(randomResponse.GetRandomResponse(StringResources.kpiShowStory), Usr.UserName, url);
            }
            catch (Exception e)
            {
                Resp.ErrorText = string.Format("Error in ShowStory() for StoryID '{0}': {1}", StoryID, e);
            }

            return Resp;
        }
    }
}
