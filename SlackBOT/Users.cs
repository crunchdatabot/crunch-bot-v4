﻿using Dialogflow;
using QlikLog;
using QlikSense;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SlackBOT
{
    public class Users
    {
        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        string module = "Users", method = string.Empty;

        private static List<QlikSenseUser> UserList;

        public Users()
        {
            UserList = new List<QlikSenseUser>();
        }

        /// <summary>
        /// Get list of users
        /// </summary>
        /// <returns></returns>
        public List<QlikSenseUser> GetUserList()
        {
            return (UserList);
        }

        /// <summary>
        /// Read users' information from CSV
        /// </summary>
        /// <param name="fileName"></param>
        public void ReadFromCSV(string fileName)
        {
            List<QlikSenseUser> values;

            try
            {
                values = File.ReadAllLines(fileName, System.Text.Encoding.Default)
                                               .Skip(0)
                                               .Select(v => QlikSenseUser.FromCsv(v))
                                               .ToList();
            }
            catch (Exception e)
            {
                if (e.Message != "Index was outside the bounds of the array.")
                {
                    method = "ReadFromCSV";
                    log.Error(module, method, string.Format("Error in reading users from file \"{0}\"", fileName));
                    log.Error(module, method, e.Message);
                    log.Error(module, method, e.StackTrace);
                }

                values = null;
            }

            if (values != null) UserList = values;
        }

        /// <summary>
        /// Write users' information to CSV
        /// </summary>
        /// <param name="fileName"></param>
        public void WriteToCSV(string fileName)
        {
            try
            {
                File.WriteAllLines(fileName, UserList.Select(u => QlikSenseUser.ToCsv(u)), Encoding.Default);
            }
            catch (Exception e)
            {
                method = "WriteToCSV";
                log.Error(module, method, string.Format("Error in writing user to file \"{0}\"", fileName));
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
            }
        }

        /// <summary>
        /// Get filtered (based on channel) user
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public QlikSenseUser GetUser(string channel)
        {
            QlikSenseUser result;

            try
            {
                result = UserList.Find(x => x.Channel.ToLower() == channel.ToLower().Trim());
            }
            catch (Exception e)
            {
                result = null;
            }

            if (result != null) return result;
            else return null;
        }

        /// <summary>
        /// Add user to CSV
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="qlikSenseUserId"></param>
        /// <param name="qlikSenseUserDir"></param>
        /// <param name="qlikSenseUserName"></param>
        /// <param name="lastAccess"></param>
        /// <param name="allowed"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public QlikSenseUser AddUser(string channel, string userId = "", string userName = "", string qlikSenseUserId = "", string qlikSenseUserDir = "", string qlikSenseUserName = "", string lastAccess = "", bool allowed = true, string fileName = "",string emailID = "")
        {
            QlikSenseUser Usr;

            try
            {
                Usr = GetUser(channel);

                if (Usr == null)
                {
                    Usr = new QlikSenseUser();
                    Usr.Channel = channel.Trim();
                    Usr.Allowed = allowed;
                    UserList.Add(Usr);
                }

                if (string.IsNullOrEmpty(Usr.UserId))
                    if (!string.IsNullOrEmpty(userId))
                        Usr.UserId = userId.Trim();
                if (string.IsNullOrEmpty(Usr.UserName))
                    if (!string.IsNullOrEmpty(userName))
                        Usr.UserName = userName.Trim();
                if (string.IsNullOrEmpty(Usr.QlikSenseUserId))
                    if (!string.IsNullOrEmpty(qlikSenseUserId))
                        Usr.QlikSenseUserId = qlikSenseUserId.Trim();
                if (string.IsNullOrEmpty(Usr.QlikSenseUserDir))
                    if (!string.IsNullOrEmpty(qlikSenseUserDir))
                        Usr.QlikSenseUserDir = qlikSenseUserDir.Trim();
                if (string.IsNullOrEmpty(Usr.QlikSenseUserName))
                    if (!string.IsNullOrEmpty(qlikSenseUserName))
                        Usr.QlikSenseUserName = qlikSenseUserName.Trim();
                if (string.IsNullOrEmpty(Usr.EmailID))
                    if (!string.IsNullOrEmpty(emailID))
                        Usr.EmailID = emailID.Trim();

                DateTime LastAccessDateTime;

                if (lastAccess == "")
                    LastAccessDateTime = DateTime.Now;
                else
                    LastAccessDateTime = DateTime.ParseExact(lastAccess, QlikSenseUser.conDateTimeParseFormat, CultureInfo.InvariantCulture);

                Usr.LastAccess = LastAccessDateTime;

                if (!string.IsNullOrEmpty(fileName))
                    WriteToCSV(fileName);
            }
            catch (Exception e)
            {
                method = "AddUser";
                log.Error(module, method, string.Format("Error in adding user: \"{0}\"", userId));
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                return null;
            }

            return Usr;
        }
    }

    public class QlikSenseConversationSummaryItem
    {
        public IntentType Intent;
        public string Measure;
        public string Dimension;
        public List<string> Element;
        public int Times = 0;
    }

    public class QlikSenseConversationSummary
    {
        public List<QlikSenseConversationSummaryItem> Measure = new List<QlikSenseConversationSummaryItem>();
        public List<QlikSenseConversationSummaryItem> MeasureByDimension = new List<QlikSenseConversationSummaryItem>();
        public List<QlikSenseConversationSummaryItem> MeasureForElement = new List<QlikSenseConversationSummaryItem>();
        public List<QlikSenseConversationSummaryItem> Top = new List<QlikSenseConversationSummaryItem>();
        public List<QlikSenseConversationSummaryItem> Bottom = new List<QlikSenseConversationSummaryItem>();
        public List<QlikSenseConversationSummaryItem> Filter = new List<QlikSenseConversationSummaryItem>();
    }

    public class QlikSenseUser
    {
        public string Channel;
        public string EmailID;
        public string UserId;
        public string UserName;
        public string QlikSenseUserId;
        public string QlikSenseUserDir;
        public string QlikSenseUserName;
        private DateTime _LastAccess;
        private DateTime _PreviousAccess;
        public bool Allowed;
        public string Language = "en-US";
        public QlikSenseApp QlikSenseApp;
        public Response LastResponse;
        public QlikSenseObject LastChart;
        public List<Intent> ConversationHistory = new List<Intent>();
        public QlikSenseConversationSummary ConversationSummary = new QlikSenseConversationSummary();

        public DateTime LastAccess
        {
            get { return _LastAccess; }
            set
            {
                _PreviousAccess = LastAccess;
                _LastAccess = value;
            }
        }

        private bool _TheBotIsAngry = false;

        public bool TheBotIsAngry
        {
            get { return _TheBotIsAngry; }
            set
            {
                _TheBotIsAngry = value;
                if (_TheBotIsAngry)
                    _LastArgument = DateTime.Now;
            }
        }

        private DateTime _LastArgument;

        public TimeSpan TimeSincePreviousAccess()
        {
            return LastAccess.Subtract(_PreviousAccess);
        }

        public TimeSpan TimeSinceLastArgument()
        {
            TimeSpan t = new TimeSpan(0);

            if (TheBotIsAngry)
                t = DateTime.Now.Subtract(_LastArgument);

            return t;
        }

        public const string conDateTimeParseFormat = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Extract and assign values from CSV data row (csvLine)
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        public static QlikSenseUser FromCsv(string csvLine)
        {
            QlikSenseUser user = new QlikSenseUser();

            string[] values = csvLine.Split(',');
            int index = 0;

            user.Channel = values[index].ToLower().Trim();
            user.UserId = values[++index].ToLower().Trim();
            user.UserName = values[++index].Trim();
            user.QlikSenseUserId = values[++index].Trim();
            user.QlikSenseUserDir = values[++index].Trim();
            user.QlikSenseUserName = values[++index].Trim();
            user.LastAccess = DateTime.ParseExact(values[++index], conDateTimeParseFormat, CultureInfo.InvariantCulture);
            user.Allowed = values[++index].Trim() == "Y" ? true : false;
            if (values.Length > index) user.Language = values[++index].Trim();

            return user;
        }

        /// <summary>
        /// Write a CSV data row (csvLine) with comma separated
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string ToCsv(QlikSenseUser user)
        {
            string csvLine;

            csvLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                user.Channel, user.UserId, user.UserName,user.EmailID, user.QlikSenseUserId, user.QlikSenseUserDir, user.QlikSenseUserName, user.LastAccess.ToString(conDateTimeParseFormat), user.Allowed ? "Y" : "N", user.Language);

            return csvLine;
        }

        public void SummarizeHistory()
        {
            foreach (Intent Pred in ConversationHistory)
            {
                QlikSenseConversationSummaryItem NewItem = new QlikSenseConversationSummaryItem
                {
                    Intent = Pred.IntentType,
                    Measure = Pred.Measure,
                    Dimension = Pred.Dimension,
                    Element = Pred.Elements,
                    Times = 1
                };

                QlikSenseConversationSummaryItem item;

                if (Pred.IntentType == IntentType.KPI || Pred.IntentType == IntentType.Chart || Pred.IntentType == IntentType.Measure4Element)
                {
                    item = ConversationSummary.Measure.Find(s => s.Measure == Pred.Measure);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.Measure.Add(item);
                    }
                    else
                        item.Times++;
                }

                if (Pred.IntentType == IntentType.Chart)
                {
                    item = ConversationSummary.MeasureByDimension.Find(s => s.Measure == Pred.Measure && s.Dimension == Pred.Dimension);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.MeasureByDimension.Add(item);
                    }
                    else
                        item.Times++;
                }

                if (Pred.IntentType == IntentType.Measure4Element)
                {
                    item = ConversationSummary.MeasureForElement.Find(s => s.Measure == Pred.Measure && s.Element == Pred.Elements && s.Dimension == Pred.Dimension);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.MeasureForElement.Add(item);
                    }
                    else
                        item.Times++;
                }

                if (Pred.IntentType == IntentType.Filter || Pred.IntentType == IntentType.Measure4Element)
                {
                    item = ConversationSummary.Filter.Find(s => s.Dimension == Pred.Dimension && s.Element == Pred.Elements);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.Filter.Add(item);
                    }
                    else
                        item.Times++;
                }

                if (Pred.IntentType == IntentType.RankingTop)
                {
                    item = ConversationSummary.Top.Find(s => s.Measure == Pred.Measure && s.Dimension == Pred.Dimension);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.Top.Add(item);
                    }
                    else
                        item.Times++;
                }

                if (Pred.IntentType == IntentType.RankingBottom)
                {
                    item = ConversationSummary.Bottom.Find(s => s.Measure == Pred.Measure && s.Dimension == Pred.Dimension);
                    if (item == null)
                    {
                        item = NewItem;
                        ConversationSummary.Bottom.Add(item);
                    }
                    else
                        item.Times++;
                }

            }

            ConversationHistory.Clear();

            ConversationSummary.Measure.OrderByDescending(order => order.Times);
            ConversationSummary.MeasureByDimension.OrderByDescending(order => order.Times);
            ConversationSummary.MeasureForElement.OrderByDescending(order => order.Times);
            ConversationSummary.Filter.OrderByDescending(order => order.Times);
            ConversationSummary.Top.OrderByDescending(order => order.Times);
            ConversationSummary.Bottom.OrderByDescending(order => order.Times);
        }
    }
}
