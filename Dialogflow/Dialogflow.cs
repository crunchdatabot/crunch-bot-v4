﻿using ApiAiSDK;
using ApiAiSDK.Model;
using QlikLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Util;

namespace Dialogflow
{
    public class Dialogflow
    {
        private ApiAi apiAI;
        public bool IsConnected = false;

        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        string module = "Dialogflow", method = string.Empty;

        public Dialogflow(string ClientToken, string Language = "English")
        {
            SupportedLanguage supLang = ApiAiSDK.SupportedLanguage.FromLanguageTag(Language);
            var config = new AIConfiguration(ClientToken, supLang);
            apiAI = new ApiAi(config);
            IsConnected = true;
        }

        private Intent Predicted = new Intent();

        public bool HasPrediction { get { return Predicted.HasPrediction; } }
        public string OriginalQuery { get { return Predicted.OriginalQuery; } }
        public IntentType IntentType { get { return Predicted.IntentType; } }
        public string Measure { get { return Predicted.Measure; } }
        public string Measure2 { get { return Predicted.Measure2; } }
        public List<string> Elements { get { return Predicted.Elements; } }
        public string Dimension { get { return Predicted.Dimension; } }
        public string Dimension2 { get { return Predicted.Dimension2; } }
        public string Dimension3 { get { return Predicted.Dimension3; } }
        public string Percentage { get { return Predicted.Percentage; } }
        public List<double?> Number { get { return Predicted.Number; } }
        public string ChartType { get { return Predicted.ChartType; } }
        public double DistanceKm { get { return Predicted.DistanceKm; } }
        public string Language { get { return Predicted.Language; } }
        public string Response { get { return Predicted.Response; } }
        public List<string> Date { get { return Predicted.Date; } }
        public List<string> DatePeriod { get { return Predicted.DatePeriod; } }
        public List<string> DatePeriod1 { get { return Predicted.DatePeriod1; } }
        public List<string> OriginalDatePeriod { get { return Predicted.OriginalDatePeriod; } }

        public bool Predict(string TextToPredict)
        {
            Predicted.HasPrediction = false;
            Predicted.OriginalQuery = null;
            Predicted.IntentType = IntentType.None;
            Predicted.Response = null;
            Predicted.Measure = null;
            Predicted.Measure2 = null;
            Predicted.Elements = new List<string>();
            Predicted.Dimension = null;
            Predicted.Dimension2 = null;
            Predicted.Dimension3 = null;
            Predicted.Percentage = null;
            Predicted.Number = new List<double?>();
            Predicted.ChartType = null;
            Predicted.Date = new List<string>();
            Predicted.DatePeriod = new List<string>();
            Predicted.DatePeriod1 = new List<string>();
            Predicted.OriginalDatePeriod = new List<string>();

            //Predicted.DistanceKm = 0;
            //Predicted.Language = null;

            try
            {
                AIResponse response = apiAI.TextRequest(TextToPredict.ToLower());

                if (!response.IsError && response.Status.ErrorType == "success")
                {
                    if (response.Result.ActionIncomplete ||
                        response.Result.Action == "Greetings" ||
                        response.Result.Action == "Hello" ||
                        response.Result.Action == "input.welcome" ||
                        response.Result.Action == "Disgrace" ||
                        response.Result.Action == "GetStarted" ||
                        response.Result.Action == "Gratitude" ||
                        response.Result.Action == "Quit")
                    {
                        Predicted.HasPrediction = true;
                        Predicted.OriginalQuery = response.Result.ResolvedQuery;
                        if (response.Result.Action == "Welcome")
                            Predicted.IntentType = IntentType.Welcome;
                        else if (response.Result.Action == "GetStarted")
                            Predicted.IntentType = IntentType.GetStarted;
                        else if (response.Result.Action == "Gratitude")
                            Predicted.IntentType = IntentType.Gratitude;
                        else if (response.Result.Action == "Greetings")
                            Predicted.IntentType = IntentType.Greetings;
                        else
                            Predicted.IntentType = IntentType.DirectResponse;
                        Predicted.Response = response.Result.Fulfillment?.Speech;
                    }

                    #region Generic Intent
                    //else
                    //{
                    //    if (response.Result.Action == "CommonIntent" || response.Result.Action == "ApplyFilters")
                    //    {
                    //        IntentType intent = IdentifyIntent(ref Predicted, response.Result.Parameters, response.Result.Action);

                    //        this.Predicted.HasPrediction = true;
                    //        this.Predicted.OriginalQuery = TextToPredict;

                    //        switch (intent)
                    //        {
                    //            case IntentType.None:
                    //                this.Predicted.IntentType = IntentType.None;
                    //                this.Predicted.Response = "I do not understand you, could you try again with other question?";
                    //                break;

                    //            case IntentType.KPI:
                    //                this.Predicted.IntentType = IntentType.KPI;
                    //                this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    //                break;

                    //            case IntentType.Filter:
                    //                this.Predicted.IntentType = IntentType.Filter;
                    //                this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    //                break;

                    //            case
                    //                IntentType.Measure4Element:
                    //                this.Predicted.IntentType = IntentType.Measure4Element;
                    //                this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    //                break;

                    //            case
                    //                IntentType.RankingTop:
                    //                this.Predicted.IntentType = IntentType.RankingTop;
                    //                this.Predicted.Response = "You want to know the top elements by " + Predicted.Dimension;
                    //                break;

                    //            case
                    //                IntentType.RankingBottom:
                    //                this.Predicted.IntentType = IntentType.RankingBottom;
                    //                this.Predicted.Response = "You want to know the bottom elements by " + Predicted.Dimension;
                    //                break;

                    //            case
                    //                IntentType.CreateChart:
                    //                this.Predicted.IntentType = IntentType.CreateChart;
                    //                this.Predicted.Response = "I will create a chart for you";
                    //                break;

                    //            case
                    //                IntentType.ShowMeasureByMeasure:
                    //                this.Predicted.IntentType = IntentType.ShowMeasureByMeasure;
                    //                this.Predicted.Response = "I will show you the result of analyzing these two measures";
                    //                break;

                    //            case
                    //                IntentType.ShowElementsAboveValue:
                    //                this.Predicted.IntentType = IntentType.ShowElementsAboveValue;
                    //                this.Predicted.Response = "You want to know the elements that meet a measure is above this value";
                    //                break;

                    //            case
                    //                IntentType.ShowElementsBelowValue:
                    //                this.Predicted.IntentType = IntentType.ShowElementsBelowValue;
                    //                this.Predicted.Response = "You want to know the elements that meet a measure is below this value";
                    //                break;

                    //            case
                    //                IntentType.ShowElementsBetweenValues:
                    //                this.Predicted.IntentType = IntentType.ShowElementsBetweenValues;
                    //                this.Predicted.Response = "You want to know the elements that meet a measure is between values";
                    //                break;

                    //            case
                    //                IntentType.ShowListOfElements:
                    //                this.Predicted.IntentType = IntentType.ShowListOfElements;
                    //                this.Predicted.Response = "You want to know the elements that meet a measure is between values";
                    //                break;

                    //            default:

                    //                this.Predicted.HasPrediction = false;
                    //                this.Predicted.OriginalQuery = TextToPredict;
                    //                this.Predicted.IntentType = IntentType.None;
                    //                this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";
                    //                break;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (response.Result.Action == "input.unknown")
                    //        {
                    //            this.Predicted.HasPrediction = false;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.None;
                    //            //this.Predicted.Number = 0;
                    //            this.Predicted.Response = "I do not understand you, could you try again with other question?";
                    //        }
                    //        else if (response.Result.Action == "ShowChart")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.Chart;
                    //            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    //        }
                    //        else if (response.Result.Action == "Alert")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.Alert;
                    //            this.Predicted.Response = "You want to be alerted when " + Predicted.Measure + " changes by " + Predicted.Percentage;
                    //        }
                    //        else if (response.Result.Action == "Hello" || response.Result.Action == "input.welcome")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.Hello;
                    //            this.Predicted.Response = "Hello";
                    //        }
                    //        else if (response.Result.Action == "Reports")
                    //        {
                    //            this.Predicted.HasPrediction = false;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.None;
                    //            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";

                    //            ////Temporary commented
                    //            //this.Predicted.HasPrediction = true;
                    //            //this.Predicted.OriginalQuery = TextToPredict;
                    //            //this.Predicted.IntentType = IntentType.Reports;
                    //            //this.Predicted.Response = "I will show you the available reports";
                    //        }
                    //        else if (response.Result.Action == "ShowAnalysis")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAnalysis;
                    //            this.Predicted.Response = "I will create an analysis for you";
                    //        }
                    //        else if (response.Result.Action == "Help")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.Help;
                    //            this.Predicted.Response = "You can ask for any information in the current Qlik Sense app";
                    //        }
                    //        else if (response.Result.Action == "ClearFilters" || response.Result.Action == "ClearAllFilters")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ClearAllFilters;
                    //            this.Predicted.Response = "I will clear all filters";
                    //        }
                    //        else if (response.Result.Action == "CurrentSelections")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.CurrentSelections;
                    //            this.Predicted.Response = "I will show you the current selections in the app";
                    //        }
                    //        else if (response.Result.Action == "ShowApps" || response.Result.Action == "ShowAllApps")
                    //        {
                    //            this.Predicted.HasPrediction = false;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.None;
                    //            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";

                    //            ////Temporary commented
                    //            //this.Predicted.HasPrediction = true;
                    //            //this.Predicted.OriginalQuery = TextToPredict;
                    //            //this.Predicted.IntentType = IntentType.ShowAllApps;
                    //            //this.Predicted.Response = "I will show all the available apps you can connect";
                    //        }
                    //        else if (response.Result.Action == "ShowVisualizations")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAllVisualizations;
                    //            this.Predicted.Response = "I will show all the available visualizations you can connect";
                    //        }
                    //        else if (response.Result.Action == "ShowDimensions" || response.Result.Action == "ShowAllDimensions")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAllDimensions;
                    //            this.Predicted.Response = "I will show you all the master dimensions in the current app";
                    //        }
                    //        else if (response.Result.Action == "ShowMeasures" || response.Result.Action == "ShowAllMeasures")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAllMeasures;
                    //            this.Predicted.Response = "I will show you all the master measures in the current app";
                    //        }
                    //        else if (response.Result.Action == "ShowSheets" || response.Result.Action == "ShowAllSheets")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAllSheets;
                    //            this.Predicted.Response = "I will show you all the sheets in the current app";
                    //        }
                    //        else if (response.Result.Action == "ShowStories" || response.Result.Action == "ShowAllStories")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowAllStories;
                    //            this.Predicted.Response = "I will show you all the stories in the current app";
                    //        }
                    //        else if (response.Result.Action == "ShowKPIs")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.ShowKPIs;
                    //            this.Predicted.Response = "I will show you the most used metrics in the current app";
                    //        }
                    //        else if (response.Result.Action == "Apologize")
                    //        {
                    //            this.Predicted.HasPrediction = true;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.Apologize;
                    //            this.Predicted.Response = "OK";
                    //        }
                    //        else
                    //        {
                    //            this.Predicted.HasPrediction = false;
                    //            this.Predicted.OriginalQuery = TextToPredict;
                    //            this.Predicted.IntentType = IntentType.None;
                    //            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Default NLP Engine
                    else if (response.Result.Action == "Followup" || response.Result.Action == "FollowupNext")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = TextToPredict;

                        var context = response.Result.Contexts.Where(x => x.Name == "fup1").ToList();

                        foreach (System.Collections.Generic.KeyValuePair<string, object> param in context[0].Parameters)
                        {
                            string strParam = (string)param.Value.ToString();

                            if (strParam.StartsWith("[\r\n"))
                                strParam = (string)param.Value.ToString().Replace("[\r\n  \"", "").Replace("\"\r\n]", "");

                            string comparator = param.Key.ToString().StartsWith("fUp2") ? "fUp2" : param.Key.ToString().StartsWith("fUp1") ? "fUp1" : string.Empty;

                            if (!string.IsNullOrEmpty(strParam) && !param.Key.ToString().EndsWith(".original"))
                            {
                                if (param.Key == "Measure") Predicted.Measure = strParam;
                                else if (param.Key == "Measure1") Predicted.Measure2 = strParam;
                                else if (param.Key == "Dimension") Predicted.Dimension = strParam;
                                else if (param.Key.StartsWith(comparator + "dim")) { if (!Predicted.Elements.Contains(strParam)) Predicted.Elements.Add(strParam); }
                                else if (param.Key == comparator + "Date-Period") Predicted.DatePeriod = strParam.Split('/').ToList();
                                else if (param.Key == comparator + "Date-Period1") Predicted.DatePeriod1 = strParam.Split('/').ToList();
                                else if (param.Key == comparator + "OriginalDate-Period") { Predicted.OriginalDatePeriod = new List<string>(); Predicted.OriginalDatePeriod.Add(strParam); }
                                else if (param.Key == comparator + "OriginalDate-Period1") { Predicted.OriginalDatePeriod[1] = strParam; }
                                else if ((param.Key == comparator + "Date" || param.Key == comparator + "Date1")) { if (!Predicted.Date.Contains(strParam)) Predicted.Date.Add(strParam); }
                                else if (param.Key == "ChartType") Predicted.ChartType = strParam;
                                else if (param.Key == "Intent")
                                {
                                    switch (strParam)
                                    {
                                        case "ShowMeasure":
                                        case "ShowMeasureForDimension":
                                        case "ShowMeasureForElement":
                                            this.Predicted.IntentType = IntentType.Measure4Element;
                                            break;
                                        case "Ranking":
                                            if (!string.IsNullOrEmpty(response.Result.Parameters["Superlatives"].ToString()))
                                                if (response.Result.Parameters["Superlatives"].ToString() == "top")
                                                    this.Predicted.IntentType = IntentType.RankingTop;
                                                else if (response.Result.Parameters["Superlatives"].ToString() == "bottom")
                                                    this.Predicted.IntentType = IntentType.RankingBottom;
                                            break;
                                        case "CreateChart":
                                            this.Predicted.IntentType = IntentType.CreateChart;
                                            break;
                                        case "MeasureVsMeasure":
                                        case "ShowMeasureByMeasure":
                                            this.Predicted.IntentType = IntentType.ShowMeasureByMeasure;
                                            break;
                                        case "Range":
                                            if (!string.IsNullOrEmpty(response.Result.Parameters["Comparator"].ToString()))
                                            {
                                                if (response.Result.Parameters["Comparator"].ToString() == ">" || response.Result.Parameters["Comparator"].ToString() == ">=")
                                                    this.Predicted.IntentType = IntentType.ShowElementsAboveValue;
                                                else if (response.Result.Parameters["Comparator"].ToString() == "<" || response.Result.Parameters["Comparator"].ToString() == "<=")
                                                    this.Predicted.IntentType = IntentType.ShowElementsBelowValue;
                                                else if (response.Result.Parameters["Comparator"].ToString() == "==")
                                                { }
                                                else if (response.Result.Parameters["Comparator"].ToString() == "!=")
                                                { }
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        if (response.Result.Action == "FollowupNext")
                        {
                            var request = (HttpWebRequest)WebRequest.Create("https://api.dialogflow.com/v1/contexts?sessionId=" + response.SessionId);
                            request.Method = "DELETE";
                            request.Headers.Add("Authorization", "Bearer " + Parameters.getConfigParameters("conDialogflowKey").ToString());
                            var resp = (HttpWebResponse)request.GetResponse();
                            var responseString = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                        }
                    }
                    else
                    {
                        if (response.Result.Parameters != null)
                        {
                            foreach (System.Collections.Generic.KeyValuePair<string, object> param in response.Result.Parameters)
                            {
                                string strParam = (string)param.Value.ToString();

                                if (strParam.StartsWith("[\r\n"))
                                    strParam = (string)param.Value.ToString().Replace("[\r\n  \"", "").Replace("\"\r\n]", "");

                                if (!string.IsNullOrEmpty(strParam))
                                {
                                    if (param.Key == "Measure") Predicted.Measure = strParam;
                                    else if ((param.Key == "Measure1")) Predicted.Measure2 = strParam;
                                    else if (param.Key == "Dimension") Predicted.Dimension = strParam;
                                    else if (param.Key.StartsWith("dim")) { if (!Predicted.Elements.Contains(strParam)) Predicted.Elements.Add(strParam); }
                                    else if (param.Key == "Date-Period") Predicted.DatePeriod = strParam.Split('/').ToList();
                                    else if (param.Key == "Date-Period1") Predicted.DatePeriod1 = strParam.Split('/').ToList();
                                    else if (param.Key == "OriginalDate-Period") { Predicted.OriginalDatePeriod = new List<string>(); Predicted.OriginalDatePeriod.Add(strParam); }
                                    else if (param.Key == "OriginalDate-Period1") { Predicted.OriginalDatePeriod[1] = strParam; }
                                    else if ((param.Key == "Date" || param.Key == "Date1")) Predicted.Date.Add(strParam);
                                    else if (param.Key == "ChartType") Predicted.ChartType = strParam;

                                    else if (param.Key == "Percentage")
                                    {
                                        Predicted.Percentage = strParam;
                                        Predicted.Number = PercentageToDouble(Predicted.Percentage);
                                    }
                                    else if (param.Key == "Number" || param.Key == "Number1") Predicted.Number.Add(Convert.ToDouble(param.Value));
                                }
                            }
                        }

                        if (response.Result.Action == "input.unknown")
                        {
                            this.Predicted.HasPrediction = false;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.None;
                            //this.Predicted.Number = 0;
                            this.Predicted.Response = "I do not understand you, could you try again with other question?";
                        }
                        else if (response.Result.Source == "domains")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.DirectResponse;
                            this.Predicted.Response = response.Result.Fulfillment.Speech;
                        }
                        else if (response.Result.Action == "ShowMeasure")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.KPI;
                            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                        }
                        else if (response.Result.Action == "ShowChart")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Chart;
                            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                        }
                        else if (response.Result.Action == "ShowMeasureForDimension" || response.Result.Action == "ShowMeasureForElement")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Measure4Element;
                            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;

                            //if (string.IsNullOrEmpty(Predicted.Dimension) && Predicted.Elements.Count() == 0)
                            //{
                            //    this.Predicted.HasPrediction = true;
                            //    this.Predicted.OriginalQuery = TextToPredict;
                            //    this.Predicted.IntentType = IntentType.KPI;
                            //    this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                            //}
                            //else
                            //{
                            //    this.Predicted.HasPrediction = true;
                            //    this.Predicted.OriginalQuery = TextToPredict;
                            //    this.Predicted.IntentType = IntentType.Measure4Element;
                            //    this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                            //}
                        }
                        else if (response.Result.Action == "Alert")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Alert;
                            this.Predicted.Response = "You want to be alerted when " + Predicted.Measure + " changes by " + Predicted.Percentage;
                        }
                        else if (response.Result.Action == "Ranking")
                        {
                            if (!string.IsNullOrEmpty(response.Result.Parameters["Superlatives"].ToString()))
                            {
                                if (response.Result.Parameters["Superlatives"].ToString() == "top")
                                {
                                    this.Predicted.HasPrediction = true;
                                    this.Predicted.OriginalQuery = TextToPredict;
                                    this.Predicted.IntentType = IntentType.RankingTop;
                                    this.Predicted.Response = "You want to know the top elements by " + Predicted.Dimension;
                                }
                                else if (response.Result.Parameters["Superlatives"].ToString() == "bottom")
                                {
                                    this.Predicted.HasPrediction = true;
                                    this.Predicted.OriginalQuery = TextToPredict;
                                    this.Predicted.IntentType = IntentType.RankingBottom;
                                    this.Predicted.Response = "You want to know the bottom elements by " + Predicted.Dimension;
                                }
                            }
                        }
                        else if (response.Result.Action == "ApplyFilters" || response.Result.Action == "Filter")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Filter;
                            this.Predicted.Response = "You want to filter " + Predicted.Dimension + " by " + Predicted.Elements;
                        }
                        else if (response.Result.Action == "Hello" || response.Result.Action == "input.welcome")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Hello;
                            this.Predicted.Response = "Hello";
                        }
                        else if (response.Result.Action == "Reports")
                        {
                            this.Predicted.HasPrediction = false;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.None;
                            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";

                            ////Temporary commented
                            //this.Predicted.HasPrediction = true;
                            //this.Predicted.OriginalQuery = TextToPredict;
                            //this.Predicted.IntentType = IntentType.Reports;
                            //this.Predicted.Response = "I will show you the available reports";
                        }
                        else if (response.Result.Action == "CreateChart")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.CreateChart;
                            this.Predicted.Response = "I will create a chart for you";
                        }
                        else if (response.Result.Action == "ShowAnalysis")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAnalysis;
                            this.Predicted.Response = "I will create an analysis for you";
                        }
                        else if (response.Result.Action == "ContactQlik")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ContactQlik;
                            this.Predicted.Response = "You can go to www.qlik.com";
                        }
                        else if (response.Result.Action == "Help")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Help;
                            this.Predicted.Response = "You can ask for any information in the current Qlik Sense app";
                        }
                        else if (response.Result.Action == "ClearFilters" || response.Result.Action == "ClearAllFilters")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ClearAllFilters;
                            this.Predicted.Response = "I will clear all filters";
                        }
                        else if (response.Result.Action == "ClearDimensionFilter")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ClearDimensionFilter;
                            this.Predicted.Response = "I will clear this dimension filter";
                        }
                        else if (response.Result.Action == "CurrentSelections")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.CurrentSelections;
                            this.Predicted.Response = "I will show you the current selections in the app";
                        }
                        else if (response.Result.Action == "ShowApps" || response.Result.Action == "ShowAllApps")
                        {
                            this.Predicted.HasPrediction = false;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.None;
                            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";

                            ////Temporary commented
                            //this.Predicted.HasPrediction = true;
                            //this.Predicted.OriginalQuery = TextToPredict;
                            //this.Predicted.IntentType = IntentType.ShowAllApps;
                            //this.Predicted.Response = "I will show all the available apps you can connect";
                        }
                        else if (response.Result.Action == "ShowVisualizations")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAllVisualizations;
                            this.Predicted.Response = "I will show all the available visualizations you can connect";
                        }
                        else if (response.Result.Action == "ShowDimensions" || response.Result.Action == "ShowAllDimensions")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAllDimensions;
                            this.Predicted.Response = "I will show you all the master dimensions in the current app";
                        }
                        else if (response.Result.Action == "ShowMeasures" || response.Result.Action == "ShowAllMeasures")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAllMeasures;
                            this.Predicted.Response = "I will show you all the master measures in the current app";
                        }
                        else if (response.Result.Action == "ShowSheets" || response.Result.Action == "ShowAllSheets")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAllSheets;
                            this.Predicted.Response = "I will show you all the sheets in the current app";
                        }
                        else if (response.Result.Action == "ShowStories" || response.Result.Action == "ShowAllStories")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowAllStories;
                            this.Predicted.Response = "I will show you all the stories in the current app";
                        }
                        else if (response.Result.Action == "ShowKPIs")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowKPIs;
                            this.Predicted.Response = "I will show you the most used metrics in the current app";
                        }
                        else if (response.Result.Action == "MeasureVsMeasure" || response.Result.Action == "ShowMeasureByMeasure")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowMeasureByMeasure;
                            this.Predicted.Response = "I will show you the result of analyzing these two measures";
                        }
                        else if (response.Result.Action == "ShowListOfElements")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.ShowListOfElements;
                            this.Predicted.Response = "I will show you a list of elements for this dimension";
                        }
                        else if (response.Result.Action == "Apologize")
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.Apologize;
                            this.Predicted.Response = "OK";
                        }
                        else if (response.Result.Action == "Range")
                        {
                            if (!string.IsNullOrEmpty(response.Result.Parameters["Comparator"].ToString()))
                            {
                                if (response.Result.Parameters["Comparator"].ToString() == ">" || response.Result.Parameters["Comparator"].ToString() == ">=")
                                {
                                    this.Predicted.HasPrediction = true;
                                    this.Predicted.OriginalQuery = TextToPredict;
                                    this.Predicted.IntentType = IntentType.ShowElementsAboveValue;
                                    this.Predicted.Response = "You want to know the elements that meet a measure is above this value";
                                }
                                else if (response.Result.Parameters["Comparator"].ToString() == "<" || response.Result.Parameters["Comparator"].ToString() == "<=")
                                {
                                    this.Predicted.HasPrediction = true;
                                    this.Predicted.OriginalQuery = TextToPredict;
                                    this.Predicted.IntentType = IntentType.ShowElementsBelowValue;
                                    this.Predicted.Response = "You want to know the elements that meet a measure is below this value";
                                }
                                else if (response.Result.Parameters["Comparator"].ToString() == "between")
                                {
                                    this.Predicted.HasPrediction = true;
                                    this.Predicted.OriginalQuery = TextToPredict;
                                    this.Predicted.IntentType = IntentType.ShowElementsBetweenValues;
                                    this.Predicted.Response = "You want to know the elements that meet a measure is between values";
                                }
                                else if (response.Result.Parameters["Comparator"].ToString() == "==")
                                { }
                                else if (response.Result.Parameters["Comparator"].ToString() == "!=")
                                { }
                            }
                        }
                        else
                        {
                            this.Predicted.HasPrediction = false;
                            this.Predicted.OriginalQuery = TextToPredict;
                            this.Predicted.IntentType = IntentType.None;
                            this.Predicted.Response = "I do not have the logic to answer " + TextToPredict + " yet";
                        }
                        //else if (response.Result.Action == "GoodAnswer")
                        //{
                        //    this.Predicted.HasPrediction = true;
                        //    this.Predicted.OriginalQuery = TextToPredict;
                        //    this.Predicted.IntentType = IntentType.GoodAnswer;
                        //    this.Predicted.Response = ":-)";
                        //}
                        //else if (response.Result.Action == "Bye")
                        //{
                        //    this.Predicted.HasPrediction = true;
                        //    this.Predicted.OriginalQuery = TextToPredict;
                        //    this.Predicted.IntentType = IntentType.Bye;
                        //    this.Predicted.Response = "Bye";
                        //}
                        //else if (response.Result.Action == "BadWords" )
                        //{
                        //    this.Predicted.HasPrediction = true;
                        //    this.Predicted.OriginalQuery = TextToPredict;
                        //    this.Predicted.IntentType = IntentType.BadWords;
                        //    this.Predicted.Response = ":-(\nI prefer not to answer this type of questions, I am a robot but I am very polite.";
                        //}
                    }
                    #endregion
                }
            }
            catch (Exception e)
            {
                method = "Predict";
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                Predicted.HasPrediction = false;
            }

            return Predicted.HasPrediction;
        }

        private IntentType IdentifyIntent(ref Intent predicted, Dictionary<string, object> parameters, string intent)
        {
            string comparator = string.Empty, superlative = string.Empty;

            ExtractParameter(parameters, out predicted, out comparator, out superlative);

            if (!string.IsNullOrEmpty(predicted.Measure))
            {
                if (!string.IsNullOrEmpty(predicted.Measure2))
                    return IntentType.ShowMeasureByMeasure;
                else if (!string.IsNullOrEmpty(comparator) || !string.IsNullOrEmpty(superlative) || (predicted.Number != null && predicted.Number.Count() > 0 && !string.IsNullOrEmpty(predicted.Dimension)))
                {
                    if (predicted.Number.Count() > 1)
                        return IntentType.ShowElementsBetweenValues;
                    else
                    {
                        if (string.IsNullOrEmpty(comparator) && string.IsNullOrEmpty(superlative))
                            return IntentType.ShowElementsAboveValue;
                        else
                        {
                            if (comparator == ">" || comparator == ">=")
                                return IntentType.ShowElementsAboveValue;
                            else if (comparator == "<" || comparator == "<=")
                                return IntentType.ShowElementsBelowValue;
                            else if (comparator == "between")
                                return IntentType.ShowElementsBetweenValues;

                            if (superlative == "top")
                                return IntentType.RankingTop;
                            else if (superlative == "bottom")
                                return IntentType.RankingBottom;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(predicted.ChartType) && !string.IsNullOrEmpty(predicted.Dimension))
                    return IntentType.CreateChart;
                else if (predicted.Elements != null && predicted.Elements.Count() > 0 && !string.IsNullOrEmpty(predicted.Dimension))
                {
                    bool isElementFallIntoDimension = Parameters.IsDimensionHasElementValue(predicted.Dimension, predicted.Elements);

                    if (isElementFallIntoDimension)
                        return IntentType.Measure4Element;
                    else
                        return IntentType.CreateChart;
                }
                else if (predicted.Elements != null && predicted.Elements.Count() > 0)
                    return IntentType.Measure4Element;
                else if (!string.IsNullOrEmpty(predicted.Dimension))
                    return IntentType.CreateChart;
                else
                    return IntentType.KPI;
            }
            else if (predicted.Elements.Count() > 0)
                return IntentType.Filter;
            else if (!string.IsNullOrEmpty(predicted.Dimension))
                return IntentType.ShowListOfElements;

            return IntentType.None;
        }

        private void ExtractParameter(Dictionary<string, object> parameters, out Intent predicted, out string comparator, out string superlative)
        {
            predicted = new Intent(); comparator = string.Empty; superlative = string.Empty;

            predicted.Elements = new List<string>();
            predicted.DatePeriod = new List<string>();
            predicted.DatePeriod1 = new List<string>();
            predicted.OriginalDatePeriod = new List<string>();
            predicted.Date = new List<string>();
            predicted.Number = new List<double?>();

            foreach (KeyValuePair<string, object> param in parameters)
            {
                string paramString = (string)param.Value.ToString().Replace("[]", "");

                if (paramString.StartsWith("[\r\n"))
                    paramString = string.Join(",", new JavaScriptSerializer().Deserialize<string[]>(param.Value.ToString()));

                if (!string.IsNullOrEmpty(paramString))
                {
                    List<string> parameterSet = paramString.Split(',').ToList().Distinct().ToList();

                    if (parameterSet.Count() != 0)
                    {
                        if (param.Key == "Measure")
                        {
                            if (parameterSet.Count() > 0)
                                predicted.Measure = parameterSet[0];

                            if (parameterSet.Count() == 2)
                                predicted.Measure2 = parameterSet[1];
                        }
                        else if (param.Key == "Dimension")
                        {
                            if (parameterSet.Count() > 0)
                                predicted.Dimension = parameterSet[0];

                            if (parameterSet.Count() == 2)
                                predicted.Dimension2 = parameterSet[1];

                            if (parameterSet.Count() == 3)
                                predicted.Dimension3 = parameterSet[2];
                        }
                        else if (param.Key == "Element") { if (!predicted.Elements.Contains(parameterSet[0])) predicted.Elements.Add(parameterSet[0]); }
                        else if (param.Key == "date-period")
                        {
                            if (parameterSet.Count() > 0)
                                predicted.DatePeriod = parameterSet[0].Split('/').ToList();

                            if (parameterSet.Count() == 2)
                                predicted.DatePeriod1 = parameterSet[1].Split('/').ToList();
                        }
                        else if (param.Key == "OriginalDate-Period") predicted.OriginalDatePeriod = parameterSet;
                        else if (param.Key == "date") predicted.Date = parameterSet;
                        else if (param.Key == "ChartType") predicted.ChartType = parameterSet[0];

                        else if (param.Key == "Percentage")
                        {
                            predicted.Percentage = parameterSet[0];
                            predicted.Number = PercentageToDouble(predicted.Percentage);
                        }
                        else if (param.Key == "number")
                        {
                            List<double?> dblList = new List<double?>();
                            parameterSet.ForEach(delegate (string x) { dblList.Add(Convert.ToDouble(x)); });
                            predicted.Number = dblList;
                        }
                        else if (param.Key == "Superlatives")
                            superlative = parameterSet[0];
                        else if (param.Key == "Comparator")
                            comparator = parameterSet[0];
                    }
                }
            }
        }

        private List<double?> PercentageToDouble(string Percentage)
        {
            List<double?> d = new List<double?>();
            string strPct = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol;

            if (!Percentage.Contains(strPct)) return null;

            try
            {
                d.Add(double.Parse(Percentage.Replace(strPct, "")) / 100);
            }
            catch (Exception e)
            {
                d = null;
            }

            return d;
        }
    }
}
