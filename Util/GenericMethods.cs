﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public class GenericMethods
    {
        public static string RefineFileName(string filePath)
        {
            List<string> chars = new List<string>() { "/", "\\", ":", "*", "?", "\"", "<", ">", "|" };

            List<string> fileName = filePath.Split('\\').ToList();

            foreach (string character in chars)
                fileName[fileName.Count - 1] = fileName[fileName.Count - 1].Replace(character, "").Replace("  ", " ");

            return string.Join("\\", fileName);
        }
    }
}
