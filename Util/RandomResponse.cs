﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Util
{
    public class RandomResponse
    {
        static List<int> listNumbers = new List<int>();
        private string conRandomResponseFile = ConfigurationManager.AppSettings["conRandomResponseFilePath"];
        public string GetRandomResponse(string randomResponseTag)
        {
            string responseTag = GetResxNameByValue(randomResponseTag);
            string randomSelectedString = string.Empty;
            Random pickRandom = new Random();
            int randomNumber = 0;
            if (!string.IsNullOrEmpty(responseTag) && !string.IsNullOrEmpty(conRandomResponseFile) && File.Exists(conRandomResponseFile))
            {
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(conRandomResponseFile);
                    XmlNodeList rootNodeList = xmlDoc.DocumentElement.SelectNodes("/Response/" + responseTag);
                    //XmlNodeList rootNodeList = xmlDoc.DocumentElement.SelectNodes("/Response/nlGetElement");
                    int countOption = 1;
                    foreach (XmlNode optionList in rootNodeList)
                    {
                        if (optionList.HasChildNodes)
                            randomNumber = pickRandom.Next(countOption, optionList.ChildNodes.Count);
                        foreach (XmlNode option in optionList)
                        {
                            if (countOption == randomNumber)
                            {
                                randomSelectedString = option.InnerText;
                                break;
                            }
                            countOption++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                if (string.IsNullOrEmpty(randomSelectedString))
                    randomSelectedString = randomResponseTag;
                return randomSelectedString;
            }
            else
                return randomResponseTag;
        }
        private string GetResxNameByValue(string value)
        {
            try
            {
                System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Util.Multilanguage.StringResources", GetType().Assembly);
                var entry =
                    rm.GetResourceSet(System.Threading.Thread.CurrentThread.CurrentCulture, true, true)
                      .OfType<DictionaryEntry>()
                      .FirstOrDefault(e => e.Value.ToString() == value);

                var key = entry.Key.ToString();
                return key;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
