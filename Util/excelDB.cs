﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Util
{
    public class excelDB
    {
        static string SystemConfigurationPath = string.Empty,
            metaDataPath = string.Empty;

        public void GenerateExcel(string metadataXlPath, string systemPath)
        {
            metaDataPath = metadataXlPath;
            SystemConfigurationPath = systemPath;

            List<string> missingFiles = new List<string>();
            List<string> missingDirectories = new List<string>();
            try
            {
                var dt = new System.Data.DataTable();
                var ds = new DataSet();
                dt = ds.Tables.Add("Application");
                dt.Columns.Add("App_ID");
                dt.Columns.Add("Name");
                addDatatoTable("Application", ref dt);

                //Measures
                dt = ds.Tables.Add("Visualizations");
                dt.Columns.Add("App_ID");
                dt.Columns.Add("Name");
                dt.Columns.Add("Tags");
                addDatatoTable("Visualizations", ref dt);

                //Measures
                dt = ds.Tables.Add("Measures");
                dt.Columns.Add("App_ID");
                dt.Columns.Add("Name");
                dt.Columns.Add("Tags");
                addDatatoTable("Measures", ref dt);

                //Dimensions
                dt = ds.Tables.Add("Dimensions");
                dt.Columns.Add("App_ID");
                dt.Columns.Add("Dim_ID");
                dt.Columns.Add("Name");
                dt.Columns.Add("Tags");
                addDatatoTable("Dimensions", ref dt);

                //Dimensions' Values
                dt = ds.Tables.Add("Dimensions' Values");
                dt.Columns.Add("App_ID");
                dt.Columns.Add("Dim_ID");
                dt.Columns.Add("Value");
                addDatatoTable("DimensionsValues", ref dt);

                try
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        foreach (System.Data.DataTable dtt in ds.Tables)
                            wb.Worksheets.Add(dtt);

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            FileStream file = new FileStream(metaDataPath, FileMode.Create, FileAccess.Write);
                            MyMemoryStream.WriteTo(file);
                            file.Close();
                        }

                    }
                }
                catch (Exception ee)
                {
                }
                //return "";
            }
            catch (Exception ex)
            {
                //  return ex.ToString();
            }

        }

        private void addDatatoTable(string v, ref DataTable dt)
        {
            throw new NotImplementedException();
        }
        //Excel to DataTable

    }
}
