﻿using Flurl.Http;
using Newtonsoft.Json.Linq;
using NinjaNye.SearchExtensions;
using Qlik.Engine;
using Qlik.Sense.Client;
using Qlik.Sense.Client.Visualizations;
using Qlik.Sense.Client.Visualizations.Components;
using QlikLog;
using QlikSense;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Util;

namespace QlikSense
{
    public class QlikSenseUtil
    {

    }

    //Qlik Sense Object to Fetch Created Objects from App
    public class QlikSenseObject
    {
        public string ObjectId { get; set; }
        public string ObjectURL { get; set; }
        public string ObjectType { get; set; }
        public string Description { get; set; }
        public string HRef { get; set; }
        public string ThumbURL { get; set; }
        public string ChartType { get; set; }
    }

    public class QlikSenseFilter
    {
        public string Dimension { get; set; }
        public string DimensionExpression { get; set; }
        public string Element { get; set; }
        public List<string> SetOfElement { get; set; }
        public bool isInLine { get; set; }
    }

    public class QlikSenseValueFilter
    {
        public string MeasureExpression { get; set; }
        public string MeasureCondition { get; set; }
    }

    //Qlik Sense Data List for Ranking
    public class QlikSenseDataList
    {
        public string DimValue { get; set; }
        public double MeasValue { get; set; }
        public string MeasFormattedValue { get; set; }
    }

    public class QlikSenseAlert
    {
        public string UserName { get; set; }
        public string UserID { get; set; }
        public string SendToID { get; set; }
        public string AlertRequest { get; set; }
        public string AlertMessage { get; set; }
        public string AlertPhoto { get; set; }
        public string AlertDoc { get; set; }
        public bool AlertActive { get; set; }
        public bool AlertSent { get; set; }

        public QlikSenseAlert()
        {
            AlertActive = false;
        }
    }

    //Qlik Sense Field Detail
    public class QlikSenseField
    {
        public string FieldNameInApp { get; set; }
        public string FieldToSearch { get; set; }
        public IEnumerable<string> Tags { get; set; }
    }

    //Qlik Sense Master Item Detail
    public class QlikSenseMasterItem
    {
        public string Name { get; set; }
        public string Expression { get; set; }
        public string Id { get; set; }
        public string FormattedExpression { get; set; }
        public IEnumerable<string> Tags { get; set; }

    }

    //Qlik Sense Story Detail
    public class QlikSenseStory
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    //Qlik Sense Sheet Detail
    public class QlikSenseSheet
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    //Qlik Sense App Properties
    public class QlikSenseAppProperties
    {
        public DateTime modifiedDate { get; set; }
        public bool published { get; set; }
        public DateTime publishTime { get; set; }
        public IList<string> privileges { get; set; }
        public string description { get; set; }
        public int qFileSize { get; set; }
        public string dynamicColor { get; set; }
        public object create { get; set; }
        public QlikSenseStreamProperties stream { get; set; }
        public bool canCreateDataConnections { get; set; }
        public string AppID { get; set; }
        public string AppName { get; set; }
        public string AppTitle { get; set; }
        public string ThumbnailUrl { get; set; }
    }

    //Qlik Sense Stream Properties
    public class QlikSenseStreamProperties
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class QlikSenseApp
    {
        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        private static string SessionTicket = string.Empty;

        string module = "QlikSenseUtil", method = string.Empty;

        public string qlikSenseServer { get; set; }
        public string qlikSenseAppName { get; set; }
        public string qlikSenseAppId { get; set; }
        public string qlikSenseVirtualProxyPath { get; set; }
        public string qlikSenseAppThumbnailUrl { get; set; }
        public string qlikSenseSingleServer { get; set; }
        private string qlikSenseHeaderAuthName;

        private ILocation qlikSenseLocation;
        public IApp qlikSenseApp;

        public static List<QlikSenseAppProperties> _qlikSenseAlternativeApps = new List<QlikSenseAppProperties>();
        public List<QlikSenseAppProperties> qlikSenseAlternativeApps { get { return _qlikSenseAlternativeApps; } }

        public string qlikSenseAlternativeStreams = null;

        public List<QlikSenseAlert> alertList;

        const int maxFounds = 100;
        private static Random rnd;

        private QlikSenseField latitudeField;
        private QlikSenseField longitudeField;
        private QlikSenseField addressField;
        private bool geoLocationActive = false;

        private static List<QlikSenseField> _appFields = new List<QlikSenseField>();
        public static List<QlikSenseMasterItem> _masterMeasures = new List<QlikSenseMasterItem>();
        public static List<QlikSenseMasterItem> _masterDimensions = new List<QlikSenseMasterItem>();
        public static List<QlikSenseMasterItem> _masterVisualizations = new List<QlikSenseMasterItem>();
        public static List<QlikSenseStory> _stories = new List<QlikSenseStory>();
        public static List<QlikSenseSheet> _sheets = new List<QlikSenseSheet>();

        private List<QlikSenseField> appFields { get { return _appFields; } }
        public List<QlikSenseMasterItem> masterMeasures { get { return _masterMeasures; } }
        public List<QlikSenseMasterItem> masterDimensions { get { return _masterDimensions; } }
        public List<QlikSenseMasterItem> masterVisualizations { get { return _masterVisualizations; } }
        public List<QlikSenseStory> stories { get { return _stories; } }
        public List<QlikSenseSheet> sheets { get { return _sheets; } }

        public QlikSenseMasterItem lastMeasure;
        public QlikSenseMasterItem lastDimension;
        public List<QlikSenseFilter> lastFilters;

        public List<QlikSenseValueFilter> valueFilters;

        private string qlikSenseUserId;

        private bool qlikSenseIsConnected = false;
        public bool isConnected { get { return qlikSenseIsConnected; } }

        private bool qlikSenseAppIsOpen = false;
        public bool appIsOpen { get { return qlikSenseAppIsOpen; } }

        public bool IsGeoLocationActive()
        {
            return geoLocationActive;
        }

        public QlikSenseFilter AddFilter(string Dimension, string Element, bool isInLine = true)
        {
            QlikSenseFilter ff = new QlikSenseFilter();
            ff.Dimension = Dimension;
            ff.DimensionExpression = GetDimensionExpression(Dimension);
            ff.Element = Element;
            ff.isInLine = isInLine;

            QlikSenseFilter Repeated = lastFilters.Find(f => f.Dimension == Dimension);
            if (Repeated != null)
            {
                Repeated.Element = Element;
                Repeated.isInLine = isInLine;
                return Repeated;
            }
            else
            {
                lastFilters.Add(ff);
                return ff;
            }
        }

        public QlikSenseFilter AddFilter(string Dimension, string Element, List<string> SetOfElement, bool isInLine)
        {
            QlikSenseFilter ff = new QlikSenseFilter();
            ff.Dimension = Dimension;
            ff.DimensionExpression = GetDimensionExpression(Dimension);
            ff.Element = Element;
            ff.SetOfElement = SetOfElement;
            ff.isInLine = isInLine;

            QlikSenseFilter Repeated = lastFilters.Find(f => f.Dimension == Dimension);
            if (Repeated != null)
            {
                Repeated.Element = Element;
                Repeated.SetOfElement = SetOfElement;
                Repeated.isInLine = isInLine;
                return Repeated;
            }
            else
            {
                lastFilters.Add(ff);
                return ff;
            }
        }

        public QlikSenseFilter AddFilter(QlikSenseFilter Filter, bool isInLine = true)
        {
            return AddFilter(Filter.Dimension, Filter.Element, isInLine);
        }

        public QlikSenseValueFilter AddValueFilter(string MeasureExpression, string MeasureCondition)
        {
            QlikSenseValueFilter vf = new QlikSenseValueFilter();
            vf.MeasureExpression = MeasureExpression;
            vf.MeasureCondition = MeasureCondition;

            QlikSenseValueFilter Repeated = valueFilters.Find(f => f.MeasureExpression == MeasureExpression);
            if (Repeated != null)
            {
                Repeated.MeasureCondition = MeasureCondition;
                return Repeated;
            }
            else
            {
                valueFilters.Add(vf);
                return vf;
            }
        }

        public QlikSenseValueFilter AddValueFilter(QlikSenseValueFilter ValFilter)
        {
            return AddValueFilter(ValFilter.MeasureExpression, ValFilter.MeasureCondition);
        }

        public QlikSenseApp()
        {
            qlikSenseServer = "https://myserver.com";
            qlikSenseAppName = "Executive Dashboard";
            qlikSenseAppId = "";
            qlikSenseSingleServer = "https://myserver.com";

            alertList = new List<QlikSenseAlert>();
            lastFilters = new List<QlikSenseFilter>();
            valueFilters = new List<QlikSenseValueFilter>();
            rnd = new Random();
        }

        public void CheckConnection()
        {
            method = "CheckConnection";

            if (!qlikSenseLocation.IsAlive())
            {
                log.Info(module, method, "Qlik is disconnected.", true);
                this.ConnectQlikSenseServer(qlikSenseUserId, qlikSenseHeaderAuthName, qlikSenseLocation.VirtualProxyPath, IsUsingSSL(), IsCheckingSDKVersion());
            }
            else
                log.Info(module, method, "Qlik connection is alive.", true);
        }

        public bool CheckConnectionToHub()
        {
            try
            {
                IHub MyHub = qlikSenseLocation.Hub();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void CheckConnection(string qlikSenseUserId, string QlikSenseHeaderAuthName, string QlikSenseVirtualProxyPath)
        {
            method = "CheckConnection";

            //qlikSenseLocation = null;
            //this.ConnectQlikSenseServer(qlikSenseUserId, QlikSenseHeaderAuthName, QlikSenseVirtualProxyPath, IsUsingSSL(), IsCheckingSDKVersion());

            if (!qlikSenseLocation.IsAlive())
            {
                log.Info(module, method, "Qlik is disconnected.", true);
                this.ConnectQlikSenseServer(qlikSenseUserId, QlikSenseHeaderAuthName, QlikSenseVirtualProxyPath, IsUsingSSL(), IsCheckingSDKVersion());
            }
            else
                log.Info(module, method, "Qlik connection is alive.", true);
        }

        internal static byte[] ReadFile(string fileName)
        {
            FileStream f = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            int size = (int)f.Length;
            byte[] data = new byte[size];
            size = f.Read(data, 0, size);
            f.Close();
            return data;
        }

        public void ConnectQlikSenseServer(string UserId, string HeaderAuthName, string VirtualProxyPath = "", Boolean UseSSL = false, Boolean certificateValidation = false, Boolean CheckSDKVersion = true)
        {
            method = "ConnectQlikSenseServer";

            try
            {
                qlikSenseIsConnected = false;
                string strUri = qlikSenseServer;
                Uri uri = new Uri(strUri);

                qlikSenseLocation = Qlik.Engine.Location.FromUri(uri);
                qlikSenseLocation.AsStaticHeaderUserViaProxy(UserId, HeaderAuthName, UseSSL, certificateValidation);
                qlikSenseLocation.IsVersionCheckActive = CheckSDKVersion;
                if (VirtualProxyPath.Trim() != string.Empty)
                {
                    qlikSenseLocation.VirtualProxyPath = VirtualProxyPath;
                    qlikSenseVirtualProxyPath = VirtualProxyPath;
                }

                IHub MyHub = qlikSenseLocation.Hub();

                qlikSenseUserId = UserId;
                qlikSenseHeaderAuthName = HeaderAuthName;

                qlikSenseIsConnected = true;

                //log.Info(module, method, "Connected to qlik sense version: " + MyHub.ProductVersion(), true);
                log.Info(module, method, "UserID: " + UserId + " - VirtualProxy: " + VirtualProxyPath, true);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error connecting to qlik sense server.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
            }
        }

        public bool IsCheckingSDKVersion()
        {
            return qlikSenseLocation.IsVersionCheckActive;
        }

        public bool IsUsingSSL()
        {
            return (qlikSenseLocation.ServerUri.Scheme == "https");
        }

        public string VirtualProxy()
        {
            if (qlikSenseLocation.VirtualProxyPath == null || qlikSenseLocation.VirtualProxyPath.Trim() == "")
                return "";
            else
                return qlikSenseLocation.VirtualProxyPath;
        }

        public void QlikSenseOpenApp(bool isAppChanged = false)
        {
            method = "QlikSenseOpenApp";

            CheckConnection();

            IAppIdentifier CurrentAppId;
            if (qlikSenseAppId != null && qlikSenseAppId != string.Empty)
                CurrentAppId = qlikSenseLocation.AppWithIdOrDefault(qlikSenseAppId);
            else
                CurrentAppId = qlikSenseLocation.AppWithNameOrDefault(qlikSenseAppName);

            qlikSenseAppId = CurrentAppId.AppId;
            qlikSenseAppName = CurrentAppId.AppName;

            qlikSenseApp = qlikSenseLocation.App(CurrentAppId);
            qlikSenseAppThumbnailUrl = qlikSenseApp.GetAppProperties().Thumbnail.Url;

            if (isAppChanged || _masterMeasures.Count == 0 || _masterMeasures.Count == 0 || _masterVisualizations.Count == 0)
            {
                log.Info(module, method, "Reloading metadata...", true);
                QlikSenseReadFields();
                QlikSenseReadMasterItems();
                QlikSenseReadSheets();
                QlikSenseReadStories();
                GetAlternativeApps(qlikSenseAlternativeStreams);
                log.Info(module, method, "Metadata reloaded successfully.", true);
            }

            lastMeasure = masterMeasures.Count > 0 ? masterMeasures.First() : null;
            lastDimension = masterDimensions.Count > 0 ? masterDimensions.First() : null;

            lastFilters.Clear();
            valueFilters.Clear();

            qlikSenseAppIsOpen = true;

            log.Info(module, method, "Opened App " + "\"" + (qlikSenseAppName) + "\"", true);
        }

        private void QlikSenseOpenApp(QlikSenseAppProperties App)
        {
            qlikSenseAppName = App.AppName;
            qlikSenseAppId = App.AppID;
            qlikSenseAppThumbnailUrl = App.ThumbnailUrl;

            QlikSenseOpenApp();
        }

        public void QlikSenseOpenApp(string AppId)
        {
            GetAlternativeApps(qlikSenseAlternativeStreams);
            QlikSenseAppProperties AppProp = new QlikSenseAppProperties();
            AppProp.AppID = AppId;
            QlikSenseOpenApp(AppProp);
        }

        private void GetAlternativeApps(string StreamNames = null)
        {
            List<string> Streams;
            if (StreamNames != null)
                Streams = StreamNames.Split(';').ToList();
            else
                Streams = new List<string>();

            _qlikSenseAlternativeApps.Clear();

            foreach (IAppIdentifier App in qlikSenseLocation.GetAppIdentifiers())
            {
                QlikSenseAppProperties AppProp = new QlikSenseAppProperties();
                AppProp = Newtonsoft.Json.JsonConvert.DeserializeObject<QlikSenseAppProperties>(App.Meta.PrintStructure());
                AppProp.AppID = App.AppId;
                AppProp.AppName = App.AppName;
                AppProp.AppTitle = App.Title;
                AppProp.ThumbnailUrl = App.Thumbnail.Url;

                if (AppProp.published && (Streams.Count == 0 || Streams.Contains(AppProp.stream?.name)))
                    _qlikSenseAlternativeApps.Add(AppProp);
                if (!AppProp.published && AppProp.AppName == qlikSenseUserId)
                    _qlikSenseAlternativeApps.Add(AppProp);
            }
        }

        private void QlikSenseReadSheets()
        {
            sheets.Clear();
            foreach (Qlik.Sense.Client.ISheet AppSheet in Qlik.Sense.Client.AppExtensions.GetSheets(qlikSenseApp))
            {
                QlikSenseSheet qlikSenses = new QlikSenseSheet();
                qlikSenses.Id = AppSheet.Id;
                qlikSenses.Name = AppSheet.Properties.MetaDef.Title;
                var m = AppSheet.Properties.MetaDef;
                sheets.Add(qlikSenses);
            }
        }

        public void QlikSenseDoSave()
        {
            qlikSenseApp.DoSave();
        }

        public bool QlikSenseRemoveSheet(string SheetID)
        {
            if (SheetID != null && SheetID != string.Empty)
                return qlikSenseApp.RemoveSheet(SheetID);
            else
                return false;
        }

        public string QlikSenseCreateSheet(string SheetID, string Title = "", string Description = "")
        {
            QlikSenseRemoveSheet(SheetID);
            QlikSenseDoSave();

            SheetProperties sp = new SheetProperties();
            if (Title != string.Empty)
                sp.MetaDef.Title = Title;
            if (Description != string.Empty)
                sp.MetaDef.Description = Description;
            sp.Rank = 0;

            ISheet Sheet = qlikSenseApp.CreateSheet(SheetID, sp);
            QlikSenseDoSave();

            return SheetID;
        }

        public void QlikSenseCreateTextImage(string SheetID, string Id, string Title, string Text)
        {
            ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
            ITextImage TextImg = Sheet.CreateTextImage(Id, new TextImageProperties { Title = Title, Markdown = Text });
            //return TextImg;
        }

        public void QlikSenseCreateKPI(string SheetID, string Id, string Measure1, string Measure2 = null,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateKPI";

            IKpi Kpi = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem mm2 = GetMasterMeasure(Measure2);

                var properties = new KpiProperties
                {
                    Title = ChartTitle,
                    Subtitle = mm1.Name,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new KpiVisualizationHyperCubeDef
                    {
                        SuppressMissing = true,
                        InterColumnSortOrder = new[] { 0, 1 },
                        InitialDataFetch = new List<NxPage>
                        {
                            new NxPage
                            {
                                Height = 500,
                                Left = 0,
                                Top = 0,
                                Width = 10
                            }
                        }.ToArray(),
                        Measures = new List<KpiHyperCubeMeasureDef>
                        {
                            new KpiHyperCubeMeasureDef
                            {
                                Def =
                                    new KpiHyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            },
                            new KpiHyperCubeMeasureDef
                            {
                                Def =
                                    new KpiHyperCubeMeasureqDef
                                    {
                                        Def = mm2.Expression,
                                        Label = mm2.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            }
                        },
                        SuppressZero = true
                    }
                };

                Kpi = Sheet.CreateKpi(Id, properties);

                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var kpiCell = Sheet.CellFor(Kpi);
                    //kpi1Cell.SetBounds(1, 1, 3, 3);
                    kpiCell.SetBounds(row, col, width, height);
                }
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating KPI.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                Kpi = null;
            }
        }

        public void QlikSenseCreateBarChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Measure1,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateBarChart";

            IBarchart BChart = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);

                FieldAttributes measureFormat;

                var format = mm1.Tags.Where(x => x.StartsWith("_format")).ToList(); //_formatMoney, _formatPercent, _formatNumber

                if (format.Contains("_formatMoney"))
                    measureFormat = new FieldAttributes() { Type = FieldAttrType.MONEY, Dec = ".", Thou = ",", Fmt = "$#,##0.00;-$#,##0.00" };
                else
                    measureFormat = new FieldAttributes();

                var properties = new BarchartProperties()
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new VisualizationHyperCubeDef
                    {
                        Dimensions = new List<HyperCubeDimensionDef>
                        {
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md1.Name},
                                    FieldDefs = new[] {md1.Expression}
                                }
                            }
                        },
                        Measures = new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat
                                    },
                                SortBy = new SortCriteria() { SortByNumeric = SortDirection.Descending }
                            }
                        },
                        InterColumnSortOrder = new[] { 1, 0 },
                        InitialDataFetch =
                            new List<NxPage>
                            {
                                new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                            }.ToArray(),
                        SuppressMissing = true,
                        SuppressZero = true
                    }
                };
                BChart = Sheet.CreateBarchart(Id, properties);

                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var barchartCell = Sheet.CellFor(BChart);
                    barchartCell.SetBounds(row, col, width, height);
                }

                narrative = getNarratives(ref Id, ref imageFileName, BChart, properties, "Barchart", mm1.Tags);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating bar chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                BChart = null;
            }
            //return BChart;
        }

        public void QlikSenseCreateLineChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Measure1,
           string ChartTitle = "",
           int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateLineChart";

            ILinechart LChart = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);

                FieldAttributes measureFormat;

                var format = mm1.Tags.Where(x => x.StartsWith("_format")).ToList(); //_formatMoney, _formatPercent, _formatNumber

                if (format.Contains("_formatMoney"))
                    measureFormat = new FieldAttributes() { Type = FieldAttrType.MONEY, Dec = ".", Thou = ",", Fmt = "$#,##0.00;-$#,##0.00" };
                else
                    measureFormat = new FieldAttributes();

                var properties = new LinechartProperties()
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new VisualizationHyperCubeDef
                    {
                        Dimensions = new List<HyperCubeDimensionDef>
                        {
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md1.Name},
                                    FieldDefs = new[] {md1.Expression}
                                }
                            }
                        },
                        Measures = new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat
                                    },
                                SortBy = new SortCriteria() { SortByNumeric = SortDirection.Descending }
                            }
                        },
                        InterColumnSortOrder = new[] { 1, 0 },
                        InitialDataFetch =
                            new List<NxPage>
                            {
                                new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                            }.ToArray(),
                        SuppressMissing = true,
                        SuppressZero = true
                    }
                };
                LChart = Sheet.CreateLinechart(Id, properties);


                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var linechartCell = Sheet.CellFor(LChart);
                    linechartCell.SetBounds(row, col, width, height);
                }

                narrative = getNarratives(ref Id, ref imageFileName, LChart, properties, "Linechart", mm1.Tags);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating line chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                LChart = null;
            }
            //return LChart;
        }

        public void QlikSenseCreatePieChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Measure1,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreatePieChart";
            IPiechart PChart = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);

                var properties = new PiechartProperties()
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new VisualizationHyperCubeDef
                    {
                        Dimensions = new List<HyperCubeDimensionDef>
                        {
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md1.Name},
                                    FieldDefs = new[] {md1.Expression}
                                }
                            }
                        },
                        Measures = new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            }
                        },
                        //InterColumnSortOrder = new[] { 1, 0 },
                        InitialDataFetch =
                            new List<NxPage>
                            {
                                new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                            }.ToArray(),
                        SuppressMissing = true,
                        SuppressZero = true
                    }
                };
                PChart = Sheet.CreatePiechart(Id, properties);


                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var piechartCell = Sheet.CellFor(PChart);
                    piechartCell.SetBounds(row, col, width, height);
                }

                narrative = getNarratives(ref Id, ref imageFileName, PChart, properties, "Piechart", mm1.Tags);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating pie chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                PChart = null;
            }
            //return PChart;
        }

        public void QlikSenseCreateTreeChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Measure1,
            string ChartTitle = "", string Dimension2 = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateTreeChart";

            ITreemap TChart = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);
                QlikSenseMasterItem md2 = GetMasterDimension(Dimension2);

                if (Dimension2.Length > 0)
                {
                    var properties = new TreemapProperties()
                    {
                        Title = ChartTitle == "" ? Id : ChartTitle,
                        ShowTitles = ChartTitle == "" ? false : true,
                        HyperCubeDef = new VisualizationHyperCubeDef
                        {
                            Mode = NxHypercubeMode.DATA_MODE_PIVOT_STACK,
                            Dimensions = new List<HyperCubeDimensionDef>
                            {
                                new HyperCubeDimensionDef
                                {
                                    Def = new HyperCubeDimensionqDef
                                    {
                                        CId = ClientExtension.GetCid(),
                                        FieldLabels = new[] {md1.Name},
                                        FieldDefs = new[] {md1.Expression}
                                    }
                                },
                                new HyperCubeDimensionDef
                                {
                                    Def = new HyperCubeDimensionqDef
                                    {
                                        CId = ClientExtension.GetCid(),
                                        FieldLabels = new[] {md2.Name},
                                        FieldDefs = new[] {md2.Expression}
                                    }
                                }
                            },
                            Measures = new List<HyperCubeMeasureDef>
                            {
                                new HyperCubeMeasureDef
                                {
                                    Def =
                                        new HyperCubeMeasureqDef
                                        {
                                            Def = mm1.Expression,
                                            Label = mm1.Name,
                                            CId = ClientExtension.GetCid()
                                        }
                                }
                            },
                            InterColumnSortOrder = new[] { 1, 0 },
                            InitialDataFetch =
                                new List<NxPage>
                                {
                                    new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                                }.ToArray(),
                            SuppressMissing = true,
                            SuppressZero = true
                        }
                    };
                    TChart = Sheet.CreateTreemap(Id, properties);

                    narrative = getNarratives(ref Id, ref imageFileName, TChart, properties, "Treechart", mm1.Tags);
                }
                else
                {
                    var properties = new TreemapProperties()
                    {
                        Title = ChartTitle == "" ? Id : ChartTitle,
                        ShowTitles = ChartTitle == "" ? false : true,
                        Color = new ColorMode
                        {
                            Auto = false,
                            Mode = ColorModeMode.ByDimension,
                            SingleColor = 3,
                            Persistent = false,
                            DimensionScheme = ColorModeDimensionScheme.DimensionScheme12,
                            MeasureScheme = ColorModeMeasureScheme.Sg,
                            ReverseScheme = false
                        },
                        HyperCubeDef = new VisualizationHyperCubeDef
                        {
                            Mode = NxHypercubeMode.DATA_MODE_PIVOT_STACK,
                            Dimensions = new List<HyperCubeDimensionDef>
                            {
                                new HyperCubeDimensionDef
                                {
                                    Def = new HyperCubeDimensionqDef
                                    {
                                        CId = ClientExtension.GetCid(),
                                        FieldLabels = new[] {md1.Name},
                                        FieldDefs = new[] {md1.Expression}
                                    }
                                }
                            },
                            Measures = new List<HyperCubeMeasureDef>
                            {
                                new HyperCubeMeasureDef
                                {
                                    Def =
                                        new HyperCubeMeasureqDef
                                        {
                                            Def = mm1.Expression,
                                            Label = mm1.Name,
                                            CId = ClientExtension.GetCid()
                                        }
                                }
                            },
                            //InterColumnSortOrder = new[] { 1, 0 },
                            InitialDataFetch =
                                new List<NxPage>
                                {
                                    new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                                }.ToArray()
                        }
                    };
                    TChart = Sheet.CreateTreemap(Id, properties);
                    //NxTreeDataOption ntd = new NxTreeDataOption();
                    //ntd.MaxNbrOfNodes = 3;
                    //NxPageTreeLevel nxPageTreeLevel = new NxPageTreeLevel();
                    //nxPageTreeLevel.Depth = 1;
                    //nxPageTreeLevel.Left = 1;
                    //var asv = TChart.GetHyperCubeTreeData("/qHyperCubeDef", ntd);
                    narrative = getNarratives(ref Id, ref imageFileName, TChart, properties, "Treechart", mm1.Tags);
                }


                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var treechartCell = Sheet.CellFor(TChart);
                    treechartCell.SetBounds(row, col, width, height);
                }
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating tree chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                TChart = null;
            }
            //return TChart;
        }

        public void QlikSenseCreatePivotTableChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Dimension2, string Dimension3,
            string Measure1 = null, string Measure2 = null, string Measure3 = null,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreatePivotTableChart";
            IPivottable PTChart = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem mm2 = GetMasterMeasure(Measure2);
                QlikSenseMasterItem mm3 = GetMasterMeasure(Measure3);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);
                QlikSenseMasterItem md2 = GetMasterDimension(Dimension2);
                QlikSenseMasterItem md3 = GetMasterDimension(Dimension3);

                var properties = new PivottableProperties()
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new VisualizationHyperCubeDef
                    {
                        Mode = NxHypercubeMode.DATA_MODE_PIVOT,
                        Dimensions = new List<HyperCubeDimensionDef>
                        {
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md1.Name},
                                    FieldDefs = new[] {md1.Expression},
                                    Grouping = NxGrpType.GRP_NX_COLLECTION
                                }
                            },
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md2.Name},
                                    FieldDefs = new[] {md2.Expression},
                                    Grouping = NxGrpType.GRP_NX_COLLECTION
                                }
                            },
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md3.Name},
                                    FieldDefs = new[] {md3.Expression},
                                    Grouping = NxGrpType.GRP_NX_COLLECTION
                                }
                            }
                        },
                        Measures = new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            },
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm2.Expression,
                                        Label = mm2.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            },
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm3.Expression,
                                        Label = mm3.Name,
                                        CId = ClientExtension.GetCid()
                                    }
                            }
                        },
                        //InterColumnSortOrder = new[] { 1, 0 },
                        InitialDataFetch =
                            new List<NxPage>
                            {
                                new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                            }.ToArray(),
                        SuppressMissing = true,
                        SuppressZero = true
                    }
                };
                PTChart = Sheet.CreatePivottable(Id, properties);


                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var pivotchartCell = Sheet.CellFor(PTChart);
                    pivotchartCell.SetBounds(row, col, width, height);
                }

                narrative = getNarratives(ref Id, ref imageFileName, PTChart, properties, "Pivot-table", mm1.Tags);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating pivot table chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                PTChart = null;
            }
            //return PTChart;
        }

        public void QlikSenseCreateScatterChart(ref string narrative, ref string imageFileName, string SheetID, string Id, string Dimension1, string Measure1,
            string Measure2, string Measure3 = null,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateScatterChart";

            IScatterplot SChart = null;

            try
            {
                int NumMeasures = 3;

                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem mm2 = GetMasterMeasure(Measure2);
                QlikSenseMasterItem mm3 = null;

                FieldAttributes measureFormat1, measureFormat2, measureFormat3 = new FieldAttributes();

                var format = mm1.Tags.Where(x => x.StartsWith("_format")).ToList(); //_formatMoney, _formatPercent, _formatNumber

                if (format.Contains("_formatMoney"))
                    measureFormat1 = new FieldAttributes() { Type = FieldAttrType.MONEY, Dec = ".", Thou = ",", Fmt = "$#,##0.00;-$#,##0.00" };
                else
                    measureFormat1 = new FieldAttributes();

                format = mm2.Tags.Where(x => x.StartsWith("_format")).ToList(); //_formatMoney, _formatPercent, _formatNumber

                if (format.Contains("_formatMoney"))
                    measureFormat2 = new FieldAttributes() { Type = FieldAttrType.MONEY, Dec = ".", Thou = ",", Fmt = "$#,##0.00;-$#,##0.00" };
                else
                    measureFormat2 = new FieldAttributes();

                if (string.IsNullOrEmpty(Measure3))
                    NumMeasures = 2;
                else
                {
                    mm3 = GetMasterMeasure(Measure3);

                    format = mm3.Tags.Where(x => x.StartsWith("_format")).ToList(); //_formatMoney, _formatPercent, _formatNumber

                    if (format.Contains("_formatMoney"))
                        measureFormat3 = new FieldAttributes() { Type = FieldAttrType.MONEY, Dec = ".", Thou = ",", Fmt = "$#,##0.00;-$#,##0.00" };
                    else
                        measureFormat3 = new FieldAttributes();
                }

                IMeasure[] im = new IMeasure[NumMeasures];
                im[0] = qlikSenseApp.GetMeasure(mm1.Id);
                im[1] = qlikSenseApp.GetMeasure(mm2.Id);
                if (NumMeasures > 2) im[2] = qlikSenseApp.GetMeasure(mm3.Id);

                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);

                var properties = new ScatterplotProperties()
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    HyperCubeDef = new VisualizationHyperCubeDef
                    {
                        Dimensions = new List<HyperCubeDimensionDef>
                        {
                            new HyperCubeDimensionDef
                            {
                                Def = new HyperCubeDimensionqDef
                                {
                                    CId = ClientExtension.GetCid(),
                                    FieldLabels = new[] {md1.Name},
                                    FieldDefs = new[] {md1.Expression}
                                }
                            }
                        },
                        Measures = NumMeasures > 2 ? new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat1
                                    }
                            },
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm2.Expression,
                                        Label = mm2.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat2
                                    }
                            },
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm3.Expression,
                                        Label = mm3.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat3
                                    }
                            }
                        } : new List<HyperCubeMeasureDef>
                        {
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm1.Expression,
                                        Label = mm1.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat1
                                    }
                            },
                            new HyperCubeMeasureDef
                            {
                                Def =
                                    new HyperCubeMeasureqDef
                                    {
                                        Def = mm2.Expression,
                                        Label = mm2.Name,
                                        CId = ClientExtension.GetCid(),
                                        NumFormat = measureFormat2
                                    }
                            }
                        },
                        //InterColumnSortOrder = new[] { 1, 0 },
                        InitialDataFetch =
                            new List<NxPage>
                            {
                                new NxPage { Height = 500, Left = 0, Top = 0, Width = 10 }
                            }.ToArray(),
                        SuppressMissing = true,
                        SuppressZero = true
                    }
                };
                SChart = Sheet.CreateScatterplot(Id, properties);


                ScatterplotProperties p = SChart.Properties;
                p.Title = Measure1 + " vs " + Measure2;

                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var scatterchartCell = Sheet.CellFor(SChart);
                    scatterchartCell.SetBounds(row, col, width, height);
                }


                narrative = getNarratives(ref Id, ref imageFileName, SChart, properties, "Scatterchart", mm1.Tags, mm2.Tags, NumMeasures > 2 ? mm3.Tags : null);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating scatter chart.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                SChart = null;
            }
            //return PTChart;
        }

        public void QlikSenseCreateFilterPane(string SheetID, string Id, string Dimension1, string Dimension2 = null, string Dimension3 = null,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateFilterPane";

            IFilterpane FPane = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);
                QlikSenseMasterItem md2 = GetMasterDimension(Dimension2);
                QlikSenseMasterItem md3 = GetMasterDimension(Dimension3);

                var props = new FilterpaneProperties
                {
                    Title = ChartTitle == "" ? Id : ChartTitle,
                    ShowTitles = ChartTitle == "" ? false : true,
                    ChildListDef = new FilterpaneListboxObjectViewListDef { Data = new FilterpaneListboxObjectViewDef() },
                    Visualization = "filterpane"
                };

                var filterpane = Sheet.CreateFilterpane(Id, props);
                var dim1Props = new ListboxProperties
                {
                    Title = md1.Name,
                    ListObjectDef = new ListboxListObjectDef
                    {
                        InitialDataFetch = new[] { Pager.Default },
                        Def =
                            new ListboxListObjectDimensionDef
                            {
                                FieldDefs = new List<string> { md1.Expression },
                                FieldLabels = new List<string> { md1.Name },
                                CId = ClientExtension.GetCid()
                            }
                    }
                };
                var dim2Props = new ListboxProperties
                {
                    Title = md2.Name,
                    ListObjectDef = new ListboxListObjectDef
                    {
                        InitialDataFetch = new[] { Pager.Default },
                        Def =
                            new ListboxListObjectDimensionDef
                            {
                                FieldDefs = new List<string> { md2.Expression },
                                FieldLabels = new List<string> { md2.Name },
                                CId = ClientExtension.GetCid()
                            }
                    }
                };
                var dim3Props = new ListboxProperties
                {
                    Title = md3.Name,
                    ListObjectDef = new ListboxListObjectDef
                    {
                        InitialDataFetch = new[] { Pager.Default },
                        Def =
                            new ListboxListObjectDimensionDef
                            {
                                FieldDefs = new List<string> { md3.Expression },
                                FieldLabels = new List<string> { md3.Name },
                                CId = ClientExtension.GetCid()
                            }
                    }
                };
                filterpane.CreateListbox(md1.Id, dim1Props);
                filterpane.CreateListbox(md2.Id, dim2Props);
                filterpane.CreateListbox(md3.Id, dim3Props);

                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var fpaneCell = Sheet.CellFor(FPane);
                    fpaneCell.SetBounds(row, col, width, height);
                }
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating filter pane.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                FPane = null;
            }
            //return FPane;
        }

        public void QlikSenseCreateMapPoint(string SheetID, string Id, string Dimension1, string Measure1,
            string ChartTitle = "",
            int col = 0, int row = 0, int width = 0, int height = 0)
        {
            method = "QlikSenseCreateMapPoint";

            IMap Map = null;

            try
            {
                ISheet Sheet = qlikSenseApp.GetSheet(SheetID);
                QlikSenseMasterItem mm1 = GetMasterMeasure(Measure1);
                QlikSenseMasterItem md1 = GetMasterDimension(Dimension1);

                MapLayerDataContainer mapLayerData = new MapLayerDataContainer(Qlik.Sense.Client.Visualizations.MapComponents.LayerType.Point)
                {
                    Dimension = qlikSenseApp.GetDimension(md1.Id),
                    Measure = qlikSenseApp.GetMeasure(mm1.Id)
                };

                Map = Sheet.CreateMap(Id, new List<MapLayerDataContainer> { mapLayerData });

                if (col > 0 && row > 0 && width > 0 && height > 0)
                {
                    var mappointCell = Sheet.CellFor(Map);
                    mappointCell.SetBounds(row, col, width, height);
                }
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error creating map point.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                Map = null;
            }
            //return Map;
        }

        public string QlikSenseCreateApp(string FileName, string AppName = "Telegram", string FolderConnection = "TelegramFiles")
        {
            method = "QlikSenseCreateApp";

            IAppIdentifier myAppId;

            try
            {
                try
                {
                    myAppId = qlikSenseLocation.AppWithNameOrDefault(AppName);
                    if (myAppId != null)
                        qlikSenseLocation.Delete(myAppId);
                }
                catch (Exception ex) { }

                myAppId = qlikSenseLocation.CreateAppWithName(AppName);
                IApp myApp = qlikSenseLocation.App(myAppId);
                string localizationScript = myApp.GetEmptyScript();
                string newScript = "";

                if (Path.GetExtension(FileName).ToLower() == ".csv")
                {
                    newScript = localizationScript + "\n\nLOAD  * FROM [lib://" + FolderConnection + "/" + FileName
                        + "] (txt, utf8, embedded labels, delimiter is ';', msq);";
                }
                else if (Path.GetExtension(FileName) == ".xls" || Path.GetExtension(FileName) == ".xlsx")
                {
                    return null;
                }

                myApp.SetScript(newScript);
                string currentScript = myApp.GetScript();
                bool didReload = myApp.DoReload();
                if (!didReload) log.Info(module, method, "The app " + AppName + " did not reload.", true); //Console.WriteLine("The app " + AppName + " did not reload.");

                myApp.DoSave();
            }
            catch (Exception e)
            {
                if (e.HResult == -2146233074)
                {
                    log.Error(module, method, "You cannot create apps :-(", true);
                    log.Error(module, method, e.Message);
                    log.Error(module, method, e.StackTrace);
                    //Console.WriteLine("You cannot create apps :-(");
                }
                else
                {
                    log.Error(module, method, "Error creating qlik sense app.", true);
                    log.Error(module, method, e.Message);
                    log.Error(module, method, e.StackTrace);
                    //Console.WriteLine(e);
                }
                return null;
            }

            return myAppId.AppId;
        }

        public bool QlikSensePublishApp(string StreamId, string NewName = null)
        {
            method = "QlikSensePublishApp";

            try
            {
                qlikSenseApp.Publish(StreamId, NewName);
                return true;
            }
            catch (Exception e)
            {
                // Error
                log.Error(module, method, "Error publishing qlik sense app.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine(e);
                return false;
            }
        }

        public void QlikSenseCreateMasterItemsFromFields()
        {
            if (masterDimensions.Count + masterMeasures.Count + masterVisualizations.Count > 0)
                return;

            List<string> Aggregations = new List<string> { "Sum", "Sum", "Sum", "Count", "Count", "Count", "Count", "Count", "Count", "Avg", "Max", "Min" };

            foreach (QlikSenseField field in appFields)
            {
                List<string> tags = field.Tags.ToList();

                if (tags.Contains("$hidden") || tags.Contains("$system") || tags.Contains("$key")
                    || field.FieldToSearch.Contains("fax") || field.FieldToSearch.Contains("phone")
                    || field.FieldToSearch.StartsWith("id") || field.FieldToSearch.EndsWith("id")
                    || field.FieldToSearch.StartsWith("code") || field.FieldToSearch.EndsWith("code")
                    || field.FieldToSearch.StartsWith("_")
                    )
                    continue;
                else if (tags.Contains("$text") || tags.Contains("$ascii")
                    || field.FieldToSearch.Contains("name") || field.FieldToSearch.Contains("desc") || field.FieldToSearch.Contains("address")
                    || field.FieldToSearch.Contains("title") || field.FieldToSearch.Contains("city") || field.FieldToSearch.Contains("customer")
                    || field.FieldToSearch.Contains("nombre") || field.FieldToSearch.Contains("direcc") || field.FieldToSearch.Contains("titulo")
                    || field.FieldToSearch.Contains("título") || field.FieldToSearch.Contains("ciudad") || field.FieldToSearch.Contains("cliente"))
                {
                    string dim = field.FieldNameInApp;
                    IDimension myDimension = qlikSenseApp.CreateDimension(dim,
                        new DimensionProperties
                        {
                            MetaDef = new MetaAttributesDef
                            {
                                Title = dim,
                                Description = "Dimension " + dim + " created by the Qlik Sense Bot"
                            },
                            Dim = new NxLibraryDimensionDef
                            {
                                FieldDefs = new[] { dim },
                                FieldLabels = new[] { dim }
                            }
                        });
                }
                else if (tags.Contains("$date") || tags.Contains("$timestamp")
                    || field.FieldToSearch.Contains("date") || field.FieldToSearch.Contains("time") || field.FieldToSearch.Contains("year")
                    || field.FieldToSearch.Contains("month") || field.FieldToSearch.Contains("week") || field.FieldToSearch.Contains("quarter")
                    || field.FieldToSearch.Contains("fecha") || field.FieldToSearch.Contains("hora") || field.FieldToSearch.Contains("año")
                    || field.FieldToSearch.Contains("mes") || field.FieldToSearch.Contains("semana") || field.FieldToSearch.Contains("trimestre")
                    )
                {
                    string dim = field.FieldNameInApp;
                    IDimension myDimension = qlikSenseApp.CreateDimension(dim,
                        new DimensionProperties
                        {
                            MetaDef = new MetaAttributesDef
                            {
                                Title = dim,
                                Description = "Dimension " + dim + " created by the Qlik Sense Bot"
                            },
                            Dim = new NxLibraryDimensionDef
                            {
                                FieldDefs = new[] { dim },
                                FieldLabels = new[] { dim }
                            }
                        });
                }
                else if (tags.Contains("$geoname") || tags.Contains("$geopoint") || tags.Contains("$geomultipolygon")
                    || field.FieldToSearch.Contains("latitude") || field.FieldToSearch.Contains("longitude"))
                {
                    string dim = field.FieldNameInApp;
                    IDimension myDimension = qlikSenseApp.CreateDimension(dim,
                        new DimensionProperties
                        {
                            MetaDef = new MetaAttributesDef
                            {
                                Title = dim,
                                Description = "Dimension " + dim + " created by the Qlik Sense Bot"
                            },
                            Dim = new NxLibraryDimensionDef
                            {
                                FieldDefs = new[] { dim },
                                FieldLabels = new[] { dim }
                            }
                        });
                }
                else if (tags.Contains("$numeric") || tags.Contains("$integer")
                    || field.FieldToSearch.Contains("#") || field.FieldToSearch.Contains("$") || field.FieldToSearch.Contains("€")
                    || field.FieldToSearch.Contains("sales") || field.FieldToSearch.Contains("cos") || field.FieldToSearch.Contains("discount")
                    || field.FieldToSearch.Contains("quantity") || field.FieldToSearch.Contains("units") || field.FieldToSearch.Contains("salary")
                    || field.FieldToSearch.Contains("ventas") || field.FieldToSearch.Contains("descuento")
                    || field.FieldToSearch.Contains("cantidad") || field.FieldToSearch.Contains("unidad") || field.FieldToSearch.Contains("salario")
                    )
                {
                    string meas = field.FieldNameInApp;
                    IMeasure myMeasure = qlikSenseApp.CreateMeasure(meas,
                        new MeasureProperties
                        {
                            Measure = new NxLibraryMeasureDef
                            {
                                Def = "Sum([" + meas + "])",
                                Label = meas
                            },
                            MetaDef = new MetaAttributesDef
                            {
                                Title = meas,
                                Description = "Measure " + meas + " created by the Qlik Sense Bot"
                            }
                        });
                }
                else if (!field.FieldToSearch.Contains("id") && !field.FieldToSearch.Contains("cod"))
                {
                    string meas = field.FieldNameInApp;
                    string aggr = Aggregations.PickRandom();

                    IMeasure myMeasure = qlikSenseApp.CreateMeasure(meas,
                        new MeasureProperties
                        {
                            Measure = new NxLibraryMeasureDef
                            {
                                Def = aggr + "([" + meas + "])",
                                Label = aggr + " of " + meas
                            },
                            MetaDef = new MetaAttributesDef
                            {
                                Title = aggr + " of " + meas,
                                Description = "Measure " + meas + " created by the Qlik Sense Bot"
                            }
                        });
                }
            }
            qlikSenseApp.DoSave();
            QlikSenseReadMasterItems();
        }

        private void QlikSenseReadStories()
        {
            stories.Clear();
            foreach (Qlik.Sense.Client.Storytelling.IStory AppStory in Qlik.Sense.Client.AppExtensions.GetStories(qlikSenseApp))
            {
                QlikSenseStory qlikSenset = new QlikSenseStory();
                qlikSenset.Id = AppStory.Id;
                qlikSenset.Name = AppStory.Properties.MetaDef.Title;
                stories.Add(qlikSenset);
            }
        }

        private void QlikSenseReadFields()
        {
            appFields.Clear();
            Qlik.Sense.Client.IFieldList fieldList = qlikSenseApp.GetFieldList();
            latitudeField = null;
            longitudeField = null;
            addressField = null;
            geoLocationActive = false;

            foreach (NxFieldDescription field in fieldList.Items)
            {
                QlikSenseField f = new QlikSenseField();
                f.FieldNameInApp = field.Name;
                f.FieldToSearch = field.Name.ToLower().Trim();
                f.Tags = field.Tags;
                appFields.Add(f);

                if (f.FieldToSearch == "latitude") latitudeField = f;
                if (f.FieldToSearch.Contains("latitude") && latitudeField == null) latitudeField = f;

                if (f.FieldToSearch == "longitude") longitudeField = f;
                if (f.FieldToSearch.Contains("longitude") && longitudeField == null) longitudeField = f;

                if (f.FieldToSearch == "address") addressField = f;
                if (f.FieldToSearch.Contains("address") && addressField == null) addressField = f;
            }

            if (latitudeField != null && longitudeField != null) geoLocationActive = true;
        }

        private void QlikSenseReadMasterItems()
        {
            method = "QlikSenseReadMasterItems";

            _masterVisualizations.Clear();
            try
            {
                var allMasterObjects = GetAllMasterObjects(qlikSenseApp);

                foreach (IMasterObject mo in allMasterObjects)
                {
                    QlikSenseMasterItem mi = new QlikSenseMasterItem();
                    var properties = mo.Properties;
                    mi.Id = properties.Info.Id;
                    mi.Name = properties.MetaDef.Title;
                    mi.Tags = mo.MetaAttributes.Tags;
                    masterVisualizations.Add(mi);
                }
                _masterVisualizations = (List<QlikSenseMasterItem>)masterVisualizations.Shuffle();
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error reading master items (Visualizations).", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine("QlikSenseUtil Error in QlikSenseReadMasterItems: {0} Exception caught.", e);
            }

            _masterMeasures.Clear();
            try
            {
                var allMeasures = qlikSenseApp.GetMeasureList().Items;

                foreach (IMeasureObjectViewListContainer mm in allMeasures)
                {
                    QlikSenseMasterItem mi = new QlikSenseMasterItem();
                    INxLibraryMeasure md = qlikSenseApp.GetMeasure(mm.Info.Id).NxLibraryMeasure;
                    mi.Id = mm.Info.Id;
                    mi.Name = md.Label;
                    mi.Tags = mm.Data.Tags;
                    mi.Expression = md.Def;
                    //mi.Tags = mm
                    mi.FormattedExpression = GetExpressionFormattedValue(Measure: mi.Expression, Label: mi.Name);
                    masterMeasures.Add(mi);
                }
                _masterMeasures = (List<QlikSenseMasterItem>)masterMeasures.Shuffle();
                lastMeasure = masterMeasures.Count > 0 ? masterMeasures.First() : null;
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error reading master items (Measures).", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine("QlikSenseUtil Error in QlikSenseReadMasterItems: {0} Exception caught.", e);
            }

            _masterDimensions.Clear();
            try
            {
                var allDimensions = qlikSenseApp.GetDimensionList().Items;

                foreach (DimensionObjectViewListContainer md in allDimensions)
                {
                    if (md.Data.Grouping == NxGrpType.GRP_NX_NONE)
                    {
                        QlikSenseMasterItem mi = new QlikSenseMasterItem();

                        INxLibraryDimension dd = qlikSenseApp.GetDimension(md.Info.Id).NxLibraryDimension;
                        mi.Id = md.Info.Id;
                        mi.Name = md.Data.Title;
                        mi.Expression = dd.FieldDefs.First();
                        masterDimensions.Add(mi);
                    }
                }
                _masterDimensions = (List<QlikSenseMasterItem>)masterDimensions.Shuffle();
                lastDimension = masterDimensions.Count > 0 ? masterDimensions.First() : null;
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error reading master items (Dimensions).", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine("QlikSenseUtil Error in QlikSenseReadMasterItems: {0} Exception caught.", e);
            }
        }

        private static IEnumerable<IMasterObject> GetAllMasterObjects(IApp app)
        {
            return app.GetMasterObjectList().Items?.Select(item => app.GetObject<MasterObject>(item.Info.Id));
        }

        public string QlikSenseFindField(string FieldName)
        {
            QlikSenseField field = appFields.Find(f => f.FieldToSearch == FieldName.ToLower().Trim());
            if (field != null)
            {
                return field.FieldNameInApp;
            }
            else
            {
                return null;
            }
        }

        public QlikSenseMasterItem GetMasterMeasure(string MeasureName)
        {
            QlikSenseMasterItem meas = masterMeasures.Find(m => m.Name.ToLower().Trim() == MeasureName.ToLower().Trim());

            if (meas == null)
            {
                meas = masterMeasures.Find(m => m.Name.ToLower().Trim().Contains(MeasureName.ToLower().Trim()));
            }
            if (meas == null)
            {
                var result = masterMeasures.LevenshteinDistanceOf(m => m.Name)
                    .ComparedTo(MeasureName)
                    .OrderBy(m => m.Distance);
                if (result.Count() > 0)
                    meas = (QlikSenseMasterItem)result.First().Item;
            }

            if (meas != null)
            {
                lastMeasure = meas;
                return meas;
            }
            else
            {
                return null;
            }
        }

        public string GetDimensionExpression(string DimensionName)
        {
            string DimensionExpression;

            QlikSenseMasterItem dim = masterDimensions.Find(d => d.Name.ToLower().Trim() == DimensionName.ToLower().Trim());

            if (dim != null)
            {
                DimensionExpression = dim.Expression;
                return DimensionExpression;
            }
            else
            {
                DimensionExpression = QlikSenseFindField(DimensionName);

                if (DimensionExpression != null)
                {
                    return DimensionExpression;
                }
                else
                {
                    dim = masterDimensions.Find(d => d.Name.ToLower().Trim().Contains(DimensionName.ToLower().Trim()));
                    if (dim != null)
                    {
                        DimensionExpression = dim.Expression;
                        return DimensionExpression;
                    }
                    else
                    {
                        var result = masterDimensions.LevenshteinDistanceOf(d => d.Name)
                            .ComparedTo(DimensionName)
                            .OrderBy(d => d.Distance);
                        if (result.Count() > 0)
                            dim = (QlikSenseMasterItem)result.First().Item;
                        if (dim != null)
                        {
                            DimensionExpression = dim.Expression;
                            return DimensionExpression;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public QlikSenseMasterItem GetMasterDimension(string DimensionName)
        {
            QlikSenseMasterItem dim = masterDimensions.Find(d => d.Name.ToLower().Trim() == DimensionName.ToLower().Trim());

            if (dim == null)
            {
                dim = masterDimensions.Find(d => d.Name.ToLower().Trim().Contains(DimensionName.ToLower().Trim()));
            }

            if (dim == null)
            {
                var result = masterDimensions.LevenshteinDistanceOf(d => d.Name)
                    .ComparedTo(DimensionName)
                    .OrderBy(d => d.Distance);
                if (result.Count() > 0)
                    dim = (QlikSenseMasterItem)result.First().Item;
            }

            lastDimension = dim;
            return dim;
        }

        public QlikSenseFilter[] QlikSenseSearch(string MyText, bool isInLine = true)
        {
            QlikSenseFilter[] Founds;

            SearchCombinationOptions MySearchOptions = new SearchCombinationOptions();
            MySearchOptions.Context = SearchContextType.CONTEXT_CLEARED;

            SearchPage MySearchPage = new SearchPage();
            MySearchPage.Count = 5;

            string[] MySearchTerms = { MyText };


            SearchResult MyResult = qlikSenseApp.SearchResults(MySearchOptions, MySearchTerms, MySearchPage);

            Founds = new QlikSenseFilter[5];
            var i = 0;

            foreach (var group in MyResult.SearchGroupArray)
            {
                foreach (var item in group.Items)
                {
                    QlikSenseFilter f = new QlikSenseFilter();

                    f.Dimension = item.Identifier;
                    f.DimensionExpression = GetDimensionExpression(item.Identifier);
                    f.Element = item.ItemMatches.First().Text;
                    f.isInLine = isInLine;
                    Founds[i] = f;
                    i++;
                }
            }

            return Founds;

        }

        public QlikSenseFilter[] QlikSenseSearchInDimension(string MyText, MasterObject Dim)
        {
            QlikSenseFilter[] Founds;

            SearchCombinationOptions MySearchOptions = new SearchCombinationOptions();
            MySearchOptions.Context = SearchContextType.CONTEXT_CLEARED;

            SearchPage MySearchPage = new SearchPage();
            MySearchPage.Count = 5;

            string[] MySearchTerms = { MyText };


            SearchResult MyResult = qlikSenseApp.SearchResults(MySearchOptions, MySearchTerms, MySearchPage);

            Founds = new QlikSenseFilter[5];
            var i = 0;

            foreach (var group in MyResult.SearchGroupArray)
            {
                foreach (var item in group.Items)
                {
                    QlikSenseFilter f = new QlikSenseFilter();

                    f.Dimension = item.Identifier;
                    f.Element = item.ItemMatches.First().Text;
                    Founds[i] = f;
                    i++;
                }
            }

            return Founds;

        }

        public QlikSenseObject[] QlikSenseSearchObjects(string MyText, bool ShowSelectionsBar = false)
        {
            List<string> Founds = new List<string>();
            List<string> Ids = new List<string>();
            List<string> OTypes = new List<string>();

            SearchCombinationOptions MySearchOptions = new SearchCombinationOptions();
            MySearchOptions.Context = SearchContextType.CONTEXT_CLEARED;

            SearchPage MySearchPage = new SearchPage();
            MySearchPage.Count = 1;
            MySearchPage.MaxNbrFieldMatches = maxFounds;

            SearchGroupOptions[] qGroupOptions = new SearchGroupOptions[1];
            SearchGroupOptions sgo = new SearchGroupOptions();
            sgo.Count = maxFounds;
            sgo.GroupType = SearchGroupType.GENERIC_OBJECTS_GROUP;
            qGroupOptions[0] = sgo;

            SearchGroupItemOptions[] qGroupItemOptions = new SearchGroupItemOptions[1];
            SearchGroupItemOptions sgio = new SearchGroupItemOptions();
            sgio.Count = 1;
            sgio.GroupItemType = SearchGroupItemType.GENERIC_OBJECT;
            qGroupItemOptions[0] = sgio;

            MySearchPage.GroupOptions = qGroupOptions;
            MySearchPage.GroupItemOptions = qGroupItemOptions;

            char[] sep = { ' ' };
            string[] MySearchTerms = MyText.Split(sep);

            SearchObjectOptions MySearchObjectOptions = new SearchObjectOptions();


            SearchResult MyObjects = qlikSenseApp.SearchObjects(MySearchObjectOptions, MySearchTerms, MySearchPage);

            foreach (var obGroup in MyObjects.SearchGroupArray)
            {
                foreach (var obItem in obGroup.Items)
                {
                    if (Founds.Count > maxFounds) break;

                    GenericObject MyObject = qlikSenseApp.GetGenericObject(obItem.Identifier);

                    Qlik.Sense.Client.Visualizations.VisualizationBaseProperties MyProp = MyObject.Properties as Qlik.Sense.Client.Visualizations.VisualizationBaseProperties;

                    string t;
                    t = MyProp.Title;
                    if (t.IndexOf("'") > -1 && t.IndexOf("&") > -1) t = qlikSenseApp.Evaluate(t);
                    Founds.Add(t);
                    Ids.Add(obItem.Identifier);
                    OTypes.Add(MyProp.Visualization);
                }
            }

            QlikSenseObject[] qlikSenseFounds;

            if (Founds.Count < maxFounds)
            {
                qlikSenseFounds = new QlikSenseObject[Founds.Count];
            }
            else
            {
                qlikSenseFounds = new QlikSenseObject[maxFounds];
            }


            for (int i = 0; i < Founds.Count && i < maxFounds; i++)
            {
                if (Founds[i] == "") Founds[i] = "-----";
                qlikSenseFounds[i] = new QlikSenseObject();
                qlikSenseFounds[i].Description = Founds[i];
                qlikSenseFounds[i].ObjectType = OTypes[i];
                qlikSenseFounds[i].ObjectId = Ids[i];
                qlikSenseFounds[i].ObjectURL = qlikSenseSingleServer + "/single?appid=" + qlikSenseAppId + "&obj=" + Ids[i]; //+ "&select=clearall";
                if (ShowSelectionsBar) qlikSenseFounds[i].ObjectURL += "&opt=currsel";
                qlikSenseFounds[i].HRef = "<" + qlikSenseFounds[i].ObjectURL + "|" + Founds[i] + ">";
                qlikSenseFounds[i].ThumbURL = qlikSenseSingleServer + "/resources/qlikSenseimg/" + OTypes[i] + ".png";
            }
            return qlikSenseFounds;
        }

        public string GetExpression(string Expression)
        {
            method = "GetExpression";

            string exp;

            try
            {
                exp = qlikSenseApp.Evaluate(Expression);
            }
            catch (Exception e)
            {
                log.Error(module, method, "Error getting expression.", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                //Console.WriteLine("QlikSenseUtil Error: {0} Exception caught.", e);
                exp = "";
            }

            if (Expression == "" || exp.StartsWith("Error:"))
            {
                exp = "";
            }

            return (exp);
        }

        public string ApplyGeoFilter(string LatField, string LatFilter, string LonField, string LonFilter, string IDField)
        {
            string Selection = "";

            qlikSenseApp.ClearAll();
            qlikSenseApp.GetField(LatField).Select(LatFilter);
            qlikSenseApp.GetField(LonField).Select(LonFilter);

            IAppField fID = qlikSenseApp.GetAppField(IDField);

            var p = new List<NxPage> { new NxPage { Height = 30, Width = 1 } };
            //var dataPages = fID.GetData(p);
            var dataPages = fID.GetOptional(p);

            foreach (var dataPage in dataPages)
            {
                var matrix = dataPage.Matrix;
                foreach (var cellRows in matrix)
                {
                    foreach (var cellRow in cellRows)
                    {
                        Selection = Selection + ',' + cellRow.Text.Replace(',', '.');
                    }
                }
            }

            if (Selection != string.Empty)
            {
                Selection = "&select=" + IDField + Selection;
            }
            return Selection;
        }

        public QlikSenseDataList[] GetDataList(QlikSenseMasterItem Measure, string Dimension, List<QlikSenseFilter> Filters = null, int NoOfRows = 0, bool Descending = true,
            List<double?> MeasureThreshold = null)
        {
            method = "GetDataList";

            string measureExpression = Measure.Expression;

            List<QlikSenseDataList> gl = new List<QlikSenseDataList>();

            if (NoOfRows == 0) NoOfRows = maxFounds;

            qlikSenseApp.ClearAll();

            if (Filters != null && Filters.Count > 0)
            {
                foreach (QlikSenseFilter ff in Filters)
                {
                    try
                    {
                        string DimExpression = GetDimensionExpression(ff.Dimension);
                        if (DimExpression != null)
                            if (ff.SetOfElement != null)
                            {
                                List<FieldValue> FieldValues = new List<FieldValue>();

                                foreach (string item in ff.SetOfElement)
                                {
                                    FieldValue fieldValue = new FieldValue() { Number = Convert.ToInt32(Convert.ToDateTime(item).ToOADate()), IsNumeric = true };
                                    FieldValues.Add(fieldValue);
                                }

                                qlikSenseApp.GetField(DimExpression).SelectValues(FieldValues);
                                //qlikSenseApp.GetField("Client Name").SelectValues(new List<FieldValue>() { new FieldValue() { Text = "Duff Beer" }, new FieldValue() { Text = "Internal" } }, true, true);
                            }
                            else
                                qlikSenseApp.GetField(DimExpression).Select(ff.Element.ToUpper() + "*");

                        //if (DimExpression != null)
                        //    qlikSenseApp.GetField(DimExpression).Select(ff.Element.ToUpper() + "*");
                    }
                    catch (Exception e)
                    {
                        log.Error(module, method, string.Format("Error getting data list with filter \"{0} ={1}\".", ff.Dimension, ff.Element), true);
                        log.Error(module, method, e.Message);
                        log.Error(module, method, e.StackTrace);
                        //Console.WriteLine("QlikSenseUtil Error in GetDataList with filter \"{0} ={1}\": {2} Exception caught.", ff.Dimension, ff.Element, e);
                    }
                }
            }

            var myDimension1 = new NxDimension
            {
                Def = new NxInlineDimensionDef()
            };
            myDimension1.Def.FieldDefs = new[] { Dimension };

            SortCriteria dsc = new SortCriteria();
            if (Descending)
                dsc.SortByExpression = SortDirection.Descending;
            else
                dsc.SortByExpression = SortDirection.Ascending;

            dsc.Expression = measureExpression;
            myDimension1.Def.SortCriterias = new[] { dsc };

            var myMeasure = new NxMeasure
            {
                Def = new NxInlineMeasureDef()
            };
            if (measureExpression.First() != '=')
                measureExpression = "=" + measureExpression;
            myMeasure.Def.Def = measureExpression;

            if (MeasureThreshold != null && MeasureThreshold.Count() > 0)
            {
                if (measureExpression.First() == '=') measureExpression = measureExpression.Remove(0, 1);

                if (MeasureThreshold.Count() == 2)
                    measureExpression = string.Format("=if({0} > {1} AND {0} < {2}, {0}, Null())", measureExpression, MeasureThreshold[0].ToString(), MeasureThreshold[1].ToString()); //=if(exp < 0 AND exp > 100, exp, Null())
                else
                    measureExpression = string.Format("=if({0} {2} {1}, {0}, Null())", measureExpression, MeasureThreshold[0].ToString(), Descending ? ">" : "<");
            }
            myMeasure.Def.Def = measureExpression;

            HyperCubeDef MyCubeDef = new HyperCubeDef();
            MyCubeDef.Dimensions = new List<NxDimension>
            {
                new NxDimension {Def = myDimension1.Def },
            };
            MyCubeDef.Measures = new List<NxMeasure>
            {
                new NxMeasure { Def = myMeasure.Def }
            };

            MyCubeDef.InitialDataFetch = new List<NxPage>
            {
               new NxPage {Height = NoOfRows, Width = 5}
            };

            MyCubeDef.SuppressMissing = true;
            MyCubeDef.SuppressZero = true;

            GenericObjectProperties gp = new GenericObjectProperties();
            gp.Info = new NxInfo();
            gp.Info.Type = "hypercube";
            gp.Set<HyperCubeDef>("qHyperCubeDef", MyCubeDef);
            GenericObject obj = qlikSenseApp.CreateGenericSessionObject(gp);

            var p = new List<NxPage> { new NxPage { Height = NoOfRows, Width = 5 } };
            var dataPages = obj.GetHyperCubeData("/qHyperCubeDef", p);

            foreach (var dataPage in dataPages)
            {
                var matrix = dataPage.Matrix;
                foreach (var cellRows in matrix)
                {
                    if (cellRows.Count > 1)
                    {
                        gl.Add(new QlikSenseDataList
                        {
                            DimValue = cellRows[0].Text,
                            MeasValue = cellRows[1].Num,
                            MeasFormattedValue = FormatValue(cellRows[1].Num, Measure.Name)
                        });
                    }
                }
            }

            return gl.ToArray();
        }

        public string GetExpressionFormattedValue(string Measure, List<QlikSenseFilter> Filters = null, string Label = "")
        {
            return FormatValue(GetExpressionValue(Measure, Filters), Label);
        }

        public double GetExpressionValue(string Measure, List<QlikSenseFilter> Filters = null)
        {
            method = "GetExpressionValue";

            double val = 0;

            qlikSenseApp.ClearAll();
            if (Filters != null && Filters.Count > 0)
            {
                foreach (QlikSenseFilter ff in Filters)
                {
                    try
                    {
                        string DimExpression = GetDimensionExpression(ff.Dimension);
                        if (DimExpression != null)
                            if (ff.SetOfElement != null)
                            {
                                List<FieldValue> FieldValues = new List<FieldValue>();

                                foreach (string item in ff.SetOfElement)
                                {
                                    FieldValue fieldValue = new FieldValue() { Number = Convert.ToInt32(Convert.ToDateTime(item).ToOADate()), IsNumeric = true };
                                    FieldValues.Add(fieldValue);
                                }

                                var a = qlikSenseApp.GetField(DimExpression.Replace("=", ""));
                                qlikSenseApp.GetField(DimExpression.Replace("=", "")).SelectValues(FieldValues);
                                //qlikSenseApp.GetField("Client Name").SelectValues(new List<FieldValue>() { new FieldValue() { Text = "Duff Beer" }, new FieldValue() { Text = "Internal" } }, true, true);
                            }
                            else
                                qlikSenseApp.GetField(DimExpression).Select(ff.Element.ToUpper() + "*");
                    }
                    catch (Exception e)
                    {
                        log.Error(module, method, string.Format("Error getting expression value with filter \"{0} ={1}\".", ff.Dimension, ff.Element), true);
                        log.Error(module, method, e.Message);
                        log.Error(module, method, e.StackTrace);
                        //Console.WriteLine("QlikSenseUtil Error in GetExpressionValue with filter \"{0} ={1}\": {2} Exception caught.", ff.Dimension, ff.Element, e);
                    }
                }
            }

            var myMeasure = new NxMeasure
            {
                Def = new NxInlineMeasureDef()
            };
            if (Measure.First() != '=') Measure = "=" + Measure;
            myMeasure.Def.Def = Measure;

            HyperCubeDef MyCubeDef = new HyperCubeDef();
            MyCubeDef.Measures = new List<NxMeasure>
            {
                new NxMeasure { Def = myMeasure.Def }
            };

            MyCubeDef.InitialDataFetch = new List<NxPage>
            {
                new NxPage {Height = 1, Width = 1}
            };

            GenericObjectProperties gp = new GenericObjectProperties();
            gp.Info = new NxInfo();
            gp.Info.Type = "hypercube";
            gp.Set<HyperCubeDef>("qHyperCubeDef", MyCubeDef);
            GenericObject obj = qlikSenseApp.CreateGenericSessionObject(gp);

            var p = new List<NxPage> { new NxPage { Height = 1, Width = 1 } };
            var dataPages = obj.GetHyperCubeData("/qHyperCubeDef", p);
            if (dataPages.Count() > 0)
            {
                var cr = dataPages.First().Matrix.First();
                val = cr[0].Num;
            }

            return val;
        }

        private string FormatValue(double Value, string Label = "")
        {
            string strValue = null;

            if (Value != 0)
            {
                strValue = Value.ToString("N2");

                var measure = qlikSenseApp.GetMeasureList().Items.Where(x => x.Data.Title == Label).ToList();

                if (measure.Count() > 0)
                {
                    foreach (var tag in measure[0].Data.Tags)
                    {
                        if (tag == "_formatMoney")
                            return strValue = Math.Round(Value).ToString("C0");
                        else if (tag == "_formatPercent")
                            return strValue = Value.ToString("P1");
                        else if (tag == "_formatNumber")
                            return strValue = Value.ToString("N0");
                        //else
                        //    return strValue = Value.ToString("N2");
                    }
                }

                //if (Label.Contains("%")) strValue = Value.ToString("P1");
                //else if (Value > 0 && Value < 1) strValue = Value.ToString("P1");
                //else if (Label.Contains("€") || Label.Contains("$")) strValue = Value.ToString("C2");
                //else strValue = Value.ToString("N2");
            }
            else
            {
                strValue = "0";
            }

            return strValue;
        }

        public void QlikSenseCheckAlerts(string ReportName = "ReportSales.pdf")
        {
            foreach (var a in alertList)
            {
                a.AlertMessage = a.AlertRequest;
                int i = rnd.Next(1, 9);
                a.AlertPhoto = string.Format("chart{0}.jpg", i.ToString());
                a.AlertDoc = ReportName;
                a.AlertActive = true;
            }
        }

        public void ClearAllFilter()
        {
            method = "ClearAllFilter";

            try
            {
                qlikSenseApp.ClearAll();
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Getting error while clearing filters...", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
            }
        }

        #region Narratives
        public string SendRequest(string jsonData)
        {
            try
            {
                method = "Narrative Request";
                log.Info(module, method, "Sending narrative request...", true);

                //set request url in configuration file
                string reqURL = Parameters.getConfigParameters("conReqURL");
                byte[] bytes;
                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

                bytes = Encoding.ASCII.GetBytes(jsonData);

                using (var client = new MultipartContent("form-data", boundary))
                {
                    using (var content = new MultipartFormDataContent(boundary))
                    {
                        content.Add(new StreamContent(new MemoryStream(bytes)), "values");
                        var responseStrings = reqURL.PostAsync(content).ReceiveString().Result;
                        log.Info(module, method, "narrative fetched...", true);
                        return responseStrings;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Error while sending narratiive request.", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
                return string.Empty;
            }
        }

        public string getNarratives(ref string chartID, ref string imageFileName, dynamic chart, dynamic properties, string chartName, IEnumerable<string> tags4Measure1, IEnumerable<string> tags4Measure2 = null, IEnumerable<string> tags4Measure3 = null)
        {
            method = "Narrative Request";
            log.Info(module, method, "getNarratives started...", true);

            string ObjectID = chartID;
            string imageFile = imageFileName;

            ////
            //Task generateChart = new Task(() => getChartTable(ObjectID, chartName, imageFile));
            //generateChart.Start();

            Parallel.Invoke(() =>
            {
                getChartTable(ObjectID, chartName, imageFile);
            });

            Narratives narratives = new Narratives();
            narratives.apiKey = Parameters.getConfigParameters("conApiKey");
            string dataType = string.Empty;
            //var pagers = new List<NxPage>() { new NxPage { Top = 0, Left = 0, Width = chart.GetLayout().HyperCube.Size.cx, Height = chart.GetLayout().HyperCube.Size.cy } };
            if (properties.HyperCubeDef?.Measures != null)
            {
                int measureCount = properties.HyperCubeDef.Measures.Count;
                narratives.measures = new Util.Measure[measureCount];
                int index = 0;
                foreach (var measure in properties.HyperCubeDef?.Measures)
                {
                    narratives.measures[index] = new Util.Measure();
                    narratives.measures[index].id = index;
                    narratives.measures[index].cumulative = "yes";
                    IEnumerable<string> tagList = tags4Measure1;
                    if (index == 1)
                    {
                        tagList = tags4Measure2;
                    }
                    else if (index == 2)
                    {
                        tagList = tags4Measure3;
                    }

                    var formatting = (from tag in tagList
                                      where tag.Contains("_formatMoney")
                                      select "money")
                                .Union(from tag in tagList
                                       where tag.Contains("_formatPercent")
                                       select "percent")
                                .Union(from tag in tagList
                                       where tag.Contains("_formatNumber")
                                       select "number");
                    narratives.measures[index].measureIn = formatting.SingleOrDefault() == null ? "number" : formatting.SingleOrDefault();

                    var badMeasure = (from tag in tagList
                                      where tag.Contains("_badMeasure")
                                      select "bad");
                    narratives.measures[index].largeValue = badMeasure.SingleOrDefault() == null ? "good" : badMeasure.SingleOrDefault();

                    narratives.measures[index].value = measure.Def.Label;// = properties.HyperCubeDef?.Measures;
                    index++;
                }
            }
            if (properties.HyperCubeDef?.Dimensions != null)
            {
                int dimCount = (int)properties.HyperCubeDef?.Dimensions.Count;
                narratives.dimensions = new Util.Dimension[dimCount];
                int index = 0;
                foreach (var dimension in properties.HyperCubeDef?.Dimensions)
                {
                    narratives.dimensions[index] = new Util.Dimension();
                    narratives.dimensions[index].id = index;
                    narratives.dimensions[index].singular = "entity";
                    narratives.dimensions[index].plural = "entities";
                    narratives.dimensions[index].value = dimension.Def.FieldLabels[dimCount - 1];//.SingleOrDefault();// = properties.HyperCubeDef?.Measures;

                    if (dimension.Def.FieldLabels[0].IndexOf("date", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                            dimension.Def.FieldLabels[0].IndexOf("week", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                dimension.Def.FieldLabels[0].IndexOf("month", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                   dimension.Def.FieldLabels[0].IndexOf("year", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                        dimension.Def.FieldLabels[0].IndexOf("quarter", StringComparison.CurrentCultureIgnoreCase) != 0)
                        dataType = "discrete";
                    else
                        dataType = "continuous";
                    index++;
                }
            }
            if (chartName == "Treechart")
            {
                var matrixData1 = chart.GetHyperCubeStackData("/qHyperCubeDef", properties.HyperCubeDef?.InitialDataFetch);
                var json = System.Web.Helpers.Json.Encode(matrixData1);
                JToken hypercubeStackJson = JToken.Parse(json);
                if (hypercubeStackJson.HasValues == true)
                {
                    if (hypercubeStackJson[0] != null && hypercubeStackJson[0]["Data"] != null && hypercubeStackJson[0]["Data"][0] != null && hypercubeStackJson[0]["Data"][0]["SubNodes"] != null)
                    {
                        narratives.dataValues = new Datavalue[hypercubeStackJson[0]["Data"][0]["SubNodes"].Count()];
                        int stackDataIndex = 0, stackDataIncrement = 0;

                        foreach (var stackDataList in hypercubeStackJson[0]["Data"][0]["SubNodes"])
                        {
                            narratives.dataValues[stackDataIndex] = new Datavalue();
                            narratives.dataValues[stackDataIndex].dim = new Dim[hypercubeStackJson[0]["Data"].Count()];
                            narratives.dataValues[stackDataIndex].mea = new Mea[stackDataList["SubNodes"].Count()];

                            narratives.dataValues[stackDataIndex].dim[stackDataIncrement] = new Dim();
                            narratives.dataValues[stackDataIndex].dim[stackDataIncrement].value = stackDataList["Text"].ToString();
                            narratives.dataValues[stackDataIndex].dim[stackDataIncrement].id = stackDataIncrement;

                            narratives.dataValues[stackDataIndex].mea[stackDataIncrement] = new Mea();
                            narratives.dataValues[stackDataIndex].mea[stackDataIncrement].value = stackDataList["SubNodes"][0] != null ? stackDataList["SubNodes"][0]["Text"].ToString() : "";
                            narratives.dataValues[stackDataIndex].mea[stackDataIncrement].id = stackDataIncrement;

                            stackDataIndex++;
                        }
                    }
                }
            }
            else
            {
                var cubedata = chart.GetHyperCubeData("/qHyperCubeDef", properties.HyperCubeDef?.InitialDataFetch);
                //var cubedata1 = chart.GetHyperCubeDataAsync("/qHyperCubeDef", properties.HyperCubeDef?.InitialDataFetch);
                //var rslt = cubedata1.Result;
                if (cubedata != null)
                {
                    foreach (var data in cubedata)
                    {
                        var matrix = data.Matrix;
                        narratives.dataValues = new Datavalue[matrix.Count];
                        int index = 0;

                        foreach (var dataValue in matrix)
                        {
                            narratives.dataValues[index] = new Datavalue();
                            int indexData = 0;
                            narratives.dataValues[index].dim = new Dim[1];
                            narratives.dataValues[index].mea = new Mea[dataValue.Count - 1];
                            bool dimFlag = true;
                            foreach (var value in dataValue)
                            {
                                if (dimFlag)
                                {
                                    narratives.dataValues[index].dim[indexData] = new Dim();
                                    narratives.dataValues[index].dim[indexData].value = value.Text;
                                    dimFlag = false;
                                }
                                else
                                {
                                    narratives.dataValues[index].mea[indexData] = new Mea();
                                    narratives.dataValues[index].mea[indexData].value = value.Text;
                                    narratives.dataValues[index].mea[indexData].id = indexData;
                                    indexData++;
                                }

                            }
                            index++;
                        }
                    }
                }
            }
            narratives.extra = new Extra();
            narratives.extra.dataType = dataType;
            narratives.extra.verbosity = Convert.ToInt32(Parameters.getConfigParameters("conVerbosity"));
            narratives.extra.sheetTitle = "ChartTitle";
            narratives.extra.objectType = chartName;
            narratives.extra.hasSelection = false;
            narratives.extra.responseForm = "JSON";

            //Task<string> getNarrativeResponse = new Task<string>(() => SendRequest(System.Web.Helpers.Json.Encode(narratives)));
            //getNarrativeResponse.Start();
            //string response = getNarrativeResponse.Result;

            string response = string.Empty;

            Parallel.Invoke(() =>
            {
                response = SendRequest(System.Web.Helpers.Json.Encode(narratives));
            });

            //GetChart(chart, properties, chartName);
            log.Info(module, method, "getNarratives completed...", true);
            return refineJSON(response);
        }

        public string getVizNarrative(string vizID, string apiKey, int verbosity, string formatType, string measureLargeValue, string imageFile)
        {
            Narratives narratives = new Narratives();
            narratives.apiKey = apiKey;//set in configuration file
            string dataType = string.Empty;

            try
            {
                if (!qlikSenseLocation.IsAlive())
                    CheckConnection(qlikSenseUserId, qlikSenseHeaderAuthName, qlikSenseVirtualProxyPath);

                GenericObject genericObject = qlikSenseApp.GetGenericObject(vizID);

                var layout = genericObject.GetLayout();
                var jsonAbstractStucture = layout.PrintStructure();
                string s = string.Empty;

                if (jsonAbstractStucture != null)
                {
                    JToken hypercubeJson = JToken.Parse(jsonAbstractStucture);
                    if (hypercubeJson.HasValues)
                    {
                        //Task getChartTableTask = new Task(() => getChartTable(vizID, hypercubeJson["visualization"].ToString(), imageFile));
                        //getChartTableTask.Start();

                        Parallel.Invoke(() =>
                        {
                            //hypercubeJson["visualization"].ToString() == "treemap" ? "barchart" : hypercubeJson["visualization"].ToString()
                            getChartTable(vizID, hypercubeJson["visualization"].ToString(), imageFile);
                        });

                        JToken qHyperCube;
                        if (hypercubeJson["visualization"].ToString() == "map")
                        {
                            var qLayer = hypercubeJson["layers"];//["qHyperCube"];
                            qHyperCube = qLayer[0]["qHyperCube"];
                        }
                        else
                            qHyperCube = hypercubeJson["qHyperCube"];

                        #region dimension
                        var qDimensions = qHyperCube["qDimensionInfo"];
                        if (qDimensions.HasValues)
                        {

                            int dimCount = qDimensions.Count();
                            if (hypercubeJson["visualization"].ToString() == "map")
                                narratives.dimensions = new Util.Dimension[dimCount - 1];
                            else
                                narratives.dimensions = new Util.Dimension[dimCount];
                            int dimIndex = 0;
                            foreach (var dimension in qDimensions)
                            {
                                //if (hypercubeJson["visualization"].ToString() == "map" || dimIndex == 0)
                                //{
                                foreach (var dim in dimension)
                                {
                                    if (dim.Path.Contains("qFallbackTitle"))
                                    {
                                        //Narratives
                                        narratives.dimensions[dimIndex] = new Util.Dimension();
                                        narratives.dimensions[dimIndex].id = dimIndex;
                                        narratives.dimensions[dimIndex].singular = "entity";
                                        narratives.dimensions[dimIndex].plural = "entities";
                                        narratives.dimensions[dimIndex].value = dim.First.ToString();//.SingleOrDefault();// = properties.HyperCubeDef?.Measures;
                                        if (dim.First.ToString().IndexOf("date", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                                dim.First.ToString().IndexOf("week", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                                    dim.First.ToString().IndexOf("month", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                                        dim.First.ToString().IndexOf("year", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                                                            dim.First.ToString().IndexOf("quarter", StringComparison.CurrentCultureIgnoreCase) != 0)
                                            dataType = "discrete";
                                        else
                                            dataType = "continuous";
                                        break;
                                    }
                                }
                                if (hypercubeJson["visualization"].ToString() == "piechart")
                                    dataType = "part_of_whole";
                                //}
                                dimIndex++;
                                if (narratives.dimensions.Count() == dimCount - 1)
                                    break;
                            }
                        }
                        #endregion

                        #region Measure
                        var qMeasures = qHyperCube["qMeasureInfo"];
                        if (qMeasures.HasValues)
                        {
                            int measureCount = qMeasures.Count();
                            narratives.measures = new Util.Measure[measureCount];
                            int index = 0;
                            foreach (var measure in qMeasures)
                            {
                                foreach (var measr in measure)
                                {
                                    if (measr.Path.Contains("qFallbackTitle"))
                                    {
                                        //Narratives
                                        narratives.measures[index] = new Util.Measure();
                                        narratives.measures[index].id = index;
                                        narratives.measures[index].cumulative = "yes";
                                        narratives.measures[index].largeValue = measureLargeValue;
                                        narratives.measures[index].measureIn = formatType;
                                        narratives.measures[index].value = measr.First.ToString();// = properties.HyperCubeDef?.Measures;
                                        index++;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region DataValues
                        var qSize = qHyperCube["qSize"];
                        List<NxPage> pages;
                        if (qSize.HasValues)
                        {
                            int height, width;
                            try
                            {
                                height = Convert.ToInt32(qSize.Last.First);
                                width = Convert.ToInt32(qSize.First.First);
                            }
                            catch
                            {
                                height = 500;
                                width = 10;
                            }
                            pages = new List<NxPage> { new NxPage { Height = height, Width = width } };
                        }
                        else
                            pages = new List<NxPage> { new NxPage { Height = 500, Width = 10 } };

                        var genericProperty = genericObject.GetProperties();

                        var propertyPath = genericProperty.LayoutableDefPaths;
                        string layoutPath = "/qHyperCubeDef";
                        if (propertyPath.Length >= 1)
                            layoutPath = propertyPath[0];

                        if (hypercubeJson["visualization"].ToString() == "treemap")
                        {
                            var matrixData1 = genericObject.GetHyperCubeStackData(layoutPath, pages);
                            var json = System.Web.Helpers.Json.Encode(matrixData1);
                            JToken hypercubeStackJson = JToken.Parse(json);
                            if (hypercubeStackJson.HasValues == true)
                            {
                                if (hypercubeStackJson[0] != null && hypercubeStackJson[0]["Data"] != null && hypercubeStackJson[0]["Data"][0] != null && hypercubeStackJson[0]["Data"][0]["SubNodes"] != null)
                                {
                                    narratives.dataValues = new Datavalue[hypercubeStackJson[0]["Data"][0]["SubNodes"].Count()];
                                    int stackDataIndex = 0, stackDataIncrement = 0;

                                    foreach (var stackDataList in hypercubeStackJson[0]["Data"][0]["SubNodes"])
                                    {
                                        narratives.dataValues[stackDataIndex] = new Datavalue();
                                        narratives.dataValues[stackDataIndex].dim = new Dim[hypercubeStackJson[0]["Data"].Count()];
                                        narratives.dataValues[stackDataIndex].mea = new Mea[stackDataList["SubNodes"].Count()];

                                        narratives.dataValues[stackDataIndex].dim[stackDataIncrement] = new Dim();
                                        narratives.dataValues[stackDataIndex].dim[stackDataIncrement].value = stackDataList["Text"].ToString();
                                        narratives.dataValues[stackDataIndex].dim[stackDataIncrement].id = stackDataIncrement;

                                        narratives.dataValues[stackDataIndex].mea[stackDataIncrement] = new Mea();
                                        narratives.dataValues[stackDataIndex].mea[stackDataIncrement].value = stackDataList["SubNodes"][0] != null ? stackDataList["SubNodes"][0]["Text"].ToString() : "";
                                        narratives.dataValues[stackDataIndex].mea[stackDataIncrement].id = stackDataIncrement;

                                        stackDataIndex++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var matrixData = genericObject.GetHyperCubeData(layoutPath, pages);
                            if (matrixData != null && matrixData.Count() >= 1 && matrixData.FirstOrDefault().Matrix.Count() >= 1)
                            {
                                int index = 0;

                                foreach (var datapage in matrixData)
                                {
                                    int matrixCount = datapage.Matrix.Count();
                                    narratives.dataValues = new Datavalue[matrixCount];
                                    if (datapage?.Matrix != null)
                                    {

                                        foreach (var qmatrix in datapage?.Matrix)
                                        {
                                            var measureCount = from p in qmatrix
                                                               where p.State == StateEnumType.LOCKED
                                                               group p by p.State into gp
                                                               select new
                                                               {
                                                                   InstanceCount = gp.Count(),
                                                               };
                                            var dimensionCount = from p in qmatrix
                                                                 where p.State == StateEnumType.OPTION
                                                                 group p by p.State into gp
                                                                 select new
                                                                 {
                                                                     InstanceCount = gp.Count(),
                                                                 };
                                            narratives.dataValues[index] = new Datavalue();
                                            if (dimensionCount.FirstOrDefault() != null)
                                            {
                                                if (hypercubeJson["visualization"].ToString() == "map")
                                                    narratives.dataValues[index].dim = new Dim[dimensionCount.FirstOrDefault().InstanceCount - 1];
                                                else
                                                    narratives.dataValues[index].dim = new Dim[dimensionCount.FirstOrDefault().InstanceCount];
                                                narratives.dataValues[index].mea = new Mea[measureCount.FirstOrDefault() == null ? 0 : measureCount.FirstOrDefault().InstanceCount];
                                            }
                                            int dimIncrement = 0, meaIncrement = 0;

                                            if (qmatrix != null && qmatrix.Count() >= 1)
                                            {
                                                int datarowCount = 0;
                                                foreach (var matrix in qmatrix)
                                                {
                                                    if (dimensionCount.FirstOrDefault() is null)
                                                        break;
                                                    if (!matrix.State.ToString().ToLower().Contains("locked"))
                                                    {
                                                        if (narratives.dataValues[index].dim.Count() > dimIncrement)
                                                        {
                                                            narratives.dataValues[index].dim[dimIncrement] = new Dim();
                                                            narratives.dataValues[index].dim[dimIncrement].value = matrix.Text;
                                                            narratives.dataValues[index].dim[dimIncrement].id = dimIncrement;
                                                            dimIncrement++;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (narratives.dataValues[index].dim.Count() == 9999)
                                                        {
                                                            narratives.dataValues[index].dim = new Dim[1];
                                                            narratives.dataValues[index].mea = new Mea[1];
                                                            narratives.dataValues[index].dim[dimIncrement] = new Dim();
                                                            narratives.dataValues[index].dim[dimIncrement].value = "-";
                                                            narratives.dataValues[index].dim[dimIncrement].id = dimIncrement;
                                                            dimIncrement++;
                                                        }
                                                        else
                                                        {
                                                            narratives.dataValues[index].mea[meaIncrement] = new Mea();
                                                            narratives.dataValues[index].mea[meaIncrement].value = matrix.Text;
                                                            narratives.dataValues[index].mea[meaIncrement].id = meaIncrement;
                                                            meaIncrement++;
                                                            if (hypercubeJson["visualization"].ToString() == "map")
                                                                break;
                                                        }

                                                    }
                                                    datarowCount++;
                                                }
                                            }
                                            index++;
                                        }

                                    }

                                }
                            }
                        }

                        #endregion

                        #region Extra
                        narratives.extra = new Extra();
                        narratives.extra.dataType = dataType;
                        narratives.extra.verbosity = verbosity;//set in configuration file
                        narratives.extra.sheetTitle = "ChartTitle";
                        narratives.extra.objectType = hypercubeJson["visualization"].ToString();// == "treemap" ? "barchart" : hypercubeJson["visualization"].ToString();
                        narratives.extra.hasSelection = false;
                        narratives.extra.responseForm = "JSON";
                        #endregion
                    }
                }

                //Task<string> getNarrativeResponse = new Task<string>(() => SendRequest(System.Web.Helpers.Json.Encode(narratives)));
                //getNarrativeResponse.Start();
                //string ResponseData = getNarrativeResponse.Result;

                string ResponseData = string.Empty;
                Parallel.Invoke(() =>
                {
                    ResponseData = SendRequest(System.Web.Helpers.Json.Encode(narratives));
                });

                return refineJSON(ResponseData);
            }
            catch (Exception ex) { }

            return string.Empty;
        }

        public bool getChartTable(string objectID, string chartType, string imageFileName)
        {
            method = "Capture Image Request Request";
            log.Info(module, method, "getChartTable started...", true);
            chartType = string.Empty;
            System.Data.DataTable chartTable = new System.Data.DataTable();

            try
            {
                if (!qlikSenseLocation.IsAlive())
                    CheckConnection(qlikSenseUserId, qlikSenseHeaderAuthName, qlikSenseVirtualProxyPath);

                GenericObject genericObject = qlikSenseApp.GetGenericObject(objectID);

                var layout = genericObject.GetLayout();
                var jsonAbstractStucture = layout.PrintStructure();
                string x = string.Empty;

                if (jsonAbstractStucture != null)
                {
                    JToken hypercubeJson = JToken.Parse(jsonAbstractStucture);
                    if (hypercubeJson.HasValues)
                    {
                        JToken qHyperCube;
                        if (hypercubeJson["visualization"].ToString() == "map")
                        {
                            var qLayer = hypercubeJson["layers"];//["qHyperCube"];
                            qHyperCube = qLayer[0]["qHyperCube"];
                        }
                        else
                            qHyperCube = hypercubeJson["qHyperCube"];

                        #region dimension
                        var qDimensions = qHyperCube["qDimensionInfo"];
                        if (qDimensions.HasValues)
                        {
                            foreach (var dimension in qDimensions)
                            {
                                foreach (var dim in dimension)
                                {
                                    if (dim.Path.Contains("qFallbackTitle"))
                                    {
                                        //DataTable for Chart---Adding Dimensions
                                        chartTable.Columns.Add(dim.First.ToString());
                                        break;
                                    }
                                }
                                break;//it will take only one dimension
                            }
                        }
                        #endregion

                        //chartTable.Columns.Add("_currencyFormat");
                        //chartTable.Columns.Add("_numberFormat");

                        #region Measure
                        var qMeasures = qHyperCube["qMeasureInfo"];
                        if (qMeasures.HasValues)
                        {
                            int measureCount = qMeasures.Count();
                            foreach (var measure in qMeasures)
                            {
                                foreach (var measr in measure)
                                {
                                    if (measr.Path.Contains("qFallbackTitle"))
                                    {
                                        //DataTable for Chart---Adding Measure
                                        chartTable.Columns.Add(measr.First.ToString());
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region DataValues
                        List<NxPage> pages = new List<NxPage> { new NxPage { Height = 25, Width = 10 } };
                        var genericProperty = genericObject.GetProperties();
                        var propertyPath = genericProperty.LayoutableDefPaths;
                        string layoutPath = "/qHyperCubeDef";

                        if (propertyPath.Length >= 1)
                            layoutPath = propertyPath[0];

                        var matrixData = genericObject.GetHyperCubeData(layoutPath, pages);
                        if (hypercubeJson["visualization"].ToString() == "treemap")
                        {

                            var matrixData1 = genericObject.GetHyperCubeStackData(layoutPath, pages);
                            var json = System.Web.Helpers.Json.Encode(matrixData1);
                            JToken hypercubeStackJson = JToken.Parse(json);
                            if (hypercubeStackJson.HasValues == true)
                            {
                                if (hypercubeStackJson[0] != null && hypercubeStackJson[0]["Data"] != null && hypercubeStackJson[0]["Data"][0] != null && hypercubeStackJson[0]["Data"][0]["SubNodes"] != null)
                                {
                                    foreach (var stackDataList in hypercubeStackJson[0]["Data"][0]["SubNodes"])
                                    {
                                        int datarowCount = 0;
                                        DataRow dataRow = chartTable.NewRow();
                                        dataRow[datarowCount] = stackDataList["Text"].ToString();
                                        dataRow[++datarowCount] = stackDataList["SubNodes"][0] != null ? stackDataList["SubNodes"][0]["Text"].ToString() : "";
                                        chartTable.Rows.Add(dataRow);
                                    }

                                }
                            }

                        }
                        else if (matrixData != null && matrixData.Count() >= 1 && matrixData.FirstOrDefault().Matrix.Count() >= 1)
                        {
                            int index = 0;

                            foreach (var datapage in matrixData)
                            {
                                int matrixCount = datapage.Matrix.Count();
                                if (datapage?.Matrix != null)
                                {

                                    foreach (var qmatrix in datapage?.Matrix)
                                    {
                                        var measureCount = from p in qmatrix
                                                           where p.State == StateEnumType.LOCKED
                                                           group p by p.State into gp
                                                           select new
                                                           {
                                                               InstanceCount = gp.Count(),
                                                           };
                                        var dimensionCount = from p in qmatrix
                                                             where p.State == StateEnumType.OPTION
                                                             group p by p.State into gp
                                                             select new
                                                             {
                                                                 InstanceCount = gp.Count(),
                                                             };
                                        bool is2ndDimension = false;
                                        if (qmatrix != null && qmatrix.Count() >= 1)
                                        {
                                            DataRow dataRow = chartTable.NewRow();
                                            int datarowCount = 0;
                                            foreach (var matrix in qmatrix)
                                            {
                                                if (dimensionCount.FirstOrDefault() is null)
                                                    break;

                                                if (!matrix.State.ToString().ToLower().Contains("locked"))
                                                {
                                                    if (!is2ndDimension)
                                                    {
                                                        dataRow[datarowCount] = matrix.Text;
                                                        is2ndDimension = true;
                                                    }
                                                }
                                                else
                                                {
                                                    dataRow[++datarowCount] = matrix.Text;
                                                }
                                            }

                                            chartTable.Rows.Add(dataRow);
                                        }
                                        index++;
                                    }

                                }

                            }
                        }
                        #endregion
                        //
                        chartType = hypercubeJson["visualization"].ToString() == "treemap" ? "barchart_vertical" : hypercubeJson["visualization"].ToString() + (hypercubeJson["visualization"].ToString().ToLower().Contains("bar") || hypercubeJson["visualization"].ToString().ToLower().Contains("combo") ? "_" + hypercubeJson["orientation"].ToString() : "");

                    }
                }

                if (chartTable != null || chartTable.Rows.Count > 0)
                {
                    //Thread thread = new Thread(() => Application.Run(new CaptureChart.Capture(chartTable, imageFileName, chartType)));
                    Thread thread = new Thread(() => new GenerateChart(chartTable, imageFileName, chartType));
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    thread.Join();
                    log.Info(module, method, "Image captured...", true);
                    return true;
                }
                else
                    return false;

                //chartTable.TableName = "Data";
                //chartTable.WriteXml(@"E:\Received Files\QlikSenseTableLive!Vertical.xml");
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string refineJSON(string jsonData)
        {
            string response = jsonData;
            if (!jsonData.Contains("Invalid Request!") && !string.IsNullOrEmpty(jsonData))
            {
                response = string.Empty;
                var narrativeData = JToken.Parse(jsonData);
                var dataNarrative = narrativeData["data"];
                bool exclude1stLine = true;
                if (dataNarrative.HasValues)
                {
                    foreach (var data in dataNarrative)
                    {
                        foreach (var line in data)
                        {
                            if (!exclude1stLine)
                                response += "*√* " + line + "\n";
                            exclude1stLine = false;
                        }

                    }
                }
            }
            return response.Replace("<ColorRed>", "*`").Replace("</ColorRed>", "`*").Replace("<ColorGreen>", "*").Replace("</ColorGreen>", "*").Replace("<StyleBold>", "*").Replace("</StyleBold>", "*");
        }
        #endregion

        //#region Google Chart with Hypercube Data

        //public static string CaptureImagePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");

        //public void GetChart(dynamic chart, dynamic properties, string chartType)
        //{
        //    System.Data.DataTable dsChartData = new System.Data.DataTable();
        //    try
        //    {
        //        var pagers = new List<NxPage>() { new NxPage { Top = 0, Left = 0, Width = chart.GetLayout().HyperCube.Size.cx, Height = chart.GetLayout().HyperCube.Size.cy } };

        //        if (properties.HyperCubeDef?.Dimensions != null)
        //        {
        //            int dimCount = (int)properties.HyperCubeDef?.Dimensions.Count;

        //            foreach (var dimension in properties.HyperCubeDef?.Dimensions)
        //                dsChartData.Columns.Add(dimension.Def.FieldLabels[dimCount - 1]);
        //        }

        //        if (properties.HyperCubeDef?.Measures != null)
        //        {
        //            int measureCount = properties.HyperCubeDef.Measures.Count;

        //            foreach (var measure in properties.HyperCubeDef?.Measures)
        //                dsChartData.Columns.Add(measure.Def.Label, typeof(Double));
        //        }

        //        var cubedata = chart.GetHyperCubeData("/qHyperCubeDef", pagers);

        //        if (cubedata != null)
        //        {
        //            foreach (var data in cubedata)
        //            {
        //                var matrix = data.Matrix;
        //                int countMeasure = 0;

        //                foreach (var dataValue in matrix)
        //                {
        //                    //if (countMeasure >= 40)
        //                    //    break;

        //                    countMeasure++;
        //                    if (!string.IsNullOrEmpty(dataValue[0].Text) && dataValue[0].Text != "-")
        //                    {
        //                        DataRow row = dsChartData.NewRow();
        //                        int rowIndex = 0;
        //                        foreach (var item in dataValue)
        //                        {
        //                            if (item.State.ToString().ToLower() == "option")
        //                                row[rowIndex] = item.Text;
        //                            else if (item.State.ToString().ToLower() == "locked")
        //                                row[rowIndex] = item.Text;

        //                            ++rowIndex;
        //                        }

        //                        dsChartData.Rows.Add(row);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex) { }
        //    finally
        //    {
        //        System.Windows.Forms.Application.Run(new CaptureChart.Capture(dsChartData, CaptureImagePath + "\\" + properties.Id + ".png", chartType));

        //        //dsChartData.TableName = title;
        //        //dsChartData.WriteXml(@"C:\Users\jsuthar\Desktop\Slack Demo\Capture\" + title + ".xml", XmlWriteMode.WriteSchema);
        //    }
        //}

        //#endregion
    }

}